package com.zenoptics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.zenoptics.dao")
@SpringBootApplication(scanBasePackages = {"com.zenoptics"})
public class PowerBiExtractorApplication {


	public static void main(String[] args) {
		SpringApplication.run(PowerBiExtractorApplication.class, args);

	}


}
