package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LStatusMaster;
import org.springframework.data.repository.CrudRepository;

public interface StatusMasterRepository extends CrudRepository<LStatusMaster, Integer> {

    public LStatusMaster findByStatusMasterId(int statusMasterId);
}
