package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LDataDictionary;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DataDictionaryRepository extends CrudRepository<LDataDictionary, Integer> {

    public List<LDataDictionary> findAllByFieldId(String fieldId);

}
