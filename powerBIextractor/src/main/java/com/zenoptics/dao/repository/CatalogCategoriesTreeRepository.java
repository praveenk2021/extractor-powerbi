package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LCategoryDetails;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

//TODO: Make it like generic like JPA, so that any DB can be plugged in future.
public interface CatalogCategoriesTreeRepository extends CrudRepository<LCategoryDetails,Integer> {

    public LCategoryDetails findByCategoryNameAndParentCategoryId(String categoryName, int parentCategoryId);
    public LCategoryDetails findByParentCategoryId(int parentCategoryId);
    public List<LCategoryDetails> findAllByParentCategoryId(int reportCategoriesId);
}


