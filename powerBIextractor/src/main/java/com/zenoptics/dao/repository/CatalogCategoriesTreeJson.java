package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LCatalogCategoryTreeJson;
import org.springframework.data.repository.CrudRepository;

public interface CatalogCategoriesTreeJson extends CrudRepository<LCatalogCategoryTreeJson,Integer> {

    public LCatalogCategoryTreeJson findByType(String type);
}
