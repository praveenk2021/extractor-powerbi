package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LCatalogUserObjectAccess;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface LCatalogUserObjectAccessRepository
    extends CrudRepository<LCatalogUserObjectAccess, Integer> {

  @Modifying(clearAutomatically = true, flushAutomatically = true)
  public void deleteBySourceId(int sourceSystemId);

}
