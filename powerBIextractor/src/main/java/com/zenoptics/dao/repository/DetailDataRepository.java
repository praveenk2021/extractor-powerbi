package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LReportDetailData;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DetailDataRepository extends CrudRepository<LReportDetailData,Integer> {

    @Modifying(clearAutomatically=true, flushAutomatically=true)
    public void deleteBysourceSystemId(int sourceSystemId);

    public List<LReportDetailData> findAllBySourceSystemId(int sourceSystemId);
}
