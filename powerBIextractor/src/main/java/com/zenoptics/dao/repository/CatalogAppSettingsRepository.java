package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LCatalogAppSettings;
import org.springframework.data.repository.CrudRepository;

public interface CatalogAppSettingsRepository extends CrudRepository<LCatalogAppSettings,Integer> {
}
