package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LReportTypes;
import org.springframework.data.repository.CrudRepository;

public interface ReportTypeRepository extends CrudRepository<LReportTypes,Integer> {

    public LReportTypes findByDisplayValue(String reportTypeName);
}
