package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LReportHeaderData;
import com.zenoptics.dao.entity.LReportHeaderDataID;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HeaderDataRepository extends CrudRepository<LReportHeaderData, LReportHeaderDataID> {

    @Modifying(clearAutomatically=true, flushAutomatically=true)
    public List<LReportHeaderData> findAllBySourceSystemIdAndIsDeleted(int sourceSystemId,int isDeleted);
    public List<LReportHeaderData> findAllBySourceSystemId(int sourceSystemId);
    public void deleteById(String id);
}
