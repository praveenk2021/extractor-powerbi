package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LCategoryDetails;
import org.springframework.data.repository.CrudRepository;

public interface CategoryMovedHistoryRepository extends CrudRepository<LCategoryDetails,Integer>{

    public LCategoryDetails findByCategoryNameAndParentCategoryId(String categoryName, int parentCategoryId);
}
