package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LExtractJobLog;
import org.springframework.data.repository.CrudRepository;

public interface ExtractJobLogRepository extends CrudRepository<LExtractJobLog,Integer> {

    public LExtractJobLog findFirstBySourceIdOrderByExtractJobIdDesc(int sourceId);
    public LExtractJobLog findByExtractJobId(int jobId);
}


