package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LConnectorInfoMasterDetail;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface ConnectorInfoMasterDetailRepository extends CrudRepository<LConnectorInfoMasterDetail, Integer> {

    public List<LConnectorInfoMasterDetail> findByMasterId(int master_id);
}


