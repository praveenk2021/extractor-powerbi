package com.zenoptics.dao.repository;

import com.zenoptics.dao.entity.LUserInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserInfoRepository extends CrudRepository<LUserInfo,Integer> {

    public List<LUserInfo> findByEmail(String email);
}
