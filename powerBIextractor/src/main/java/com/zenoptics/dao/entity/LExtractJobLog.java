package com.zenoptics.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_extract_job_log")
public class LExtractJobLog {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="extract_job_Id")
    private Integer extractJobId;
    @Column(name="source_system_Id")
    private Integer sourceId;
    @Column(name="extract_start_time")
    private Timestamp extractStartTime;
    @Column(name="extract_end_time")
    private Timestamp extractEndTime;
    @Column(name="extract_log_file")
    private String extractLogFile;
    @Column(name="current_status")
    private Integer currentStatus;
    @Column(name="job_result_summary")
    private String jobResultSummary;
    @Column(name="job_type")
    private String jobType;


}
