package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_report_header_delta_links")
public class LReportHeaderDeltaLinks {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="link_id")
    private Integer linkId;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name="REPORT_HEADER_ID",referencedColumnName ="REPORT_HEADER_ID", insertable = false, updatable = false),
            @JoinColumn(name="source_system_id",referencedColumnName ="SOURCE_SYSTEM_ID",insertable = false, updatable = false)
    })
    private LReportHeaderData lReportHeaderData;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="REPORT_CATEGORIES_ID",referencedColumnName ="REPORT_CATEGORIES_ID")
    private LCategoryDetails lCategoryDetails;

}
