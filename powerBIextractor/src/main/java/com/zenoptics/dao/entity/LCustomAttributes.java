package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_custom_attributes")
public class LCustomAttributes {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="CUSTOM_ATTRIBUTE_ID")
    private Integer customAttributeId;
    @Column(name="customattribute")
    private String customattribute;
    @Column(name="DISPLAY_VALUE")
    private String displayValue;
    @Column(name="DESCRIPTION")
    private String description;
    @Column(name="sequence")
    private Integer sequence;
    @Column(name="visible")
    private Integer visible;
    @Column(name="viz_ctrl_type")
    private String vizCtrlType;
}
