package com.zenoptics.dao.entity;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_user_info")
public class LUserInfo {

    @Id
    @Column(name="id")
    private Integer id;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    @Column(name="user_id")
    private String userId;
    @Column(name="email")
    private String email;
    @Column(name="status")
    private Integer status;
    @Column(name="is_admin")
    private Short isAdmin;
    @Column(name="cost_center")
    private String costCenter;
    @Column(name="region")
    private String region;
    @Column(name="country")
    private String country;
    @Column(name="city")
    private String city;
    @Column(name="image_url")
    private String imageUrl;
    @Column(name="password")
    private String password;
    @Column(name="is_deleted")
    private Short isDeleted;
}
