package com.zenoptics.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_connector_info_master")
public class LConnectorInfoMaster {

    @Id
    @Column(name="connector_id")
    private Integer connectorId;
    @Column(name="connector_name")
    private String connectorName;
    @Column(name="connector_srt_name")
    private String connectorSrtName;
    @Column(name="connector_Lng_name")
    private String connectorLngName;
    @Column(name="create_date")
    private Timestamp createDate;
    @Column(name="last_update_date")
    private Timestamp lastUpdateDate;
    @Column(name="status")
    private String status;
    @Column(name="connection_test_status")
    private String connectionTestStatus;
    @Column(name="system_type")
    private String systemType;

}
