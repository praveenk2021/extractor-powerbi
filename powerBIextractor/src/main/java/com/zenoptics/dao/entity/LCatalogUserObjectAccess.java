package com.zenoptics.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_catalog_user_object_access")
public class LCatalogUserObjectAccess {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;
    @Column(name="user_id")
    private String userId;
    @Column(name="catalog_source")
    private Integer sourceId;
    @Column(name="REPORT_HEADER_ID")
    private String reportHeaderId;

}
