package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name="t_report_header_custom_attrs")
public class TReportHeaderCustomAttrs implements Serializable {

    @Id
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name="REPORT_HEADER_ID",referencedColumnName ="REPORT_HEADER_ID", insertable = false, updatable = false),
            @JoinColumn(name="source_system_id",referencedColumnName ="SOURCE_SYSTEM_ID",insertable = false, updatable = false)
    })
    private LReportHeaderData lReportHeaderData;
    @Id
    @Column(name="CUSTOM_ATTRIBUTE_ID")
    private Integer customAttributeId;
    @Column(name="CUSTOM_ATTRIBUTE_VALUE")
    private String customAttributeValue;
    @Column(name="UPDATED_USER_ID")
    private String updateUserId;
    @Column(name="UPDATE_DATE")
    private Timestamp updateDate;

    @ManyToOne()
    @JoinColumn(name="CUSTOM_ATTRIBUTE_ID",referencedColumnName ="CUSTOM_ATTRIBUTE_ID")
    private LCustomAttributes lCustomAttributes;
}
