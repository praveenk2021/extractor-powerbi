package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_catalog_category_tree_json")
public class LCatalogCategoryTreeJson {

    @Id
    @Column(name="id")
    private Integer id;
    @Column(name="json_structure")
    private String jsonStructure;
    @Column(name="type")
    private String type;
    @Column(name="sort_type")
    private String sortType;
}
