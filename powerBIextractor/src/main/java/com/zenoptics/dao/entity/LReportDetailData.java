package com.zenoptics.dao.entity;

import lombok.*;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name="l_report_detail_data",catalog = "zenoptics")
public class LReportDetailData implements Serializable {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="COLUMN_ID")
    private Integer columnId;
    /*@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumnsOrFormulas(value = {
            @JoinColumnOrFormula(column = @JoinColumn(name="REPORT_HEADER_ID",referencedColumnName ="REPORT_HEADER_ID",insertable = true, updatable = true)),
            @JoinColumnOrFormula(formula = @JoinFormula(value="source_system_id",referencedColumnName ="SOURCE_SYSTEM_ID"))
    })*/
    @Column(name="REPORT_HEADER_ID")
    private String id;
    @Column(name="IOBJNM")
    private String iobjnm;
    @Column(name="TXTLG")
    private String txtlg;
    @Column(name="IOBJTP")
    private String iobjtp;
    @Column(name="CREATE_DATE")
    private Timestamp createDate;
    @Column(name="UPDATE_DATE")
    private Timestamp updateDate;
    @Column(name="source_system_id")
    private int sourceSystemId;
    @Column(name="FIELD_DETAILS")
    private String fieldDetails;
}
