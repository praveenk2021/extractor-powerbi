package com.zenoptics.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_catalog_app_settings")
public class LCatalogAppSettings {
    @Id
    @Column(name="setting_id")
    private Integer settingId;
    @Column(name="customer_logo_url")
    private String customerLogoUrl;
    @Column(name="customer_css_url")
    private String customerCssUrl;
    @Column(name="allow_css_pref")
    private Integer allowCssPref;
    @Column(name="search_server_url")
    private String searchServerUrl;
    @Column(name="show_quick_links")
    private Integer showQuickLinks;
    @Column(name="show_coe_links")
    private Integer showCoeLinks;
    @Column(name="show_announcements")
    private Integer showAnnouncements;
    @Column(name="show_source")
    private Integer showSource;
    @Column(name="quick_links_html")
    private String quickLinksHtml;
    @Column(name="coe_html")
    private String coeHtml;
    @Column(name="announcements_html")
    private String announcementsHtml;
    @Column(name="source_html")
    private String sourceHtml;
    @Column(name="ldap_server_url")
    private String ldapServerUrl;
    @Column(name="ldap_userbase")
    private String ldapUserBase;
    @Column(name="ldap_usersearch_string")
    private String ldapUserSearchString;
    @Column(name="ldap_rolebase")
    private String ldapRoleBase;
    @Column(name="ldap_rolename")
    private String ldapRoleName;
    @Column(name="ldap_adm_user")
    private String ldapAdmUser;
    @Column(name="ldap_adm_password")
    private String ldapAdmPassword;
    @Column(name="use_ldap")
    private String useLdap;
    @Column(name="announcement_rss_url")
    private String announcementRssUrl;
    @Column(name="ldap_users_group")
    private String ldapUsersGroup;
    @Column(name="collaboration_enabled")
    private Integer collaborationEnabled;
    @Column(name="current_collaboration_tool")
    private String currentCollaborationTool;
    @Column(name="collaboration_tool_token")
    private String collaborationToolToken;
    @Column(name="whats_new_time_span")
    private Integer whatsNewTimeSpan;
    @Column(name="show_network")
    private Integer showNetwork;
    @Column(name="network_drive_url")
    private String networkDriveUrl;
    @Column(name="show_bi_glossary")
    private Integer showBiGlossary;
    @Column(name="new_content_added_timespan")
    private Integer newContentAddedTimespan;
}
