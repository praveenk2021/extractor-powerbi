package com.zenoptics.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_connector_info_master_detail")
public class LConnectorInfoMasterDetail {

    @Id
    @Column(name="id")
    private Integer id;
    @Column(name="master_id")
    private Integer masterId;
    @Column(name="property_name")
    private String propertyName;
    @Column(name="property_key")
    private String propertyKey;
    @Column(name="property_value")
    private String propertyValue;
}
