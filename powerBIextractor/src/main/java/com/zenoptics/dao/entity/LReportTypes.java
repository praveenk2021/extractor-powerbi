package com.zenoptics.dao.entity;


import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_report_types")
public class LReportTypes {

    @Id
    @Column(name="REPORT_TYPES_ID")
    private Integer reportTypeId;
    @Column(name="DISPLAY_VALUE")
    private String displayValue;
    @Column(name="DESCRIPTION")
    private String description;
    @Column(name="ICON_URL")
    private String iconUrl;
    @Column(name="IS_ACTIVE")
    private Integer isActive;
    @Column(name="IS_DELETED")
    private Integer isDeleted;
    @Column(name="CREATE_USER_ID")
    private String createUserId;
    @Column(name="CREATE_DATE")
    private Timestamp createDate;
    @Column(name="UPDATE_USER_ID")
    private String updateUserId;
    @Column(name="UPDATE_DATE")
    private Timestamp updateDate;
    @Column(name="thumbnail_generation_supported")
    private String thumbSupport;

    @OneToMany(mappedBy = "lReportTypes")
    private Set<TReportTypeReportUrlMapping> tReportTypeReportUrlMappingSet;


}
