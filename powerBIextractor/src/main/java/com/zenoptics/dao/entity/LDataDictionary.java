package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_data_dictionary")
public class LDataDictionary {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="DATA_DICTION_ID")
    private Integer dataDictionId;
    @Column(name="source_system_id")
    private Integer sourceSystemId;
    @Column(name="field_id")
    private String fieldId;
    @Column(name="field_name")
    private String fieldName;
    @Column(name="std_name")
    private String stdName;
    @Column(name="approved")
    private Integer approved;
    @Column(name="description")
    private String description;
    @Column(name="business_area")
    private String businessArea;
    @Column(name="same_as")
    private String sameAs;
    @Column(name="derived_from")
    private String derivedFrom;
    @Column(name="long_desc")
    private String longDesc;
    @Column(name="doc_link")
    private String docLink;
    @Column(name="data_dictionary_origin")
    private Integer dataDictionaryOrigin;
    @Column(name="created_date")
    private Timestamp createdDate;
    @Column(name="created_by")
    private String createdBy;
    @Column(name="modified_date")
    private Timestamp modifiedDate;
    @Column(name="modified_by")
    private String modifiedBy;

}
