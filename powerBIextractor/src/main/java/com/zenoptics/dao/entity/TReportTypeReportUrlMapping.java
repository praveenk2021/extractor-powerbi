package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name="t_reporttype_reporturl_mapping")
public class TReportTypeReportUrlMapping {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="MAPPING_ID")
    private Integer mappingId;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="catalog_source",referencedColumnName ="connector_id", insertable = false, updatable = false)
    private LConnectorInfoMaster lConnectorInfoMaster;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="REPORT_TYPE_ID",referencedColumnName ="REPORT_TYPES_ID",insertable = false, updatable = false)
    private LReportTypes lReportTypes;
    @Column(name="REPORT_URL")
    private String reportUrl;
    @Column(name="thumbnail_generation_url")
    private String thumbnailGenerationUrl;
}
