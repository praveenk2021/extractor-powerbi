package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_report_ratings")
public class LReportRatings {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="rating_id")
    private Integer ratingId;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name="REPORT_HEADER_ID",referencedColumnName ="REPORT_HEADER_ID"),
            @JoinColumn(name="source_system_id",referencedColumnName ="SOURCE_SYSTEM_ID")
    })
    private LReportHeaderData lReportHeaderData;
    @Column(name="rated_by_userid")
    private String ratedByUserId;
    @Column(name="rating")
    private Integer rating;
    @Column(name="rating_update_timestamp")
    private Timestamp ratingUpdateTimestamp;
    @Column(name="comment")
    private String comment;

}
