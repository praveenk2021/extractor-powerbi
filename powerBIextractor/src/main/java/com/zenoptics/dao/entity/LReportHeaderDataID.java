package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class LReportHeaderDataID implements Serializable {
    private String id;
    private Integer sourceSystemId;
}
