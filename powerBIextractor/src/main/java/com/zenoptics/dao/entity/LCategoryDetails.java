package com.zenoptics.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_catalog_categories_tree")
public class LCategoryDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="REPORT_CATEGORIES_ID")
    private int reportCategoriesId;
    @Column(name="CATEGORY_NAME")
    private String categoryName;
    @Column(name="CATEGORY_DESC")
    private String categoryDesc;
    @Column(name="IS_ACTIVE")
    private int isActive;
    @Column(name="CREATE_DATE")
    private Timestamp createDate;
    @Column(name="PARENT_CATEGORY_ID")
    private int parentCategoryId;
    @Column(name="is_enabled_for_whats_new")
    private String isEnabledForWhatsNew;
    @Column(name="category_source")
    private String categorySource;
    @Column(name="sort_type")
    private String sortType;
    @Column(name="original_category_name")
    private String originalCategoryName;
    @Column(name="category_unique_name")
    private String categoryUniqueName;

}
