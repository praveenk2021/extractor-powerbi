package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="t_latest_report_usage")
public class TLatestReportUsage {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="USAGE_ID")
    private Integer usageId;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name="REPORT_HEADER_ID",referencedColumnName ="REPORT_HEADER_ID"),
            @JoinColumn(name="source_system_id",referencedColumnName ="SOURCE_SYSTEM_ID")
    })
    private LReportHeaderData lReportHeaderData;
    @Column(name="TIMESTAMP")
    private Timestamp timestamp;
    @Column(name="USER_ID")
    private String userId;
}
