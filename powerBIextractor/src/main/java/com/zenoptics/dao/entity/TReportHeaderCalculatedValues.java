package com.zenoptics.dao.entity;

import lombok.*;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name="t_report_header_calculated_values")
public class TReportHeaderCalculatedValues {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name="REPORT_HEADER_ID",referencedColumnName ="REPORT_HEADER_ID"),
            @JoinColumn(name="source_system_id",referencedColumnName ="SOURCE_SYSTEM_ID")
        })
    private LReportHeaderData lReportHeaderData;
    @Column(name="rated_by_userid")
    private String ratedByUserId;
    @Column(name="average_rating")
    private Double averageRating;
    @Column(name="last_one_week_usage")
    private Integer lastOneWeekUsage;
    @Column(name="one_week_usage_update_timestamp")
    private Timestamp oneWeekUsageUpdateTimeStamp;
    @Column(name="last_30_days_usage")
    private Integer last30DaysUsage;
}
