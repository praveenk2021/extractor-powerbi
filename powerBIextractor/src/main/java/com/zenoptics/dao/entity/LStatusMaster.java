package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="l_status_master")
public class LStatusMaster {

    @Id
    @Column(name="STATUS_MASTER_ID")
    private Integer statusMasterId;
    @Column(name="DISPLAY_VALUE")
    private String displayValue;
    @Column(name="DESCRIPTION")
    private String description;
    @Column(name="ICON_URL")
    private String iconUrl;
    @Column(name="CREATE_USER_ID")
    private String createUserID;
    @Column(name="CREATE_DATE")
    private Timestamp createDate;
    @Column(name="UPDATE_USER_ID")
    private String updateUserID;
    @Column(name="UPDATE_DATE")
    private Timestamp updateDate;
}
