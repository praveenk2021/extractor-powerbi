package com.zenoptics.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name="l_report_header_data")
@IdClass(LReportHeaderDataID.class)
public class LReportHeaderData implements Serializable {

    @Id
    @Column(name="REPORT_HEADER_ID")
    private String id;
    @Column(name="DISPLAY_VALUE")
    private String displayValue;
    @Column(name="DESCRIPTION")
    private String description;
    @Column(name="REPORT_KEYWORDS")
    private String reportKeywords;
    @Column(name="REPORT_OWNER")
    private String reportOwner;
    @Column(name="REPORT_PREVIEW_IMAGE_URL")
    private String reportPreviewImageUrl;
    @Column(name="DATA_SOURCE")
    private String dataSource;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="REPORT_CATEGORIES_ID",referencedColumnName ="REPORT_CATEGORIES_ID")
    private LCategoryDetails lCategoryDetails;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="REPORT_TYPES_ID",referencedColumnName ="REPORT_TYPES_ID")
    private LReportTypes lReportTypes;
    @Column(name="SECURITY_WHITELISTED")
    private Integer securityWhitelisted;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "STATUS_MASTER_ID")
    private LStatusMaster lStatusMaster;
    @Column(name="CREATE_USER_ID")
    private String createUserId;
    @Column(name="CREATE_DATE")
    private Timestamp createDate;
    @Column(name="UPDATE_USER_ID")
    private String updateUserId;
    @Column(name="UPDATE_DATE")
    private Timestamp updateDate;
    @Id
    @Column(name="SOURCE_SYSTEM_ID")
    private Integer sourceSystemId;
    @Column(name="CREATE_MODE")
    private String createMode;
    @Column(name="ZEN_CREATE_DATE")
    private Timestamp zenCreateDate;
    @Column(name="IS_PUBLISH")
    private Integer isPublish;
    @Column(name="IS_DELETED")
    private Integer isDeleted;
    @Column(name="CATEGORY_HIERARCHY")
    private String categoryHierarchy;
    @Column(name="thumbnail_id")
    private String thumbnailId;

    @OneToMany(mappedBy="lReportHeaderData")
    private Set<TReportHeaderCalculatedValues> tReportHeaderCalculatedValuesSet;
    @OneToMany(mappedBy="lReportHeaderData")
    private Set<TLatestReportUsage> tLatestReportUsageSet;
    @OneToMany(mappedBy="lReportHeaderData")
    private Set<TCatalogReportUsage> tCatalogReportUsageSet;
    @OneToMany(mappedBy="lReportHeaderData")
    private Set<LReportRatings> lReportRatingsSet;
    @OneToMany(mappedBy="lReportHeaderData")
    private Set<TReportHeaderCustomAttrs> tReportHeaderCustomAttrsSet;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="SOURCE_SYSTEM_ID",referencedColumnName ="connector_id",insertable = false, updatable = false)
    private LConnectorInfoMaster lConnectorInfoMaster;


    @Override
    public boolean equals(Object other){
        LReportHeaderData lReportHeaderData=(LReportHeaderData)other;
        return this.getId().equals(lReportHeaderData.getId());
    }
}
