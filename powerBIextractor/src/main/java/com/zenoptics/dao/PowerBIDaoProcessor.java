package com.zenoptics.dao;

import com.zenoptics.dao.entity.*;
import com.zenoptics.dao.repository.*;
import com.zenoptics.pojo.powerBI.*;
import com.zenoptics.util.ExtractorsUtil;
import lombok.Getter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/*
PowerBI DAO Procssor class performs following operations,
1) Update the Existing records which are not part of current run to isDeleted=1.
2) Load the Header Level data into DB.
3) Before Executing point 1&2,the data is mapped from PowerBIHeaderData to LReportHeaderData.
4) Load detailed data into DB.
5) Load User permissions to DB.
6) Bulk Load logic and the failed record processing logic also implemented.
 */
@Repository
@Transactional
public class PowerBIDaoProcessor {

  @Autowired private ReportTypeRepository reportTypeRepository;

  @Autowired private HeaderDataRepository headerDataRepository;

  @Autowired private LCatalogUserObjectAccessRepository lCatalogUserObjectAccessRepository;

  @Autowired private DetailDataRepository detailDataRepository;

  @Autowired private CatalogCategoriesTreeRepository catalogCategoriesTreeRepository;

  @Autowired private ExtractorsUtil extractorsUtil;

  @Value("${batch_size}")
  private int batchSize;

  @Getter private int totalUpdates=0;

  final long timestamp = System.currentTimeMillis();
  Logger log = Logger.getLogger(getClass().getName() + timestamp);
  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

  private List<LReportHeaderData> failedListHeaderDataFinal = new ArrayList<>();
  private List<LReportDetailData> failedListDetailedDataFinal = new ArrayList<>();
  private List<LCatalogUserObjectAccess> failedListUserPermissionsFinal = new ArrayList<>();


  public LReportTypes getReportTypeList(String reportTypeName) {
    return reportTypeRepository.findByDisplayValue(reportTypeName);
  }

  public void loadHeaderDataIntoDB(
      Map<String, PowerBIHeaderData> powerBIHeaderDataMap, int sourceSystemId) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Persisting Header data into DB");
    Set<LReportHeaderData> lReportHeaderDataList = new HashSet<>();
    List<PowerBIHeaderData> powerBIHeaderDataList =
        new ArrayList<PowerBIHeaderData>(powerBIHeaderDataMap.values());
    // Update the existing records in DB which are not part of current load to isDeleted=1
    Map<Integer, LCategoryDetails>  lCategoryDetailsMap=extractorsUtil.fetchCategoriesDetails();
    Map<Integer, LReportTypes>  lReportTypesMap=extractorsUtil.fetchReportTypes();
    LStatusMaster lStatusMaster=extractorsUtil.fetchStatusMasterMapping(1);
    updateExistingReportHeaderList(powerBIHeaderDataList,lCategoryDetailsMap,lReportTypesMap,lStatusMaster, sourceSystemId);

    for (PowerBIHeaderData powerBIHeaderData : powerBIHeaderDataList) {
      lReportHeaderDataList.add(mapToLReportHeaderData(powerBIHeaderData,lCategoryDetailsMap,lReportTypesMap,lStatusMaster, sourceSystemId));
    }
    createOrUpdateNewRecords(lReportHeaderDataList);
  }

  /*
   * Update the existing dirty records in DB to isDeleted=1(which are not part of
   * current Executor run)
   * and update it in batches to the DB.
   */
  private void updateExistingReportHeaderList(
      List<PowerBIHeaderData> powerBIHeaderDataList,Map<Integer, LCategoryDetails> reportCategoriesIdMap
          ,Map<Integer, LReportTypes> lReportTypesMap,LStatusMaster lStatusMaster, int sourceSystemId) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Updating existing Header data in DB with isdeleted=1");
    List<LReportHeaderData> existingListInDB =
        headerDataRepository.findAllBySourceSystemIdAndIsDeleted(sourceSystemId, 0);
    List<LReportHeaderData> newList =
        powerBIHeaderDataList.stream()
            .map(powerBIHeaderData -> mapToLReportHeaderData(powerBIHeaderData,reportCategoriesIdMap,lReportTypesMap,lStatusMaster, sourceSystemId))
            .collect(Collectors.toList());

    // find the diff records btw new and existing lists
    List<LReportHeaderData> diffRecords =
        existingListInDB.stream()
            .filter(existingData -> !newList.contains(existingData))
            .collect(Collectors.toList());

    List<LReportHeaderData> newRecords =
            newList.stream()
                    .filter(newData -> !existingListInDB.contains(newData))
                    .collect(Collectors.toList());

    totalUpdates = newRecords.size();
    prepareFinalListAndPersist(diffRecords);
  }

  private void prepareFinalListAndPersist(List<LReportHeaderData> diffRecords) {
    List<LReportHeaderData> finalList =
        diffRecords.stream()
            .map(
                diffRecord -> {
                  diffRecord.setIsDeleted(1);
                  return diffRecord;
                })
            .collect(Collectors.toList());

    int totalSize = finalList.size();
    int remainingCount = 0;
    int endRange = 0;
    for (int i = 0; i < totalSize; i = i + batchSize) {
      endRange = i + batchSize;
      if (endRange <= totalSize) {
        headerDataRepository.saveAll(finalList.subList(i, endRange));
      } else {
        remainingCount = totalSize - i;
        if (remainingCount > 0) {
          endRange = remainingCount + i;
          headerDataRepository.saveAll(finalList.subList(i, endRange));
        }
      }
    }
  }

  /*
   * Steps involved in Bulk Load for Header Data
   * 1) Split the LReportHeaderData into batches based on batch size provided.
   * 2) Load all the batches into DB.
   * 3) IF some batch fails load it into Failed list.
   * 4) Then reprocess the failed batch List sequentially.
   * 5) Finally capture the individual records which failed to persist in the batch.
   */
  private void createOrUpdateNewRecords(Set<LReportHeaderData> lReportHeaderDataSet) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Persisting  Header data in bulk into DB");
    List<LReportHeaderData> lReportHeaderDataList = new ArrayList<>(lReportHeaderDataSet);
    List<LReportHeaderData> failedListHeaderData = new ArrayList<>();
    int totalSize = lReportHeaderDataList.size();
    int remainingCount = 0;
    int endRange = 0;
    for (int i = 0; i < totalSize; i = i + batchSize) {
      try {
        endRange = i + batchSize;
        if (endRange <= totalSize) {
          headerDataRepository.saveAll(lReportHeaderDataList.subList(i, endRange));
        } else {
          remainingCount = totalSize - i;
          if (remainingCount > 0) {
            endRange = remainingCount + i;
            headerDataRepository.saveAll(lReportHeaderDataList.subList(i, endRange));
          }
        }
      } catch (Exception e) {
        failedListHeaderData.addAll(lReportHeaderDataList.subList(i, endRange));
      }
    }
    if (failedListHeaderData.size() > 0) {
      reprocessFailedHeaderDataSequentially(failedListHeaderData);
    }
  }

  private void reprocessFailedHeaderDataSequentially(
      List<LReportHeaderData> failedListHeaderDataList) {
    for (LReportHeaderData lReportHeaderData : failedListHeaderDataList) {
      try {
        headerDataRepository.save(lReportHeaderData);
      } catch (Exception e) {
        log.info(lReportHeaderData.getId());
        headerDataRepository.save(lReportHeaderData);
        failedListHeaderDataFinal.add(lReportHeaderData);
      }
    }
  }

  /*
   * Steps involved in Bulk Load for Detailed Data
   * 1) Remove the failed HeaderData from DetailedDataList.
   * 2) Split the PowerBIDetailedData into batches based on batch size provided.
   * 3) Load all the batches into DB.
   * 4) IF some batch fails load it into Failed list.
   * 5) Then reprocess the failed batch List sequentially.
   * 6) Finally capture the individual records which failed to persist in the batch.
   * 7) Remove the records which failed the DetailedData load but it is already persisted  in Header table if any.
   */
  public void loadDetailedDataIntoDB(
      Map<String, Set<PowerBIDetailedData>> reportDetailedDataMapping, int sourceSystemId) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Persisting Detailed data into DB");
    Set<LReportDetailData> lDetailedData = new HashSet<>();
    if (failedListHeaderDataFinal.size() > 0) {
      reportDetailedDataMapping =
          removeFailedHeaderDataFromDetailedDataList(reportDetailedDataMapping);
    }
    reportDetailedDataMapping.entrySet().stream()
        .forEach(
            reportDetailedData -> {
              reportDetailedData.getValue().stream()
                  .forEach(
                      detailedData -> {
                        lDetailedData.add(prepareDetailedDataObject(detailedData));
                      });
            });
    detailDataRepository.deleteBysourceSystemId(sourceSystemId);
    savingDetailedDataInBulk(lDetailedData);
  }

  private Map<String, Set<PowerBIDetailedData>> removeFailedHeaderDataFromDetailedDataList(
      Map<String, Set<PowerBIDetailedData>> reportDetailedDataMapping) {
    failedListHeaderDataFinal.stream()
        .forEach(
            failedListHeaderData -> {
              reportDetailedDataMapping.remove(failedListHeaderData.getId());
            });
    return reportDetailedDataMapping;
  }

  private void savingDetailedDataInBulk(Set<LReportDetailData> lDetailedData) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Persisting  Deatiled data in bulk into DB");
    List<LReportDetailData> lDetailedDataList = new ArrayList<>(lDetailedData);
    List<LReportDetailData> failedListDetailedData = new ArrayList<>();
    int totalSize = lDetailedDataList.size();
    int remainingCount = 0;
    int endRange = 0;
    for (int i = 0; i < totalSize; i = i + batchSize) {
      try {
        endRange = i + batchSize;
        if (endRange <= totalSize) {
          detailDataRepository.saveAll(lDetailedDataList.subList(i, endRange));
        } else {
          remainingCount = totalSize - i;
          if (remainingCount > 0) {
            endRange = remainingCount + i;
            detailDataRepository.saveAll(lDetailedDataList.subList(i, endRange));
          }
        }
      } catch (Exception e) {
        failedListDetailedData.addAll(lDetailedDataList.subList(i, endRange));
      }
    }
    if (failedListDetailedData.size() > 0) {
      reprocessFailedDetailedDataSequentially(failedListDetailedData);
      removeFailedDetailedDataFromHeaderTable();
    }
  }

  private void reprocessFailedDetailedDataSequentially(
      List<LReportDetailData> failedListDetailedData) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Reprocessing failed Deatiled data into DB");
    for (LReportDetailData lReportDetailData : failedListDetailedData) {
      try {
        detailDataRepository.save(lReportDetailData);
      } catch (Exception e) {
        failedListDetailedDataFinal.add(lReportDetailData);
      }
    }
  }

  private void removeFailedDetailedDataFromHeaderTable() {
    failedListDetailedDataFinal.stream()
        .forEach(
            detailedData -> {
              headerDataRepository.deleteById(detailedData.getId());
            });
  }

  /*
   * Steps involved in Bulk Load for User Permissions Data
   * 1) Remove the failed HeaderData from User PermissionsList.
   * 2) Remove the failed DetailedData from User PermissionsList.
   * 2) Split the LCatalogUserObjectAccess into batches based on batch size provided.
   * 3) Load all the batches into DB.
   * 4) IF some batch fails load it into Failed list.
   * 5) Then reprocess the failed batch List sequentially.
   * 6) Finally capture the individual records which failed to persist in the batch.
   */
  public void loadUsersPermissionDataIntoDB(
      Map<String, Set<String>> finalUserList, int sourceSystemId) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Persisting  User Permissions data into DB");
    Set<LCatalogUserObjectAccess> lCatalogUserObjectAccessesSet = new HashSet<>();
    if (failedListHeaderDataFinal.size() > 0) {
      finalUserList = removeFailedHeaderDataFromUserPermissionsList(finalUserList);
    }
    if (failedListDetailedDataFinal.size() > 0) {
      finalUserList = removeFailedDetailedDataFromUserPermissionsList(finalUserList);
    }
    finalUserList.entrySet().stream()
        .forEach(
            userMapping -> {
              String reportHeaderId = userMapping.getKey();
              userMapping.getValue().stream()
                  .forEach(
                      userId -> {
                        lCatalogUserObjectAccessesSet.add(
                            prepareCatalogUserObject(reportHeaderId, userId, sourceSystemId));
                      });
            });
    lCatalogUserObjectAccessRepository.deleteBySourceId(sourceSystemId);
    saveUserPermissionsDataInBulk(lCatalogUserObjectAccessesSet);
  }

  private Map<String, Set<String>> removeFailedHeaderDataFromUserPermissionsList(
      Map<String, Set<String>> finalUserList) {
    failedListHeaderDataFinal.stream()
        .forEach(
            failedListHeaderData -> {
              finalUserList.remove(failedListHeaderData.getId());
            });
    return finalUserList;
  }

  private Map<String, Set<String>> removeFailedDetailedDataFromUserPermissionsList(
      Map<String, Set<String>> finalUserList) {
    failedListDetailedDataFinal.stream()
        .forEach(
            failedListDetailedData -> {
              finalUserList.remove(failedListDetailedData.getId());
            });
    return finalUserList;
  }

  private void saveUserPermissionsDataInBulk(
      Set<LCatalogUserObjectAccess> lCatalogUserObjectAccessesSet) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Persisting  User Permissions data in bulk into DB");
    List<LCatalogUserObjectAccess> lCatalogUserObjectAccessesList =
        new ArrayList<>(lCatalogUserObjectAccessesSet);
    List<LCatalogUserObjectAccess> failedListUserPermissions = new ArrayList<>();
    int totalSize = lCatalogUserObjectAccessesList.size();
    int remainingCount = 0;
    int endRange = 0;
    for (int i = 0; i < totalSize; i = i + batchSize) {
      try {
        endRange = i + batchSize;
        if (endRange <= totalSize) {
          lCatalogUserObjectAccessRepository.saveAll(
              lCatalogUserObjectAccessesList.subList(i, endRange));
        } else {
          remainingCount = totalSize - i;
          if (remainingCount > 0) {
            endRange = remainingCount + i;
            lCatalogUserObjectAccessRepository.saveAll(
                lCatalogUserObjectAccessesList.subList(i, endRange));
          }
        }
      } catch (Exception e) {
        failedListUserPermissions.addAll(lCatalogUserObjectAccessesList.subList(i, endRange));
      }
    }
    if (failedListUserPermissions.size() > 0) {
      reprocessFailedUserPermissionsSequentially(failedListUserPermissions);
    }
  }

  private void reprocessFailedUserPermissionsSequentially(
      List<LCatalogUserObjectAccess> failedListUserPermissions) {
    for (LCatalogUserObjectAccess lCatalogUserObjectAccess : failedListUserPermissions) {
      try {
        lCatalogUserObjectAccessRepository.save(lCatalogUserObjectAccess);
      } catch (Exception e) {
        failedListUserPermissionsFinal.add(lCatalogUserObjectAccess);
      }
    }
  }

  private LReportDetailData prepareDetailedDataObject(PowerBIDetailedData detailedData) {
    return LReportDetailData.builder()
        .id(detailedData.getHeaderId())
        .iobjnm(detailedData.getDatasetColumnId())
        .txtlg(detailedData.getFieldName())
        .sourceSystemId(detailedData.getSourceSystemId())
        .fieldDetails(detailedData.getFieldDetails())
        .build();
  }

  private LCatalogUserObjectAccess prepareCatalogUserObject(
      String reportHeaderID, String userId, int sourceSystemId) {
    return LCatalogUserObjectAccess.builder()
            .userId(userId)
            .sourceId(sourceSystemId)
            .reportHeaderId(reportHeaderID)
            .build();
  }

  private LReportHeaderData mapToLReportHeaderData(
      PowerBIHeaderData powerBIHeaderData,Map<Integer,LCategoryDetails> reportCategoriesIdMap,
      Map<Integer, LReportTypes> lReportTypesMap,LStatusMaster lStatusMaster,int sourceSystemId) {

    return LReportHeaderData.builder()
        .id(powerBIHeaderData.getHeaderId())
        .displayValue(
            Optional.ofNullable(powerBIHeaderData.getDisplayValue()).isPresent()
                ? powerBIHeaderData.getDisplayValue()
                : "")
        .description(
            Optional.ofNullable(powerBIHeaderData.getDescription()).isPresent()
                ? powerBIHeaderData.getDescription()
                : "")
        .reportKeywords("")
        .reportOwner(
            Optional.ofNullable(powerBIHeaderData.getCreatedBy()).isPresent()
                ? powerBIHeaderData.getCreatedBy()
                : "")
        .reportPreviewImageUrl("")
        .dataSource(
            Optional.ofNullable(powerBIHeaderData.getDataSource()).isPresent()
                ? powerBIHeaderData.getDataSource()
                : "")
        .lCategoryDetails(reportCategoriesIdMap.get(powerBIHeaderData.getCatagoryId()))
        .lReportTypes(lReportTypesMap.get(powerBIHeaderData.getReportTypeId()))
        .securityWhitelisted(0)
        .lStatusMaster(lStatusMaster)
        .createUserId(
            Optional.ofNullable(powerBIHeaderData.getCreatedBy()).isPresent()
                ? powerBIHeaderData.getCreatedBy()
                : "")
        .createDate(getTimestamp(powerBIHeaderData.getCreatedDateTime()))
        .updateUserId("")
        .updateDate(getTimestamp(powerBIHeaderData.getModifiedDateTime()))
        .sourceSystemId(sourceSystemId)
        .createMode("")
        .zenCreateDate(getTimestamp(powerBIHeaderData.getCreatedDateTime()))
        .isPublish(0)
        .isDeleted(0)
        .thumbnailId("")
        .build();
  }

  private Timestamp getTimestamp(String timestamp) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
    SimpleDateFormat powerBIFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    Date powerBIDate = null;
    Date parsedDate = null;
    try {
      if (timestamp != "") {
        powerBIDate = powerBIFormat.parse(timestamp);
        parsedDate = dateFormat.parse(dateFormat.format(powerBIDate));
      } else {
        parsedDate = dateFormat.parse(dateFormat.format(new Date()));
      }
    } catch (ParseException e) {

    }
    return new Timestamp(parsedDate.getTime());
  }
}
