package com.zenoptics.restclient;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.zenoptics.authservices.AuthenticationModes;
import com.zenoptics.exceptionhandler.HttpClientException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PowerBIRestClient implements RestClient {

    @Autowired
    public AuthenticationModes powerBIAuthentication;

    final static int DEFAULT_WAIT_TIME_MS = 1000;
    final static int MAX_WAIT_TIME_MS = 1800000;
    final int waitTimeMs = 60000;
    int totalWaitTimeMs = 0;
    private long lastAPISubmitTime = 0;
    private int apiCallCount = 0;

    private HttpClient httpclient = new HttpClient();
    private Date dt = new Date();

    final long timestamp = System.currentTimeMillis();
    Logger log = Logger.getLogger(getClass().getName() + timestamp);

    @Override
    public JsonObject getCallRestClientAPI(Map<String, String> connectorInfoMasterDetailMap, String uri, String token, int sourceSystem)
            throws InterruptedException, IOException, HttpClientException {
        JsonElement response = null;
        GetMethod getMethod = new GetMethod(uri);

        getMethod.addRequestHeader("Authorization", "Bearer " + token);
        long currentTime = dt.getTime();
        long elapsedTimeFromPrevCall = currentTime - lastAPISubmitTime;
        log.debug("Elapsed time from previous API call :" + elapsedTimeFromPrevCall + " :" + currentTime + " - " + lastAPISubmitTime);
        if (elapsedTimeFromPrevCall > DEFAULT_WAIT_TIME_MS) {
            elapsedTimeFromPrevCall = DEFAULT_WAIT_TIME_MS;
        }
        Thread.sleep(DEFAULT_WAIT_TIME_MS - elapsedTimeFromPrevCall);

        log.info("API :" + uri);
        httpclient.executeMethod(getMethod);
        apiCallCount++;
        lastAPISubmitTime = dt.getTime();

        String responseBodyAsString = getMethod.getResponseBodyAsString();
        response = new JsonParser().parse(responseBodyAsString);

        return readHttpCallResponse(connectorInfoMasterDetailMap, uri, token, sourceSystem, getMethod, response,null);

    }

    @Override
    public JsonObject postCallRestClientAPI(Map<String, String> connectorInfoMasterDetailMap, String uri, String token,
                                            int sourceSystem, Set<String> workspacesSet)
            throws InterruptedException, IOException, HttpClientException {
        JsonElement response = null;
        StringBuilder workspaceStringBuilder = new StringBuilder();
        PostMethod postMethod = new PostMethod(uri);

        postMethod.addRequestHeader("Authorization", "Bearer " + token);
        long currentTime = dt.getTime();
        long elapsedTimeFromPrevCall = currentTime - lastAPISubmitTime;
        log.debug("Elapsed time from previous API call :" + elapsedTimeFromPrevCall + " :" + currentTime + " - " + lastAPISubmitTime);
        if (elapsedTimeFromPrevCall > DEFAULT_WAIT_TIME_MS) {
            elapsedTimeFromPrevCall = DEFAULT_WAIT_TIME_MS;
        }
        Thread.sleep(DEFAULT_WAIT_TIME_MS - elapsedTimeFromPrevCall);

        log.info("API :" + uri);

        workspaceStringBuilder.append("{\"workspaces\":[");
        String result = workspacesSet.stream()
                .map(s -> "\"" + s + "\"")
                .collect(Collectors.joining(","));
        workspaceStringBuilder.append(result + "]}");

        postMethod.setRequestHeader("Content-Type", "application/json");
        postMethod.setRequestEntity(new StringRequestEntity(workspaceStringBuilder.toString(), "json", "utf-8"));
        httpclient.executeMethod(postMethod);
        apiCallCount++;
        lastAPISubmitTime = dt.getTime();

        String responseBodyAsString = postMethod.getResponseBodyAsString();
        response = new JsonParser().parse(responseBodyAsString);

        return readHttpCallResponse(connectorInfoMasterDetailMap, uri, token, sourceSystem, postMethod, response,workspacesSet);


    }

    //TODO: Implement retry logic for this method too and validate the TokenExpired
    @Override
    public InputStream getCallForStreamRestClientAPI(Map<String, String> connectorInfoMasterDetailMap, String uri, String token, int sourceSystem)
            throws InterruptedException, IOException, HttpClientException {
        JsonElement response = null;
        GetMethod get = new GetMethod(uri);
        int streamingApiCallCount = 0;

        get.addRequestHeader("Authorization", "Bearer " + token);
        long currentTime = dt.getTime();
        long elapsedTimeFromPrevCall = currentTime - lastAPISubmitTime;
        log.debug("Elapsed time from previous API call :" + elapsedTimeFromPrevCall + " :" + currentTime + " - " + lastAPISubmitTime);
        if (elapsedTimeFromPrevCall > DEFAULT_WAIT_TIME_MS) {
            elapsedTimeFromPrevCall = DEFAULT_WAIT_TIME_MS;
        }
        Thread.sleep(DEFAULT_WAIT_TIME_MS - elapsedTimeFromPrevCall);

        log.info("API :" + uri);
        httpclient.executeMethod(get);
        if (get.getStatusCode() == 404) {
            return null;
        } else if (get.getStatusCode() != 200) {
            JsonElement streamResponse = new JsonParser().parse(get.getResponseBodyAsString());
            if (!Optional.ofNullable(streamResponse).isPresent()) {
                if (response.getAsJsonObject().has("error")) {
                    JsonElement error = response.getAsJsonObject().get("error").getAsJsonObject().get("code");
                    if (error.getAsString().equals("TokenExpired")) {
                        token = fetchToken(connectorInfoMasterDetailMap, sourceSystem, uri);
                        return getCallForStreamRestClientAPI(connectorInfoMasterDetailMap, uri, token, sourceSystem);
                    } else {
                        return null;
                    }
                }
            } else {
                return null;
            }


        }
        lastAPISubmitTime = dt.getTime();
        return get.getResponseBodyAsStream();

    }


    private JsonObject readHttpCallResponse(Map<String, String> connectorInfoMasterDetailMap, String uri, String token, int sourceSystem
            , HttpMethodBase httpMethod, JsonElement response,Set<String> workspacesSet) throws InterruptedException, IOException, HttpClientException {
        //Success scenario
        if ((httpMethod.getName().equals("GET") && httpMethod.getStatusCode() == 200) || (httpMethod.getName().equals("POST") && httpMethod.getStatusCode() == 202)) {
            totalWaitTimeMs = 0;
            return response.getAsJsonObject();
        } else {
            if (!response.isJsonNull() && response.isJsonObject()) {
                log.error(httpMethod.getStatusCode() + " : " + httpMethod.getStatusText() + ": API : " + uri + " Failed with response string :" + response.toString());
                if (httpMethod.getStatusCode() == 500 || httpMethod.getStatusCode() == 429) {
                    // To retry until the MAX_WAIT_TIME_MS limit is exhausted
                    if (totalWaitTimeMs <= MAX_WAIT_TIME_MS) {
                        totalWaitTimeMs = totalWaitTimeMs + waitTimeMs;
                        log.info("Recovery mode. Sleeping for " + waitTimeMs / 1000 + " seconds");
                        Thread.sleep(waitTimeMs);
                        log.info("Resubmitting : " + uri);
                        return getCallRestClientAPI(connectorInfoMasterDetailMap, uri, token, sourceSystem);
                    }
                    // When the retries exceeded MAX_WAIT_TIME_MS
                    else {
                        log.error(httpMethod.getStatusCode() + " : " + httpMethod.getStatusText() + ": API : " + uri + " Failed with response string :" + response.toString());
                        throw new HttpClientException(HttpStatus.REQUEST_TIMEOUT, httpMethod.getStatusCode() + " : " + httpMethod.getStatusText() + " : " + response.toString());
                    }
                }
                // Error indicates client side issues, 4XX series error code
                //TODO: Need to check the retry logic for POST call
                if (response.getAsJsonObject().has("error")) {
                    JsonElement error = response.getAsJsonObject().get("error").getAsJsonObject().get("code");
                    if (error.getAsString().equals("TokenExpired")) {
                        token = fetchToken(connectorInfoMasterDetailMap, sourceSystem, uri);
                        if(httpMethod.getName().equals("GET")) {
                            return getCallRestClientAPI(connectorInfoMasterDetailMap, uri, token, sourceSystem);
                        }
                        else{
                            return postCallRestClientAPI(connectorInfoMasterDetailMap, uri, token, sourceSystem,workspacesSet);
                        }
                    } else {
                        log.error(httpMethod.getStatusCode() + " : " + httpMethod.getStatusText() + ": API : " + uri + " Failed with response string :" + response.toString());
                        return null;
                    }
                } else {
                    log.error(httpMethod.getStatusCode() + " : " + httpMethod.getStatusText() + ": Unable to export the report. Response body : " + httpMethod.getResponseBodyAsString());
                    return null;
                }
            } else {
                log.error(httpMethod.getStatusCode() + " : " + httpMethod.getStatusText() + ": API : " + uri);
                return null;
            }
        }
    }

    /*
    To fetch token from Auth Services
     */
    private String fetchToken(Map<String, String> connectorInfoMasterDetailMap, int extractorID, String uri) {
        log.info("Generating new token : Token Expired for API: " + uri);
        return powerBIAuthentication.getSourceConnectionToken(connectorInfoMasterDetailMap, extractorID);
    }
}
