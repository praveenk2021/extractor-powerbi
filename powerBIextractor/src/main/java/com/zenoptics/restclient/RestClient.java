package com.zenoptics.restclient;

import com.google.gson.JsonObject;
import com.zenoptics.exceptionhandler.HttpClientException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public interface RestClient {

  public JsonObject getCallRestClientAPI(
          Map<String,String> connectorInfoMasterDetailMap, String uri, String token, int sourceSystem)
      throws InterruptedException, IOException, HttpClientException;

  public JsonObject postCallRestClientAPI(
          Map<String,String> connectorInfoMasterDetailMap, String uri, String token, int sourceSystem, Set<String> set)
      throws InterruptedException, IOException, HttpClientException;

  public InputStream getCallForStreamRestClientAPI(
          Map<String,String> connectorInfoMasterDetailMap, String uri, String token, int sourceSystem)
          throws InterruptedException, IOException, HttpClientException;
}
