package com.zenoptics.authservices;

import com.zenoptics.enums.PowerBIGrantType;
import com.zenoptics.exceptionhandler.HttpClientException;
import com.zenoptics.pojo.powerBI.ConnectorInfoMasterDetail;
import com.zenoptics.util.ExtractorsUtil;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
/*
  PowerBI Token generation class, which genrates the token based on Grant Type passed in Connection params.
  For now we support all 3 Grant Types(CMDLET,ROPC,SERVICE_PRINCIPAL) to make it backward compatible.
  TODO:Need to implement token expiration and retry logic.
 */

@Component
public class PowerBIAuthentication implements AuthenticationModes {

  @Autowired
  private ExtractorsUtil extractorsUtil;

  private HttpClient httpclient = new HttpClient();
  private String access_token;
  private static final String USERNAME = "username";
  private static final String PASSWORD = "password";
  private static final String BEARER = "Bearer ";
  private static final String VALUE_BEARER = "Value : Bearer";
  private static final String AUTHORIZATION = ": Authorization";
  private static final String POWERBI_CMDLET_SCRIPT = "powerbi_cmdlet_token.ps1";

  final long timestamp = System.currentTimeMillis();
  Logger log = Logger.getLogger(getClass().getName() + timestamp);

  /*
   To Fetch the token from PowerBI token generation API based on the Grant type passed by client.
  */
  @Override
  public String getSourceConnectionToken(Map<String,String> connectionParamsMap, int extractorID) {
    String grantType = connectionParamsMap.get("grant_type");

    JSONObject authResponse = null;
    String token_url = connectionParamsMap.get("powerBICloud.host");
    String username = connectionParamsMap.get(USERNAME);
    String password = "";
    String clientId = connectionParamsMap.get("client_id");
    String clientSecret = connectionParamsMap.get("client_secret");
    String resource = connectionParamsMap.get("resource");

    if (connectionParamsMap.get(PASSWORD) != null && !connectionParamsMap.get(PASSWORD).equals(""))
      password =
              extractorsUtil.decryptedPassword(connectionParamsMap.get(PASSWORD));

    PostMethod post = new PostMethod(token_url);

    try {
      if (grantType.equals(PowerBIGrantType.PASSWORD_GRANT_TYPE.getGrantType()))
        return fetchTokenForROPCGrantType(
            post, username, password, clientId, clientSecret, resource);

      if (grantType.equals(PowerBIGrantType.CLIENT_CREDENTIALS.getGrantType()))
        return fetchTokenForSPGrantType(post, clientId, clientSecret, resource);

      if (grantType.equalsIgnoreCase(PowerBIGrantType.CMDLET.getGrantType()))
        return getTokenFromCMDLet(username, password);
    } catch (IOException | InterruptedException | JSONException e) {
      throw new HttpClientException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }

    return null;
  }

  @Override
  public Map<String,String> getSourceConnectionParams(int extractorID)
  {
    return extractorsUtil.getSystemConnectionParams(extractorID);
  }

  /*
   To Fetch Token for ROPC grant Type.
  */
  private String fetchTokenForROPCGrantType(
      PostMethod post,
      String username,
      String password,
      String clientId,
      String clientSecret,
      String resource)
          throws IOException, JSONException {
    if (password == null || username == null) {
      return null;
    }

    post.addParameter(USERNAME, username);
    post.addParameter(PASSWORD, password);
    post.addParameter("client_id", clientId);
    post.addParameter("client_secret", clientSecret);
    post.addParameter("resource", resource);
    post.addParameter("grant_type", PowerBIGrantType.PASSWORD_GRANT_TYPE.getGrantType());

    httpclient.executeMethod(post);

    // Reading response from HttpClient
    InputStreamReader inp = new InputStreamReader(post.getResponseBodyAsStream());
    BufferedReader reader = new BufferedReader(inp);
    StringBuilder out = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
      out.append(line);
    }
    JSONObject token = new JSONObject(out.toString());
    access_token = (String) token.get("access_token");
    reader.close();
    return access_token;
  }

  /*
   To Fetch Token for Service Principal grant Type.
  */
  private String fetchTokenForSPGrantType (
      PostMethod post, String clientId, String clientSecret, String resource) throws IOException, JSONException {
    post.addParameter("client_id", clientId);
    post.addParameter("client_secret", clientSecret);
    post.addParameter("resource", resource);
    post.addParameter("grant_type", PowerBIGrantType.CLIENT_CREDENTIALS.getGrantType());

    httpclient.executeMethod(post);

    InputStreamReader inp = new InputStreamReader(post.getResponseBodyAsStream());
    BufferedReader reader = new BufferedReader(inp);
    StringBuilder out = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
      out.append(line);
    }
    JSONObject token = new JSONObject(out.toString());
    access_token = (String) token.get("access_token");
    reader.close();
    return access_token;
  }

  /*
   To Fetch Token for CMDLET grant Type.
  */
  private String getTokenFromCMDLet(String username, String password)
      throws IOException, InterruptedException {
    InputStream inputstream = null;
    InputStreamReader inputstreamreader = null;
    BufferedReader bufferedreader = null;

    Process process = executingPowerShellScript(username, password);
    log.info("Powershell command passed. Reading output");
    inputstream = process.getInputStream();

    inputstreamreader = new InputStreamReader(inputstream);

    bufferedreader = new BufferedReader(inputstreamreader);

    StringBuffer key = new StringBuffer("");
    String line;
    if (bufferedreader.ready()) {
      log.debug("Buffered reader is ready");
      while ((line = bufferedreader.readLine()) != null) {
        if (line.startsWith(VALUE_BEARER)) {
          key.append(line.trim());
          String newline = bufferedreader.readLine();
          while (newline != null && !newline.contains(AUTHORIZATION)) {
            key.append(newline.trim());
            newline = bufferedreader.readLine();
          }
        }
      }
    }

    // Empty token is invalid.
    String keyStr = key.toString();
    if (!keyStr.isEmpty()) {
      access_token = StringUtils.substringAfter(keyStr, BEARER);
    }

    if (inputstream != null & inputstreamreader != null & bufferedreader != null) {
      inputstream.close();
      inputstreamreader.close();
      bufferedreader.close();
    }

    return access_token;
  }

  private Process executingPowerShellScript(String username, String password)
      throws IOException, InterruptedException {
    String fileName = POWERBI_CMDLET_SCRIPT;
    Runtime runtime = Runtime.getRuntime();

    URL res = getClass().getClassLoader().getResource(fileName);
    File file = new File(res.getFile());
    if (!file.exists()) {
      log.error("File does not exist: " + file.getAbsolutePath());
      throw new IOException("File " + fileName + " does not exist");
    }

    String cmds =
        (String) "cmd /c pwsh.exe "
            + file.getAbsolutePath()
            + " -username "
            + username
            + " -password "
            + password;
    log.info("Running powershell command to generate token");
    Process process = runtime.exec(cmds);

    process.getOutputStream().close();
    process.waitFor();

    InputStream inputstream = null;
    InputStreamReader inputstreamreader = null;
    BufferedReader bufferedreader = null;

    String line;
    StringBuffer output = new StringBuffer("");
    if (process.exitValue() != 0) {
      log.error("Powershell command failed");
      inputstream = process.getErrorStream();
      inputstreamreader = new InputStreamReader(inputstream);

      bufferedreader = new BufferedReader(inputstreamreader);
      while ((line = bufferedreader.readLine()) != null) {
        output.append(line);
        output.append("\n");
      }
      process.destroy();
      throw new IOException(output.toString());
    }

    if (inputstream != null & inputstreamreader != null & bufferedreader != null) {
      inputstream.close();
      inputstreamreader.close();
      bufferedreader.close();
    }
    return process;
  }
}
