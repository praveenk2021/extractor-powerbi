package com.zenoptics.authservices;

import com.zenoptics.pojo.powerBI.ConnectorInfoMasterDetail;
import com.zenoptics.util.ExtractorsUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Properties;

public interface AuthenticationModes {

    public String getSourceConnectionToken(Map<String,String> connectionParamsMap, int extractorID);

    public Map<String,String> getSourceConnectionParams(int extractorID);

}