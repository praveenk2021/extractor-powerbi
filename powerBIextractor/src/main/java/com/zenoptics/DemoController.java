package com.zenoptics;

import com.zenoptics.service.PowerBIExtractorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @Autowired
    private PowerBIExtractorService powerBIExtractorService;

    @GetMapping("/begin")
    public void getMessage() {
         powerBIExtractorService.begin(127,false);
    }
}
