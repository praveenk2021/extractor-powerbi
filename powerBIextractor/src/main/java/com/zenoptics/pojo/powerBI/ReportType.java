package com.zenoptics.pojo.powerBI;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@Slf4j
public class ReportType {

    private int report_types_id;
    private String display_value;
    private boolean isActive;
    private String create_date;
    private String update_date;
    private String update_user_id;
}
