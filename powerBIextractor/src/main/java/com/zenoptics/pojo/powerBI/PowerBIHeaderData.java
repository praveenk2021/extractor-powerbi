package com.zenoptics.pojo.powerBI;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@Slf4j
public class PowerBIHeaderData {

  private String headerId;
  private String displayValue;
  private String description;
  private String dataSetId;
  private int reportTypeId;
  private String dataSource;
  private int catagoryId;
  private String createdBy;
  private String modifiedBy;
  private String createdDateTime;
  private String modifiedDateTime;
}
