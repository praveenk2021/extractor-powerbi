package com.zenoptics.pojo.powerBI;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@Slf4j
public class PowerBIDashboard {

  private String id;
  private String displayName;
  private String embedUrl;
  private boolean isReadOnly;
}
