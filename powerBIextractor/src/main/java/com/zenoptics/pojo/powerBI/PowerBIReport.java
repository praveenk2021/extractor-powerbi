package com.zenoptics.pojo.powerBI;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Builder
@Slf4j
public class PowerBIReport {

  private String id;
  private String reportType;
  private String reportName;
  private String webUrl;
  private String embedUrl;
  private String datasetId;
  private String appId;
  private List<String> users;
  private List<String> subscriptions;
}
