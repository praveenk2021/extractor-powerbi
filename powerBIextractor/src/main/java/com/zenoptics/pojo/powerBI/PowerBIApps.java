package com.zenoptics.pojo.powerBI;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@Slf4j
public class PowerBIApps {

    String id;
    String name;
    String lastUpdate;
    String description;
    String publishedBy;
}
