package com.zenoptics.pojo.powerBI;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@Slf4j
public class PowerBIDetailedData {

    private String headerId;
    private String datasetColumnId;
    private String fieldName;
    private String createdDateTime;
    private String modifiedDateTime;
    private int    sourceSystemId;
    private String fieldDetails;
}
