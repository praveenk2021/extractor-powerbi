package com.zenoptics.pojo.powerBI;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import java.sql.Timestamp;

@Data
@Builder
@Slf4j
public class ConnectorInfoMasterDetail {
    private int masterId;
    private int paramSequence;
    private String propertyName;
    private String propertyKey;
    private String propertyValue;
    private Short editable;
    private Timestamp createDate;
    private Timestamp lastUpdateDate;
}
