package com.zenoptics.util;

public enum ExtractorJobTypeEnum {
	RUN_COMPLETE("Run Complete"), RUN("Run");

	private String value;

	/**
	 * Constructor.
	 * 
	 * @param value
	 */
	ExtractorJobTypeEnum(String value) {
		this.value = value;
	}

	/**
	 * Returns the display value for this ExtractorJobTypeEnum.
	 *
	 * @return Display value for this ExtractorJobTypeEnum.
	 */
	public String value() {
		return value;
	}

}
