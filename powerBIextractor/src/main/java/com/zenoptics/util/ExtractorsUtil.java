package com.zenoptics.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.zenoptics.dao.entity.*;
import com.zenoptics.dao.repository.*;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/*
 This Util class is currently tightly coupled with PowerBI extractor.
 In further iteration the Util class and respective DAO repository,POJO classes need to be moved to separate Util JAR altogether.
*/
@Component
public class ExtractorsUtil {

  @Autowired private ConnectorInfoMasterDetailRepository connectorInfoMasterDetailRepository;
  @Autowired private ExtractJobLogRepository extractJobLogRepository;
  @Autowired private CategoryMovedHistoryRepository categoryMovedHistoryRepository;
  @Autowired private CatalogCategoriesTreeRepository catalogCategoriesTreeRepository;
  @Autowired private DataDictionaryRepository dataDictionaryRepository;
  @Autowired private DetailDataRepository detailDataRepository;
  @Autowired private CatalogAppSettingsRepository catalogAppSettingsRepository;
  @Autowired private ReportTypeRepository reportTypeRepository;
  @Autowired private HeaderDataRepository headerDataRepository;
  @Autowired private UserInfoRepository userInfoRepository;
  @Autowired private CatalogCategoriesTreeJson catalogCategoriesTreeJson;
  @Autowired private StatusMasterRepository statusMasterRepository;

  private static final String DISTINGUISHED_NAME = "distinguishedName";
  private static final String CN = "cn";
  private static final String SN = "sn";
  private static final String NAME = "name";
  private static final String UID = "uid";
  private static final String GIVEN_NAME = "givenname";
  private static final String SAMACCOUNTNAME = "samaccountname";
  private static final String USER_PRINCIPAL_NAME = "userPrincipalName";
  private static final String MAIL = "mail";
  private static final String NAMING_CONTEXTS = "namingContexts";
  private static final String ZENOPTICS_PROPERTIES = "zenoptics.properties";
  private static final String ROOT_DOMAIN_NAMING_CONTEXT = "rootDomainNamingContext";
  private static final String EXTRACTORS_USE_ROOTDOMAINNAMINGCONTEXT_FOR_LDAP_SEARCHES =
      "extractors.use.rootdomainnamingcontext.for.ldap.searches";
  private static final String CATALINA_BASE = "catalina.base";
  static final long timestamp = System.currentTimeMillis();
  static Logger log = Logger.getLogger(ExtractorsUtil.class.getName() + timestamp);

  @PersistenceContext private EntityManager entityManager;

  public Map<String, String> getSystemConnectionParams(int sourceId) {
    Map<String, String> connectionParamsMap = new HashMap<>();
    List<LConnectorInfoMasterDetail> lConnectorInfoMasterDetailList =
        connectorInfoMasterDetailRepository.findByMasterId(sourceId);
    for (LConnectorInfoMasterDetail conn : lConnectorInfoMasterDetailList) {
      connectionParamsMap.put(conn.getPropertyKey(), conn.getPropertyValue());
    }
    return connectionParamsMap;
  }

  public int createExtractJOB(
      long timestamp, int sourceSystem, String extractName, boolean runmode) {

    LExtractJobLog lExtractJobLog =
        LExtractJobLog.builder()
            .sourceId(sourceSystem)
            .currentStatus(0)
            .extractStartTime(new Timestamp(timestamp))
            .extractLogFile("logs/" + extractName + timestamp + ".log")
            .jobType(
                runmode
                    ? ExtractorJobTypeEnum.RUN_COMPLETE.value()
                    : ExtractorJobTypeEnum.RUN.value())
            .build();
    return extractJobLogRepository.save(lExtractJobLog).getExtractJobId();
  }

  public void updateJOBStatus(int jobID, int totalUpdates) {
    LExtractJobLog currentLExtractJobLog = extractJobLogRepository.findByExtractJobId(jobID);
    LExtractJobLog lExtractJobLog =
        LExtractJobLog.builder()
            .sourceId(currentLExtractJobLog.getSourceId())
            .currentStatus(1)
            .jobResultSummary("Total Updates :" + totalUpdates)
            .extractJobId(jobID)
            .extractStartTime(currentLExtractJobLog.getExtractStartTime())
            .extractEndTime(new Timestamp(new java.util.Date().getTime()))
            .extractLogFile(currentLExtractJobLog.getExtractLogFile())
            .jobType(currentLExtractJobLog.getJobType())
            .build();
    extractJobLogRepository.save(lExtractJobLog);
  }

  public int fetchCategoryId(String categoryName, int parentCategoryId) {
    AtomicInteger categoryId = new AtomicInteger();
    Optional.ofNullable(
            categoryMovedHistoryRepository.findByCategoryNameAndParentCategoryId(
                categoryName, parentCategoryId))
        .ifPresent(
            value -> {
              categoryId.set(value.getReportCategoriesId());
            });
    if (categoryId.get() == 0) {
      Optional.ofNullable(
              catalogCategoriesTreeRepository.findByCategoryNameAndParentCategoryId(
                  categoryName, parentCategoryId))
          .ifPresent(
              value -> {
                categoryId.set(value.getReportCategoriesId());
              });
    }
    return categoryId.get();
  }

  public Integer createAndSaveANewCategory(String categoryName, int parentCategoryId) {

    return catalogCategoriesTreeRepository
        .save(buildCategoryDetails(categoryName, parentCategoryId))
        .getReportCategoriesId();
  }

  public Map<Integer, LCategoryDetails> fetchCategoriesDetails() {
    Map<Integer, LCategoryDetails> lCategoryDetailsMap = new HashMap<>();
    List<LCategoryDetails> lCategoryDetailsList = (List) catalogCategoriesTreeRepository.findAll();
    lCategoryDetailsList.stream()
        .forEach(
            lCategoryDetails ->
                lCategoryDetailsMap.put(
                    lCategoryDetails.getReportCategoriesId(), lCategoryDetails));
    return lCategoryDetailsMap;
  }

  public Map<Integer, LReportTypes> fetchReportTypes() {
    Map<Integer, LReportTypes> reportTypesMap = new HashMap<>();
    List<LReportTypes> lReportTypesList = (List) reportTypeRepository.findAll();
    lReportTypesList.stream()
        .forEach(lReportTypes -> reportTypesMap.put(lReportTypes.getReportTypeId(), lReportTypes));
    return reportTypesMap;
  }

  public Map<String, LReportHeaderData> fetchReportHeaderData(int sourceSystemId) {
    Map<String, LReportHeaderData> lReportHeaderDataMap = new HashMap<>();
    List<LReportHeaderData> lReportHeaderDataList =
        (List) headerDataRepository.findAllBySourceSystemId(sourceSystemId);
    lReportHeaderDataList.stream()
        .forEach(
            lReportHeaderData ->
                lReportHeaderDataMap.put(lReportHeaderData.getId(), lReportHeaderData));
    return lReportHeaderDataMap;
  }

  public LStatusMaster fetchStatusMasterMapping(int statusMasterId) {
    return statusMasterRepository.findByStatusMasterId(statusMasterId);
  }

  /**
   * This method is used to insert data into Data Dictionary based on the population of data inside
   * Report Detail Data called by various Extractors It first checks if there is a property
   * "populate.bi.dictionary", if it exists and the value is 'yes', it populates the data dictionary
   * if the value is 'no' or anything except 'yes', it will not populate the dictionary If the
   * property does not exist for that extractor, then, by default, the dictionary will be populated
   *
   * @param sourceSystemId
   */
  public void populateDataDictionary(int sourceSystemId) {

    List<LReportDetailData> lReportDetailDataList =
        detailDataRepository.findAllBySourceSystemId(sourceSystemId);
    JsonParser parser = new JsonParser();
    Map<String, Integer> checkDuplicate = new HashMap<>();
    for (LReportDetailData lReportDetailData : lReportDetailDataList) {
      String description = null;
      if (lReportDetailData.getFieldDetails() != null
          && !lReportDetailData.getFieldDetails().equals("")) {
        String fieldDetails = lReportDetailData.getFieldDetails().toString();
        JsonObject field_details = (JsonObject) parser.parse(fieldDetails);
        if (field_details.get("description") != null) {
          description = field_details.get("description").getAsString();
        }
      }
      String iobjnm = lReportDetailData.getIobjnm();
      LDataDictionary lDataDictionary =
          LDataDictionary.builder()
              .sourceSystemId(sourceSystemId)
              .build(); // set the value of dataDictionaryOrigin to 0 when row added from Extractor.

      String fieldId = "";
      if (iobjnm.indexOf('@') > -1) {
        fieldId = iobjnm.substring(iobjnm.indexOf('@') + 1);
        if (fieldId == null || fieldId.equalsIgnoreCase("null")) {
          fieldId = iobjnm.substring(0, iobjnm.indexOf('@'));
        }
        if (fieldId.length() > 255) {
          fieldId = fieldId.substring(0, 255);
        }
        lDataDictionary.setFieldId(fieldId);
      } else {
        fieldId = iobjnm;
        if (fieldId.length() > 255) {
          fieldId = fieldId.substring(0, 255);
        }
        lDataDictionary.setFieldId(fieldId.trim());
      }

      List<LDataDictionary> existinglDataDictionaryList =
          dataDictionaryRepository.findAllByFieldId(fieldId);
      if (existinglDataDictionaryList.size() == 0 && checkDuplicate.get(fieldId) == null) {
        String fieldName = "";
        if (iobjnm.indexOf('@') > -1) {
          if (iobjnm.substring(0, iobjnm.indexOf('@')).length() > 1000) {
            fieldName = iobjnm.substring(0, iobjnm.indexOf('@')).substring(0, 1000);
          } else {
            fieldName = iobjnm.substring(0, iobjnm.indexOf('@'));
          }
        } else {
          String txtlg = lReportDetailData.getTxtlg();
          // if txtlg is null, the field name and field ID will be the same
          if (txtlg != null) {
            if (txtlg.length() > 1000) {
              fieldName = txtlg.substring(0, 1000);
            } else if (txtlg.length() <= 0) {
              fieldName = fieldId;
            } else {
              fieldName = txtlg;
            }
          } else {
            fieldName = fieldId;
          }
        }

        if (fieldName.length() <= 0) {
          fieldName = fieldId;
        }

        // The Map is to keep track of reports duplicate fieldId for a single extractor.
        // SQL entry should not be part of Data Dictionary table.
        lDataDictionary.setFieldName(fieldName.trim());
        lDataDictionary.setStdName(
            fieldName.trim().substring(0, Math.min(fieldName.trim().length(), 255)));
        lDataDictionary.setApproved((Integer) 0);
        if (description != null) lDataDictionary.setDescription(description);
        else lDataDictionary.setDescription("");
        // lDataDictionary.setDescription(fieldName.trim());
        lDataDictionary.setBusinessArea("");
        lDataDictionary.setSameAs("");
        lDataDictionary.setDerivedFrom("");
        lDataDictionary.setLongDesc("");
        lDataDictionary.setLongDesc("");
        lDataDictionary.setDocLink("");
        lDataDictionary.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        lDataDictionary.setModifiedDate(new Timestamp(System.currentTimeMillis()));
        lDataDictionary.setCreatedBy("System");
        lDataDictionary.setModifiedBy("System");
        dataDictionaryRepository.save(lDataDictionary);
        checkDuplicate.put(fieldId, 1);
      } else {
        for (Object o : existinglDataDictionaryList) {
          LDataDictionary lDataDictionary1 = (LDataDictionary) o;
          if (description != null) {
            if (!description.equals(lDataDictionary1.getDescription())) {
              lDataDictionary1.setDescription(description);
              lDataDictionary1.setModifiedBy("System");
              lDataDictionary1.setModifiedDate(new Timestamp(System.currentTimeMillis()));
            } else if (lDataDictionary1.getDescription() == null) {
              lDataDictionary1.setDescription("");
            }
          } else {
            lDataDictionary1.setDescription("");
          }
          dataDictionaryRepository.save(lDataDictionary1);
        }
      }
    }
  }

  // TODO: Need to handle the exceptions properly.
  public static String getAccessTokenForAzureAD(
      String tenantId, String clientId, String clientSecret) {
    System.out.println("Utils:: Fetches the AZURE AD Access Token");
    String access_token = null;
    if (tenantId == null || clientId == null || clientSecret == null) {
      // Properties props = readZenopticsPropertiesFile();
      tenantId = "9ac9d444-9450-4d3c-889b-5d278444d635";
      clientId = "e2ede96a-d623-4ec6-9c1b-1e3b26bafc24";
      clientSecret = "ZKNqYxvH1In8bWevaeGkIojXwhmNRlD6uaIF8pCOomk=";
    }
    System.out.println("Utils:: Zenoptics Property File values for Azure AD");
    System.out.println(
        "Tenant ID = "
            + tenantId
            + ", Client ID = "
            + clientId
            + ", Client Secret = "
            + clientSecret);
    try {
      URLCodec urlCodec = new URLCodec();
      clientSecret = urlCodec.encode(clientSecret);
      Client client = Client.create();
      WebResource webResource =
          client.resource("https://login.microsoftonline.com/" + tenantId + "/oauth2/v2.0/token");
      String payload =
          "client_id="
              + clientId
              + "&"
              + "scope=https://graph.microsoft.com/.default&"
              + "grant_type=client_credentials&"
              + "client_secret="
              + clientSecret;
      ClientResponse response =
          webResource.type("application/x-www-form-urlencoded").post(ClientResponse.class, payload);
      if (response.getStatus() != 200) {
        throw new Exception(
            "Utils:: Failed to fetch Token: HTTP error code : " + response.getStatus());
      }
      String output = response.getEntity(String.class);
      System.out.println("Utils:: Token API Response" + output);
      JSONObject jsonObj = new JSONObject(output);
      access_token = jsonObj.getString("access_token");
      System.out.println("Utils:: Token value = " + access_token);
    } catch (Exception e) {
      log.info("Exception occured with getting AccessToken For AzureAD :"+e.getMessage());
    }
    return access_token;
  }

  // TODO: Need to handle the exceptions properly.
  public static List<String> getAllUsersInAzureADGroupByID(
      String groupId, String tenantId, String clientId, String clientSecret, String access_token) {
    System.out.println("Utils:: Fetching all userPrincipalName for group = " + groupId);
    List<String> usersListForTheGroup = new ArrayList<String>();
    // String access_token=getAccessTokenForAzureAD(tenantId, clientId, clientSecret);
    if (!"".equalsIgnoreCase(groupId)) {
      try {
        Client client = Client.create();
        String uri = "https://graph.microsoft.com/v1.0/groups/" + groupId + "/transitiveMembers";
        WebResource webResource = client.resource(uri);
        ClientResponse response =
            webResource
                .header("Content-Type", "application/json")
                .header("Authorization", access_token)
                .header("Accept", "application/json")
                .get(ClientResponse.class);

        String output = response.getEntity(String.class);
        if (response.getStatus() == 401) {
          JsonParser parser = new JsonParser();
          JsonObject resp = (JsonObject) parser.parse(output);

          if (resp.getAsJsonObject().has("error")) {
            JsonElement error = resp.getAsJsonObject().get("error").getAsJsonObject().get("code");
            if (error.getAsString().equals("InvalidAuthenticationToken")) {
              System.out.println("Generating new token : Token Expired for API: " + uri);
              access_token = getAccessTokenForAzureAD(tenantId, clientId, clientSecret);
              return getAllUsersInAzureADGroupByID(
                  groupId, tenantId, clientId, clientSecret, access_token);
            } else {
              System.err.println(
                  "Failed executing API: "
                      + uri
                      + " : HTTP error code : "
                      + response.getStatus()
                      + " : "
                      + output);
              return null;
            }
          }

        } else if (response.getStatus() != 200) {
          System.err.println(
              "Failed executing API: "
                  + uri
                  + " : HTTP error code : "
                  + response.getStatus()
                  + " : "
                  + output);
          return null;
        }

        JSONObject jsonObj = new JSONObject(output);
        JSONArray jsonArr = (JSONArray) jsonObj.get("value");
        for (int i = 0; i < jsonArr.length(); i++) {
          JSONObject objects = jsonArr.getJSONObject(i);
          String type = objects.getString("@odata.type");
          if (type.contains("user")) {
            String UPN = objects.getString("userPrincipalName");
            // System.out.println("Utils:: userPrincipalName = " + UPN);
            usersListForTheGroup.add(UPN.toLowerCase());
          }
        }
      } catch (Exception e) {
        log.info("Exception occured with getting AllUsers In AzureADGroup :"+e.getMessage());
      }
    }
    return usersListForTheGroup;
  }

  private LCategoryDetails buildCategoryDetails(String categoryName, int parentCategoryId) {
    return LCategoryDetails.builder()
        .categoryName(categoryName)
        .categoryDesc("")
        .isActive(0)
        .parentCategoryId(parentCategoryId)
        .createDate(getTimestamp(""))
        .categorySource("source")
        .build();
  }

  private Timestamp getTimestamp(String timestamp) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
    SimpleDateFormat powerBIFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    Date powerBIDate = null;
    Date parsedDate = null;
    try {
      if (timestamp != "") {
        powerBIDate = powerBIFormat.parse(timestamp);
        parsedDate = dateFormat.parse(dateFormat.format(powerBIDate));
      } else {
        parsedDate = dateFormat.parse(dateFormat.format(new Date()));
      }
    } catch (ParseException e) {
      log.info("Exception occured with getTimestamp :"+e.getMessage());
    }
    return new Timestamp(parsedDate.getTime());
  }

  // TODO: need to correct config file
  public String decryptedPassword(String enc) {
    StandardPBEStringEncryptor decryptor = new StandardPBEStringEncryptor();
    File catalinaBase = (new File(System.getProperty("catalina.base"))).getAbsoluteFile();
    File propertyFile = new File(catalinaBase, "conf/zendigest.properties");
    if (propertyFile.exists()) {

      Properties props = new Properties();
      try {
        props.load(new FileInputStream(propertyFile));
        decryptor.setPassword(props.getProperty("key"));
      } catch (IOException e) {
        log.info("Exception occured with decryptedPassword :"+e.getMessage());
      }
    }

    String decryptedText = "";

    if (enc.substring(0, 3).contentEquals("ENC")) {
      decryptedText = decryptor.decrypt(enc.substring(3));
    } else {
      decryptedText = enc;
    }
    return decryptedText;
  }

  /**
   * The method will only do the Solr Stats. The Solr search index will not be done in this method.
   * For every extractor run, Solr Search Index will only be done if publish is yes. Refer
   * startSolrSearchIndex() for solr search Index.
   */
  public void startSolrIndex() {
    try {
      List<LCatalogAppSettings> result = getAppSettings();

      LCatalogAppSettings settingValues = (LCatalogAppSettings) result.get(0);
      org.apache.commons.httpclient.HttpClient client1 = new HttpClient();

      HttpMethod method1 =
          new GetMethod(
              settingValues.getSearchServerUrl()
                  + "/solr/zen_analytics/dataimport?command=full-import");
      method1
          .getParams()
          .setParameter(
              HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

      try {
        // Execute the method.
        int statusCode = client1.executeMethod(method1);

        if (statusCode != HttpStatus.SC_OK) {
          System.err.println("Method failed: " + method1.getStatusLine());
        }

        // Read the response body.
        byte[] responseBody = method1.getResponseBody();

      } catch (HttpException e) {
        log.info("HttpException occured with startSolrIndex :"+e.getMessage());
      } catch (IOException e) {
        log.info("IOException occured with startSolrIndex :"+e.getMessage());
      } finally {
        // Release the connection.
        method1.releaseConnection();
      }

    } catch (Exception e) {
      log.info("Exception occured with startSolrIndex :"+e.getMessage());
    }
  }

  /**
   * @param emailAddress
   * @return LUserInfo object for the user with the emailAddress in the parameter Returns null if no
   *     user is found with that email address
   */
  public LUserInfo getUserInfoFromEmailAddress(String emailAddress) {
    // System.out.println("Entering getUserInfoFromEmailAddress()");
    boolean userDBLDAP = false;
    LUserInfo lUserInfo = null;
    List<LCatalogAppSettings> results = getAppSettings();
    String adminUser = "";
    String admPassword = "";
    String ldapURL = "";
    String searchBase = "";
    String useLDAP = "";
    String UserLDAPGroup = "";
    String decryptedText = "";
    LdapContext ctx = null;
    for (int i = 0; i < results.size(); i++) {
      LCatalogAppSettings settings = results.get(i);

      adminUser = settings.getLdapAdmUser();
      admPassword = settings.getLdapAdmPassword();
      ldapURL = settings.getLdapServerUrl();
      searchBase = settings.getLdapUserBase();
      useLDAP = settings.getUseLdap();
      UserLDAPGroup = settings.getLdapUsersGroup();
      if (useLDAP.equalsIgnoreCase("on")) {
        userDBLDAP = true;
      }
      StandardPBEStringEncryptor decryptor = new StandardPBEStringEncryptor();
      // TODO: Replace this hardcoded value with getKey() method
      decryptor.setPassword("zenopticsZqIXdDUv");
      if (admPassword.substring(0, 3).contentEquals("ENC")) {
        decryptedText = decryptor.decrypt(admPassword.substring(3));
      } else {
        decryptedText = admPassword;
      }
    }

    if (userDBLDAP) {
      try {
        ctx = generateLdapContext(adminUser, decryptedText, ldapURL);
        String rootDSE = findRootDSE(ctx);
        if (!rootDSE.isEmpty()) {
          SearchControls searchControls =
              createLdapSearchControlsObject(attributesForLDAPUsers, SearchControls.SUBTREE_SCOPE);
          NamingEnumeration<SearchResult> userDetailsResultFromLdap =
              ctx.search(
                  rootDSE,
                  "(&(objectClass=user)(|(userPrincipalName="
                      + emailAddress
                      + ")(mail="
                      + emailAddress
                      + ")))",
                  searchControls);
          while (userDetailsResultFromLdap != null && userDetailsResultFromLdap.hasMore()) {
            Attributes attributesOfTheResultElement =
                userDetailsResultFromLdap.next().getAttributes();
            // DISTINGUISHED_NAME,CN,SN,GIVEN_NAME,SAMACCOUNTNAME,MAIL
            String firstName = "";
            String lastName = "";
            String userId = "";
            if (attributesOfTheResultElement.get(GIVEN_NAME) != null) {
              firstName =
                  (String)
                      getLdapAttributeValueForAttributeName(
                          attributesOfTheResultElement, GIVEN_NAME);
            }
            if (attributesOfTheResultElement.get(SN) != null) {
              lastName =
                  (String) getLdapAttributeValueForAttributeName(attributesOfTheResultElement, SN);
            }
            if (attributesOfTheResultElement.get(SAMACCOUNTNAME) != null) {
              userId =
                  (String)
                      getLdapAttributeValueForAttributeName(
                          attributesOfTheResultElement, SAMACCOUNTNAME);
            }
            lUserInfo =
                LUserInfo.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .userId(userId)
                    .email(emailAddress)
                    .build();
            break;
          }
        }
      } catch (NamingException e) {
        log.info("Exception occured with getting UserInfo From EmailAddress :"+e.getMessage());
      }
    } else {
      List<LUserInfo> listOfDatabaseUsers = userInfoRepository.findByEmail(emailAddress);
      if (listOfDatabaseUsers.size() > 0) {
        lUserInfo = listOfDatabaseUsers.get(0);
      }
    }
    // System.out.println("Exiting getUserInfoFromEmailAddress()");
    return lUserInfo;
  }

  public static String getKey() {
    StandardPBEStringEncryptor decryptor = new StandardPBEStringEncryptor();
    File catalinaBase = (new File(System.getProperty("catalina.base"))).getAbsoluteFile();
    File propertyFile = new File(catalinaBase, "conf/zendigest.properties");
    String key = "";
    if (propertyFile.exists()) {

      Properties props = new Properties();
      try {
        props.load(new FileInputStream(propertyFile));
        key = props.getProperty("key");
      } catch (IOException e) {
        // TODO Auto-generated catch block
        log.info("Exception occured with getKey :"+e.getMessage());
      }
    }
    return key;
  }

  private List<LCatalogAppSettings> getAppSettings() {
    return (List) catalogAppSettingsRepository.findAll();
  }

  private static LdapContext generateLdapContext(
      String adminUser, String decryptedText, String ldapURL) throws NamingException {
    LdapContext ldapContext = null;
    Hashtable<String, String> env = new Hashtable<String, String>();
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.SECURITY_AUTHENTICATION, "Simple");
    env.put(Context.SECURITY_PRINCIPAL, adminUser);
    env.put(Context.SECURITY_CREDENTIALS, decryptedText);
    env.put(Context.PROVIDER_URL, ldapURL);
    env.put(Context.REFERRAL, "follow");
    // Enable connection pooling
    env.put("com.sun.jndi.ldap.connect.pool", "true");
    ldapContext = new InitialLdapContext(env, null);
    return ldapContext;
  }

  private String findRootDSE(LdapContext context) {
    String contextAttribute = determineNamingContextChoice();
    String domainName = "";
    try {
      Attributes attributes = context.getAttributes("", new String[] {contextAttribute});
      Attribute attribute = attributes.get(contextAttribute);
      NamingEnumeration<?> all = attribute.getAll();
      while (all.hasMore()) {
        String next = (String) all.next();
        domainName = next;
        if (domainName.startsWith("DC=")) {
          break;
        }
      }
    } catch (NamingException e) {
      log.info("Exception occured with findRootDSE :"+e.getMessage());
    }
    return domainName;
  }

  private String determineNamingContextChoice() {
    boolean useRootDomainNamingContextAsRootDse = false;
    String namingContextValue = NAMING_CONTEXTS;
    // TODO: remove the hardcoded value true after testing
    String useRootDomainNamingContextProperty = "true";
    // getPropertyFromAPropertyFile(EXTRACTORS_USE_ROOTDOMAINNAMINGCONTEXT_FOR_LDAP_SEARCHES);
    if (!useRootDomainNamingContextProperty.isEmpty()) {
      useRootDomainNamingContextAsRootDse = Boolean.valueOf(useRootDomainNamingContextProperty);
    }
    if (useRootDomainNamingContextAsRootDse) {
      namingContextValue = ROOT_DOMAIN_NAMING_CONTEXT;
    }
    return namingContextValue;
  }

  private SearchControls createLdapSearchControlsObject(String[] returningAttributes, int scope) {
    SearchControls searchControls = new SearchControls();
    searchControls.setSearchScope(scope);
    searchControls.setReturningAttributes(returningAttributes);
    return searchControls;
  }

  private Object getLdapAttributeValueForAttributeName(Attributes attributes, String attributeName)
      throws NamingException {
    return attributes.get(attributeName).get();
  }

  private String[] attributesForLDAPUsers = {
    DISTINGUISHED_NAME, CN, SN, GIVEN_NAME, SAMACCOUNTNAME, MAIL
  };

  public static String getPropertyFromAPropertyFile(String propertyName) {
    Properties props = getPropertiesFileByName(ZENOPTICS_PROPERTIES);
    String propertyValue = "";
    if (props.containsKey(propertyName)) {
      propertyValue = props.getProperty(propertyName);
    }
    return propertyValue;
  }

  private static Properties getPropertiesFileByName(String fileName) {
    File catalinaBase = (new File(System.getProperty(CATALINA_BASE))).getAbsoluteFile();
    File propertyFile = new File(catalinaBase, fileName);
    // FileInputStream stream;
    Properties props = new Properties();
    try (FileInputStream stream = new FileInputStream(propertyFile)) {
      props.load(stream);
    } catch (IOException e) {
      log.info("Exception occured with get Properties File ByName :"+e.getMessage());
    }
    return props;
  }


  /*
    For publish after Extraction
    this utility method generates a JSON string representing the
    folder structure to be shown on click of Category button. But it
    does not filter the tree based on the reports a user is authorized to access.
    The authorization filtering happens on logon/refresh of browser using getCategoryTileData()
    It populates three sections.
    1.CategoryTree
    2.ReportTypeTree
    3.GetAllReportsJSON
   */

  @SuppressWarnings({"rawtypes", "unchecked"})
  public Boolean generateJsonStructureFromCategoryTree() {

    LCategoryDetails rootCategory =
        catalogCategoriesTreeRepository.findAllByParentCategoryId(-1).get(0);
    String sortType = "A_Z";
    if (rootCategory.getSortType() != null) {
      sortType = rootCategory.getSortType();
    }
    String sql =
            "select cat.reportCategoriesId as categoryId, cat.categoryName as categoryName, cat.categoryDesc as categoryDesc, "
                    + " cat.sortType as sortType, cat.isEnabledForWhatsNew as isEnabledForWhatsNew, cat.categorySource as categorySource"
            + " from LCategoryDetails  cat "
            + " where  cat.parentCategoryId = :parentCategoryId ";
    sql = sql + getOrderByClauseBasedOnSortType(sortType, "cat.categoryName", "cat.createDate");
    Session session = entityManager.unwrap(Session.class);
    Transaction tx = session.beginTransaction();
    List finalList = new ArrayList();
    LCatalogCategoryTreeJson categoryTreeJson = null;
    try {
      Query query =
          session
              .createQuery(sql)
              .setParameter("parentCategoryId", rootCategory.getReportCategoriesId());
      query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List data = query.list();
      for (Object item : data) {
        Map row = (Map) item;
        String subCatSortType = "A_Z";
        if (row.get("sortType") != null) {
          subCatSortType = row.get("sortType").toString();
        }
        row.put("sortType", subCatSortType);
        row = getCategoryChildren(
                Integer.valueOf(row.get("categoryId").toString()), row, session, sortType);
        if (row != Collections.EMPTY_MAP) {
          finalList.add(row);
        }
      }
      Map mapJson = new HashMap();
      if (rootCategory.getSortType() != null) {
        sortType = rootCategory.getSortType();
      }
      mapJson.put("sortType", sortType);
      mapJson.put("categories", finalList);
      Gson gson = new Gson();
      String jsonStructure = gson.toJson(mapJson);

      LCatalogCategoryTreeJson lCatalogCategoryTreeJson =
          catalogCategoriesTreeJson.findByType("CategoryTree");

      // Checking if the row exists or not.
      if (lCatalogCategoryTreeJson == null) {
        // If no row exists in the database, create new object and store the right properties.
        lCatalogCategoryTreeJson =
            LCatalogCategoryTreeJson.builder().type("CategoryTree").build();
      }

      lCatalogCategoryTreeJson.setJsonStructure(jsonStructure);

      session.saveOrUpdate(lCatalogCategoryTreeJson);
      tx.commit();
      // ExtractorsUtil.startSolrIndex();
      startSolrIndex();

      generateJsonStructureFromReportTypes();
      generateAllReportsJSON();
      // updatePublishStatus();
    } catch (Exception e) {
      log.info("Exception occured with generate JsonStructure From CategoryTree :"+e.getMessage());
      tx.rollback();
      return false;
    } finally {
      if (session.isOpen()) {
        session.close();
      }
    }

    return true;
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  private Map getCategoryChildren(int categoryID, Map row, Session session, String sortType) {

    String sql1 =
        "select header.id as REPORT_HEADER_ID, "
            + "  header.lConnectorInfoMaster.connectorId as SOURCE_SYSTEM_ID, "
            + "  header.displayValue as DISPLAY_VALUE,"
            + "  header.description as DESCRIPTION,"
            + "  header.reportKeywords as REPORT_KEYWORDS,"
            + "  header.reportOwner as REPORT_OWNER,"
            + "  header.reportPreviewImageUrl as REPORT_PREVIEW_IMAGE_URL,"
            + "  header.dataSource as DATA_SOURCE,"
            + "  header.lCategoryDetails.reportCategoriesId as REPORT_CATEGORIES_ID,"
            + "  header.lCategoryDetails.categoryName as ReportCategoryName,"
            + "  header.lReportTypes.reportTypeId as REPORT_TYPES_ID,"
            + "  header.lReportTypes.displayValue as ReportTypeName, "
            + "  header.updateDate as UPDATE_DATE,"
            + "  header.lReportTypes.displayValue as report_DISPLAY_VALUE,"
            + "  header.lReportTypes.iconUrl as ICON_URL,"
            + "  header.lConnectorInfoMaster.connectorName as connectorName,"
            + "  mapping.reportUrl as reportURL, "
            + "  ratings.averageRating as averageRating, "
            + "  header.updateDate as UPDATE_DATE,"
            + "  header.createDate as CREATE_DATE,"
            + "  usage.timestamp as lastUsed, "
            + "  certified.customAttributeValue as reportCertified "
            // + " from LCatalogUserObjectAccess ACS " +
            + " FROM LReportHeaderData as header "
            + " join header.lReportTypes.tReportTypeReportUrlMappingSet as mapping "
            + " left join header.tReportHeaderCalculatedValuesSet as ratings "
            + "	left join header.tReportHeaderCustomAttrsSet as certified "
            + " left join certified.lCustomAttributes lca "
            + " left join header.tLatestReportUsageSet  as usage "
            + "   where header.lCategoryDetails = "
            + categoryID
            + "   and (header.isDeleted = 0 OR header.isDeleted IS NULL) "
            + "   and header.securityWhitelisted = 0 "
            + "   and header.lConnectorInfoMaster.connectorId= mapping.lConnectorInfoMaster.connectorId"
            + "	  AND (lca.customattribute = 'approved' OR lca.customattribute IS NULL) ";
    sql1 =
        sql1
            + getOrderByClauseBasedOnSortType(sortType, "header.displayValue", "header.updateDate");
    try {
      // session = HibernateSessionFactory.getSession();
      Query query1 = session.createQuery(sql1);
      query1.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List data1 = query1.list();

      String sqlForGetingDeltaReports =
          "SELECT a.lReportHeaderData.id AS report_header_id, "
              + "	a.lReportHeaderData.lConnectorInfoMaster.connectorId AS source_system_id, "
              + "	a.lCategoryDetails.reportCategoriesId AS category_id "
              + "	FROM LReportHeaderDeltaLinks a WHERE a.lCategoryDetails.reportCategoriesId = :categoryId AND a.lReportHeaderData.securityWhitelisted = 0 AND "
              + "(a.lReportHeaderData.isDeleted = 0 OR a.lReportHeaderData.isDeleted IS NULL)";
      sqlForGetingDeltaReports =
          sqlForGetingDeltaReports
              + getOrderByClauseBasedOnSortType(
                  sortType, "a.lReportHeaderData.displayValue", "a.lReportHeaderData.updateDate");
      Query queryForGettingDeltaReports =
          session
              .createQuery(sqlForGetingDeltaReports)
              .setParameter("categoryId", categoryID)
              .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List deltaReportsList = queryForGettingDeltaReports.list();
      if (deltaReportsList.size() > 0) {
        for (Object deltaReport : deltaReportsList) {
          Map rowOfDeltaReport = (Map) deltaReport;
          String sqlForGettingReport =
              "select header.id as REPORT_HEADER_ID, "
                  + "	 header.lConnectorInfoMaster.connectorId as SOURCE_SYSTEM_ID, "
                  + "  header.displayValue as DISPLAY_VALUE,"
                  + "  header.description as DESCRIPTION,"
                  + "  header.reportKeywords as REPORT_KEYWORDS,"
                  + "  header.reportOwner as REPORT_OWNER,"
                  + "  header.reportPreviewImageUrl as REPORT_PREVIEW_IMAGE_URL,"
                  + "  header.dataSource as DATA_SOURCE,"
                  + "  c.reportCategoriesId as REPORT_CATEGORIES_ID,"
                  + "  c.categoryName as ReportCategoryName,"
                  + "  header.lReportTypes.reportTypeId as REPORT_TYPES_ID,"
                  + "  header.lReportTypes.displayValue as ReportTypeName, "
                  + "  header.updateDate as UPDATE_DATE,"
                  + "  header.lReportTypes.displayValue as report_DISPLAY_VALUE,"
                  + "  header.lReportTypes.iconUrl as ICON_URL,"
                  + "  header.lConnectorInfoMaster.connectorName as connectorName,"
                  + "  mapping.reportUrl as reportURL, "
                  + "  ratings.averageRating as averageRating, "
                  + "  header.updateDate as UPDATE_DATE,"
                  + "  header.createDate as CREATE_DATE,"
                  + "  usage.timestamp as lastUsed, "
                  + "  certified.customAttributeValue as reportCertified "
                  + " FROM LReportHeaderData as header "
                  + " join header.lReportTypes.tReportTypeReportUrlMappingSet as mapping "
                  + " left join header.tReportHeaderCalculatedValuesSet as ratings "
                  + "	left join header.tReportHeaderCustomAttrsSet as certified"
                  + "	left join certified.lCustomAttributes lca "
                  + " left join header.tLatestReportUsageSet as usage "
                  + " left join header.lCategoryDetails c"
                  + " WHERE  header.id = :reportHeaderId"
                  + " and header.lConnectorInfoMaster.connectorId = :connectorId"
                  + "   and  header.lConnectorInfoMaster.connectorId= mapping.lConnectorInfoMaster.connectorId"
                  + "	AND c.reportCategoriesId = :categoryId"
                  + "	AND (lca.customattribute = 'approved' OR lca.customattribute IS NULL)";
          Query queryForGettingDeltaReport =
              session
                  .createQuery(sqlForGettingReport)
                  .setParameter("reportHeaderId", rowOfDeltaReport.get("report_header_id"))
                  .setParameter("connectorId", rowOfDeltaReport.get("source_system_id"))
                  .setParameter("categoryId", rowOfDeltaReport.get("category_id"))
                  .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
          data1.addAll(queryForGettingDeltaReport.list());
        }
      }

      row.put("reports", data1);

      String sql2 =
          "select count(types.iconUrl) as reportTypeCount,lower(types.iconUrl) as reportIconPath,types.displayValue as reportTypeName"
              + "	from LReportHeaderData header"
              + "	join header.lReportTypes as types"
              + "		where types.reportTypeId = header.lReportTypes.reportTypeId "
              + "		and header.lCategoryDetails =  "
              + categoryID
              + "		group by types.iconUrl,types.displayValue ";

      Query query2 = session.createQuery(sql2);
      query2.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      row.put("reportTypes", query2.list());

      String subCatSql =
          "select cat.reportCategoriesId as categoryID, cat.categoryName as categoryName, cat.categoryDesc as categoryDesc, "
              + " cat.sortType as sortType, cat.isEnabledForWhatsNew as isEnabledForWhatsNew, cat.categorySource as categorySource"
              + " from LCategoryDetails cat "
              + " where  cat.parentCategoryId = :categoryId";
      subCatSql =
          subCatSql
              + getOrderByClauseBasedOnSortType(sortType, "cat.categoryName", "cat.createDate");
      Query subCatSqlQuery = session.createQuery(subCatSql).setParameter("categoryId", categoryID);
      subCatSqlQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List subCategoriesList = subCatSqlQuery.list();
      if (subCategoriesList.size() >= 1) {
        List SubCatList = new ArrayList();
        for (int i = 0; i < subCategoriesList.size(); i++) {
          Map subCat = (Map) subCategoriesList.get(i);
          Map<String, Object> subCatObj = new HashMap<String, Object>();
          subCatObj.put("categoryID", subCat.get("categoryID"));
          subCatObj.put("categoryName", subCat.get("categoryName"));
          subCatObj.put("categoryDesc", subCat.get("categoryDesc"));
          subCatObj.put("categorySource", subCat.get("categorySource"));

          Map<Object, Object> childRows = new HashMap<Object, Object>();
          String subCatSortType =
              subCat.get("sortType") != null ? subCat.get("sortType").toString() : "A_Z";
          subCatObj.put("sortType", subCatSortType);
          Map row1 =
              getCategoryChildren(
                  Integer.valueOf(subCat.get("categoryID").toString()),
                  childRows,
                  session,
                  subCatSortType);
          if (!row1.isEmpty()) {
            subCatObj.put("children", row1);
            SubCatList.add(subCatObj);
          }
        }
        if (!SubCatList.isEmpty()) {
          row.put("subCategory", SubCatList);
        }
      }
    } catch (HibernateException e) {
      log.info("Exception occured with getting Category Children :"+e.getMessage());
    } finally {
      /*
       * if (session.isOpen()) { session.close(); }
       */
    }
    List reportRow = (List) row.get("reports");
    if (reportRow.isEmpty() && row.get("subCategory") == null) {
      return Collections.EMPTY_MAP;
    }
    return row;
  }

  // To generate JSON structure for report types.
  @SuppressWarnings({"rawtypes", "unchecked"})
  public Boolean generateJsonStructureFromReportTypes() {

    // List to hold the report types and the reports under it.
    List reportTypesList = new ArrayList<Object>();
    // Gson object to convert list to json objects.
    Gson reportTypesGSON = new Gson();
    // To hold the json string.
    String reportTypesJSONString = "";
    // GetTileDataByType returns the List object containing the report types and its reports.
    // Sending the currently logged user.
    reportTypesList = getTileDataByType();

    // Checking if the report types list is empty.
    if (reportTypesList == null) {
      return false;
    } else {
      // If not empty, converting the list to JSON, then converting the json to string and setting
      // the string.
      reportTypesJSONString = reportTypesGSON.toJson(reportTypesList);
    }

    // Getting session to save LCataglogCategoryTreeJson Object.
    Session session = entityManager.unwrap(Session.class);

    // To hold the existing or new LCatalogCategoryTreeJson Object.
    LCatalogCategoryTreeJson lCatalogCategoryTreeJsonObj = null;

    LCatalogCategoryTreeJson lCatalogCategoryTreeJson =
        catalogCategoriesTreeJson.findByType("ReportTypeTree");

    // Checking if the row exists or not.
    if (lCatalogCategoryTreeJson == null) {
      // If no row exists in the database, create new object and store the right properties.
      lCatalogCategoryTreeJson = LCatalogCategoryTreeJson.builder().type("ReportTypeTree").build();
    }

    lCatalogCategoryTreeJson.setJsonStructure(reportTypesJSONString);

    Transaction tx = session.beginTransaction();

    try {
      // Saving the object to the database.
      session.saveOrUpdate(lCatalogCategoryTreeJson);

      tx.commit();

    } catch (Exception e) {
      log.info("Exception occured with generate JsonStructure From ReportTypes :"+e.getMessage());
      tx.rollback();
      return false;
    } finally {
      // if session is open, closing the session.
      if (session.isOpen()) {
        session.close();
      }
    }

    // Return true if successfully executed.
    return true;
  }

  // Function to generate JSON structure for All reports.

  public boolean generateAllReportsJSON() {
    // Session with database connection.
    Session session = entityManager.unwrap(Session.class);

    // Holds the return value.
    Map row = new HashMap();

    // Query to fetch all the reports that have access in Zenoptics.
    // ACS WHERE ACS.userId = :userId OR ACS.userId = 'all_users'
    String reportsForUser =
        "SELECT DISTINCT CONCAT(reportHeaderId, '-', CAST(sourceId AS string)) "
            + "	FROM LCatalogUserObjectAccess ACS ";

    // Fetching the results from the query for all the reports in database.
    // List<String> reportsListForUser = session.createQuery(reportsForUser).setParameter("userId",
    // request.getSession().getAttribute("zenuser").toString().toUpperCase()).list();
    List<String> reportsListForUser = session.createQuery(reportsForUser).list();

    // Query string to fetch all reports.
    String sql1 =
        "select usage.timestamp AS lastUsed,header.id as REPORT_HEADER_ID,"
            + " header.lConnectorInfoMaster.connectorId as SOURCE_SYSTEM_ID, "
            + "	certified.customAttributeValue as reportCertified,"
            + "	ratings.averageRating AS averageRating,"
            + "  header.displayValue as DISPLAY_VALUE,"
            + "  header.description as DESCRIPTION,"
            + "  header.reportKeywords as REPORT_KEYWORDS,"
            + "  header.reportOwner as REPORT_OWNER,"
            + "  header.reportPreviewImageUrl as REPORT_PREVIEW_IMAGE_URL,"
            + "  header.dataSource as DATA_SOURCE,"
            + "  header.lCategoryDetails.reportCategoriesId as REPORT_CATEGORIES_ID,"
            + "  header.lCategoryDetails.categoryName as ReportCategoryName,"
            + "  header.lReportTypes.reportTypeId as REPORT_TYPES_ID,"
            + "  header.lReportTypes.displayValue as ReportTypeName, "
            + "  header.updateDate as UPDATE_DATE,"
            + "  header.createDate as CREATE_DATE,"
            + "  usage.timestamp as lastUsed, "
            + "  header.lReportTypes.displayValue as report_DISPLAY_VALUE,"
            + "  header.lReportTypes.iconUrl as ICON_URL,"
            + "	header.lConnectorInfoMaster.connectorName as connectorName,"
            + "	header.createMode as createMode,"
            + "	header.createUserId as createUserId,"
            + "	header.reportPreviewImageUrl as reportPreviewImage,"
            + "  mapping.reportUrl as reportURL "
            +
                " FROM LReportHeaderData as header "
                + "  join header.lReportTypes.tReportTypeReportUrlMappingSet as mapping"
                + "	left join header.tLatestReportUsageSet  as usage"
                + "	left join header.tReportHeaderCustomAttrsSet AS certified"
                + "  left join certified.lCustomAttributes lca"
                + "	left join header.tReportHeaderCalculatedValuesSet as ratings"
            +
            " where CONCAT(header.id, '-', CAST(header.lConnectorInfoMaster.connectorId AS string)) IN (:reportsList)"
            + "  and header.securityWhitelisted = 0 "
            + "  and (header.isDeleted = 0 OR header.isDeleted IS NULL) "
            +
            " and header.lConnectorInfoMaster.connectorId= mapping.lConnectorInfoMaster.connectorId"
            + " AND (lca.customattribute = 'approved' OR lca.customattribute IS NULL) "
            + " GROUP BY "
            + "  usage.timestamp, header.id, "
            + "  header.lConnectorInfoMaster.connectorId, "
            + "	certified.customAttributeValue,"
            + "  ratings.averageRating,"
            + "  header.displayValue,"
            + "  header.description,"
            + "  header.reportKeywords,"
            + "  header.reportOwner,"
            + "  header.reportPreviewImageUrl,"
            + "  header.dataSource,"
            + "  header.lCategoryDetails.reportCategoriesId,"
            + "  header.lCategoryDetails.categoryName,"
            + "  header.lReportTypes.reportTypeId,"
            + "  header.lReportTypes.displayValue, "
            + "  header.updateDate,"
            + "  header.createDate,"
            + "  header.lReportTypes.displayValue,"
            + "  header.lReportTypes.iconUrl,"
            + "	header.lConnectorInfoMaster.connectorName,"
            + "	header.createMode,"
            + "	header.createUserId,"
            + "  header.reportPreviewImageUrl,"
            + "  mapping.reportUrl "
            +
            "	ORDER BY header.displayValue ASC";
    try {

      // To hold the all reports query results.
      List data1 = new ArrayList();

      // Size of the list of query results.
      int size = reportsListForUser.size(),
          tempSize = reportsListForUser.size(),
          subListCount = 0,
          parameterCount = 0,
          limit = 2000;

      // Restricting the number of reports in the query to 2000 and looping through to get the
      // results.
      while (tempSize > limit && subListCount < size) {
        Query query1 = session.createQuery(sql1);
        query1.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        query1.setParameterList(
            "reportsList", reportsListForUser.subList(subListCount, subListCount + limit - 1));
        data1.addAll(query1.list());

        subListCount += limit;
        tempSize -= limit;
      }

      // Final call to get the left over reports.
      if (size != 0 && tempSize != 0) {
        Query query1 = session.createQuery(sql1);
        query1.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        query1.setParameterList("reportsList", reportsListForUser.subList(subListCount, size));
        data1.addAll(query1.list());
      }

      // Fetch report types
      String getReportTypes =
          "SELECT reportTypes.reportTypeId as reportTypeId, reportTypes.displayValue as displayValue, reportTypes.description as description FROM "
              + "	  LReportTypes reportTypes WHERE reportTypes.isActive = 1";
      Query queryForReportTypes = session.createQuery(getReportTypes);
      queryForReportTypes.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List reportTypesList = queryForReportTypes.list();

      // fetch report categories
      String getReportCategories =
          "SELECT reportCategories.reportCategoriesId as reportCategoriesId, reportCategories.categoryName as categoryName FROM "
              + "	   LCategoryDetails reportCategories ORDER BY reportCategories.categoryName, reportCategories.reportCategoriesId";
      Query queryForReportCategories = session.createQuery(getReportCategories);
      queryForReportCategories.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List reportCategoriesList = queryForReportCategories.list();

      // fetch status master
      String getStatusMaster =
          "SELECT statusMaster.statusMasterId as statusMasterId, statusMaster.displayValue as displayValue FROM LStatusMaster statusMaster";
      Query queryForStatusMaster = session.createQuery(getStatusMaster);
      queryForStatusMaster.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List statusMasterList = queryForStatusMaster.list();

      // fetch source systems for various report types
      String getSourceSystemDetailsForVariousReportTypes =
          "select reportType.reportTypeId as reportTypeId, reportType.displayValue as displayValue, connector.connectorName as connectorName, connector.connectorId as connectorId"
              + " from LReportTypes as reportType join reportType.tReportTypeReportUrlMappingSet as mapping join mapping.lConnectorInfoMaster as connector";
      Query queryForSourceSystemDetails =
          session.createQuery(getSourceSystemDetailsForVariousReportTypes);
      queryForSourceSystemDetails.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      List sourceSystemList = queryForSourceSystemDetails.list();

      // long startTime = System.currentTimeMillis();

      // Getting the users using, last one week usage and last three months usage from solr.
      Map moreReportDetails = getDataFromSolr();

      // Looping through the reports list and adding the users using, last one week usage and last
      // three months usage from solr.
      for (int i = 0; i < data1.size(); i++) {
        Map tempData1 = (Map) data1.get(i);

        Map tempMoreReportDetails =
            (Map)
                moreReportDetails.get(
                    tempData1.get("REPORT_HEADER_ID") + "-" + tempData1.get("SOURCE_SYSTEM_ID"));

        if (tempMoreReportDetails != null) {
          ((Map) data1.get(i)).put("usersUsing", tempMoreReportDetails.get("usersUsing"));
          ((Map) data1.get(i))
              .put("lastOneWeekUsage", tempMoreReportDetails.get("lastOneWeekUsage"));
          ((Map) data1.get(i))
              .put("lastThreeMonthsCount", tempMoreReportDetails.get("lastThreeMonthsCount"));
        }
      }

      // Adding all the fetched results to response JSON.
      row.put("reports", data1);
      row.put("reportTypes", reportTypesList);
      row.put("categories", reportCategoriesList);
      row.put("statusMaster", statusMasterList);
      row.put("sourceSystems", sourceSystemList);

      // To hold the DB row.
      LCatalogCategoryTreeJson categoryTreeJson = null;

      Transaction tx = session.beginTransaction();

      LCatalogCategoryTreeJson lCatalogCategoryTreeJson =
          catalogCategoriesTreeJson.findByType("GetAllReportsJSON");

      // Checking if the row exists or not.
      if (lCatalogCategoryTreeJson == null) {
        // If no row exists in the database, create new object and store the right properties.
        lCatalogCategoryTreeJson =
            LCatalogCategoryTreeJson.builder().type("GetAllReportsJSON").build();
      }

      // GSON object to convert MAP to JSON String.
      Gson gson = new Gson();

      // String to hold the json structure.
      String jsonStructure = gson.toJson(row);

      // Setting the DB row with the new json structure.
      lCatalogCategoryTreeJson.setJsonStructure(jsonStructure);

      // Saving or updating to the DB.
      session.saveOrUpdate(lCatalogCategoryTreeJson);

      tx.commit();

    } catch (Exception e) {
      log.info("Exception occured with generate All Reports JSON :"+e.getMessage());
      return false;
    } finally {
      if (session.isOpen()) {
        session.close();
      }
    }

    return true;
  }

  public Map getDataFromSolr() {
    Long startTime = System.currentTimeMillis();

    Map<String, Object> returnMap = new HashMap<String, Object>();

    Session session = null;

    try {

      session = entityManager.unwrap(Session.class);
      ;
      String solrUrl = "";

      // Fetching solr details from database.

      String solrSettingQueryString = "SELECT searchServerUrl AS solr FROM LCatalogAppSettings";

      Query solrSettingQuery = session.createQuery(solrSettingQueryString);

      solrSettingQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

      List solrSettingQueryResult = solrSettingQuery.list();

      if (solrSettingQueryResult != null && solrSettingQueryResult.size() > 0) {
        solrUrl = (String) ((Map) solrSettingQueryResult.get(0)).get("solr");
      }

      // String urlString =
      // "http://13.52.168.148:8983/solr/zen_analytics/select?q=dimension%3AzenTotalReports&rows=10000&fl=reportHeaderId%2C+connectorId%2C+lastOneWeekCount%2C+lastThreeMonthsCount%2C+usageCount&wt=json&indent=true";
      String urlString =
          solrUrl
              + "/solr/zen_analytics/select?"
              + "q=dimension%3AzenTotalReports"
              // +
              // "&fq=reportHeaderId%3A%22views%2FCashFlowDashboard_RunningBalances%2FRunningBalances%22"
              // + "&fq=connectorId%3A171"
              + "&rows=10000"
              + "&fl=reportHeaderId%2C+connectorId%2C+lastOneWeekCount%2C+lastThreeMonthsCount%2C+noOfUsersUsedReport"
              + "&wt=json"
              + "&indent=true";

      URL urlObject = new URL(urlString);

      HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();

      conn.setRequestProperty("Content-Type", "application/json");
      conn.setRequestProperty("Accept", "application/json");
      // conn.setRequestProperty("X-Tableau-Auth", token);
      conn.setRequestMethod("GET");
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.connect();

      StringBuilder stringBuilder = new StringBuilder();

      if (conn.getResponseCode() == 200) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        // Adding the output to a StringBuilder.
        String line = null;
        while ((line = reader.readLine()) != null) {
          stringBuilder.append(line + "\n");
        }
      }

      JSONParser parser = new JSONParser();
      org.json.simple.JSONObject jsonObj = null;
      org.json.simple.JSONArray jsArray = null;

      if (stringBuilder != null && stringBuilder.length() > 0) {
        jsonObj = (org.json.simple.JSONObject) parser.parse(stringBuilder.toString());
      }

      if (jsonObj != null
          && jsonObj.containsKey("response")
          && ((org.json.simple.JSONObject) jsonObj.get("response")).containsKey("docs")) {
        jsArray =
            (org.json.simple.JSONArray)
                ((org.json.simple.JSONObject) jsonObj.get("response")).get("docs");
      }

      if (jsArray != null && jsArray.size() > 0) {
        for (int i = 0; i < jsArray.size(); i++) {
          org.json.simple.JSONObject tempJSObj = (org.json.simple.JSONObject) jsArray.get(i);

          Map<String, Long> tempMap = new HashMap<String, Long>();

          if (tempJSObj != null) {
            tempMap.put(
                "usersUsing",
                (tempJSObj.get("noOfUsersUsedReport")) != null
                    ? (long) tempJSObj.get("noOfUsersUsedReport")
                    : 0);
            tempMap.put(
                "lastOneWeekUsage",
                (tempJSObj.get("lastOneWeekCount")) != null
                    ? (long) tempJSObj.get("lastOneWeekCount")
                    : 0);
            tempMap.put(
                "lastThreeMonthsCount",
                (tempJSObj.get("lastThreeMonthsCount")) != null
                    ? (long) tempJSObj.get("lastThreeMonthsCount")
                    : 0);

            returnMap.put(
                tempJSObj.get("reportHeaderId") + "-" + tempJSObj.get("connectorId"), tempMap);
          }
        }
      }

    } catch (Exception e) {
      log.info("Exception occured with Getting Data From Solar :"+e.getMessage());
    } finally {
      session.close();
    }

    Long endTime = System.currentTimeMillis();

    System.out.println("Total Time: " + (endTime - startTime) + "ms");

    return returnMap;
  }

  // Function to generate list of report types and reports under it.
  public List getTileDataByType() {
    // Report types query.
    String sql =
        "select reportTypeId as categoryID, iconUrl as reportIconPath, displayValue as categoryName,description as categoryDesc"
            + "   from LReportTypes ";
    Session session = null;
    // Final list to return.
    List finalList = new ArrayList();
    try {
      session = entityManager.unwrap(Session.class);
      Query query = session.createQuery(sql);
      query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      // data has the result from the report types query execution.
      List data = query.list();

      // Attempt to solve the inconsistency in report types number.
      session.clear();

      // Looping through the result data.
      for (Object item : data) {
        Map row = (Map) item;
        // Query to fetch the reports details.
        String sql1 =
            "select header.id as REPORT_HEADER_ID, "
                + "  header.lConnectorInfoMaster.connectorId as SOURCE_SYSTEM_ID, "
                + "  header.displayValue as DISPLAY_VALUE,"
                + "  header.description as DESCRIPTION,"
                + "  header.reportKeywords as REPORT_KEYWORDS,"
                + "  header.reportOwner as REPORT_OWNER,"
                + "  header.reportPreviewImageUrl as REPORT_PREVIEW_IMAGE_URL,"
                + "  header.dataSource as DATA_SOURCE,"
                + "  header.lCategoryDetails.reportCategoriesId as REPORT_CATEGORIES_ID,"
                + "  header.lCategoryDetails.categoryName as ReportCategoryName,"
                + "  header.lReportTypes.reportTypeId as REPORT_TYPES_ID,"
                + "  header.lReportTypes.displayValue as ReportTypeName, "
                + "  header.updateDate as UPDATE_DATE, "
                + "  header.createDate as CREATE_DATE, "
                + "  usage.timestamp as lastUsed, "
                + "  header.lReportTypes.displayValue as report_DISPLAY_VALUE,"
                + "  header.lReportTypes.iconUrl as ICON_URL,"
                + "  mapping.reportUrl as reportURL, "
                + "  ssid.connectorName as SYSTEM_SOURCE_NAME, "
                + "  certified.customAttributeValue as reportCertified, "
                + "  avg(ratings.rating) as averageRating "

                + " from LReportHeaderData as header "
                + " join header.lReportTypes.tReportTypeReportUrlMappingSet as mapping"
                + " join header.lConnectorInfoMaster as ssid "
                + " left join header.tLatestReportUsageSet as usage "
                + " left join header.lReportRatingsSet as ratings "
                + " left join header.tReportHeaderCustomAttrsSet as certified "
                + " left join certified.lCustomAttributes lca "

                + "  where header.lReportTypes.reportTypeId = "+ row.get("categoryID")
                + "   and header.securityWhitelisted = 0"
                + "   and (header.isDeleted = 0 OR header.isDeleted IS NULL) "
                + "   and mapping.lConnectorInfoMaster.connectorId = header.lConnectorInfoMaster.connectorId"
                + "   and ssid.connectorId = header.id.lConnectorInfoMaster.connectorId "
                + "   and (lca.customattribute = 'approved' OR lca.customattribute IS NULL) "
                + "   group by header.id, "
                + "  header.lConnectorInfoMaster.connectorId, "
                + "  header.displayValue,"
                + "  header.description,"
                + "  header.reportKeywords,"
                + "  header.reportOwner,"
                + "  header.reportPreviewImageUrl, "
                + "  header.dataSource,"
                + "  header.lCategoryDetails.reportCategoriesId,"
                + "  header.lCategoryDetails.categoryName,"
                + "  header.lReportTypes.reportTypeId,"
                + "  header.lReportTypes.displayValue, "
                + "  header.updateDate,"
                + "  header.createDate, "
                + "  usage.timestamp, "
                + "  header.lReportTypes.displayValue,"
                + "  header.lReportTypes.iconUrl,"
                + "  mapping.reportUrl, "
                + "	ssid.connectorName, "
                + "  certified.customAttributeValue";


        Query query1 = session.createQuery(sql1);
        query1.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List data1 = query1.list();

        List ReportList = new ArrayList();

        // Adding reports to the response.
        row.put("reports", data1);

        // Fetching report type details.
        String sql2 =
            "select count(types.iconUrl) as reportTypeCount,lower(types.iconUrl) as reportIconPath,types.displayValue as reportTypeName"
                + "	from LReportHeaderData header"
                + "	join header.lReportTypes as types"
                + "		where types.reportTypeId = header.lReportTypes.reportTypeId "
                + "		and header.lCategoryDetails =  "
                + row.get("categoryID")
                + "		group by types.iconUrl,types.displayValue ";

        Query query2 = session.createQuery(sql2);
        query2.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        row.put("reportTypes", query2.list());
        finalList.add(row);
      }

    } catch (Exception e) {
      log.info("Exception occured with Getting TileDataByType :"+e.getMessage());
      return null;
    } finally {
      if (session.isOpen()) {
        session.close();
      }
    }

    // Returns list.
    return finalList;
  }

  private static String getOrderByClauseBasedOnSortType(
      String sortType, String nameSortKey, String timeSortKey) {
    String sql = "";
    switch (sortType) {
      case "NEWEST_FIRST":
        sql = sql + " order by " + timeSortKey + " DESC";
        break;

      case "NEWEST_LAST":
        sql = sql + " order by " + timeSortKey + " ASC";
        break;

      case "Z_A":
        sql = sql + " order by " + nameSortKey + " DESC";
        break;

      default:
        sql = sql + " order by " + nameSortKey + " ASC";
        break;
    }
    return sql;
  }
}
