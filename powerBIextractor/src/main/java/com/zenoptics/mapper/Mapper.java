package com.zenoptics.mapper;

import java.util.function.Function;

interface Mapper<T, R> extends Function<T, R> {

}
