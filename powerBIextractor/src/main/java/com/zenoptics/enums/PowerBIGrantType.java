package com.zenoptics.enums;

public enum PowerBIGrantType {
    CMDLET("cmdlet"),PASSWORD_GRANT_TYPE("password"),CLIENT_CREDENTIALS("client_credentials");

    private final String grantType;
    PowerBIGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getGrantType() {
        return grantType;
    }
}