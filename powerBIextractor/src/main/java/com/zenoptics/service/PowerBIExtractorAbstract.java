package com.zenoptics.service;

import com.zenoptics.util.ExtractorsUtil;
import com.zenoptics.util.ReportTool;

import java.util.Properties;

/*
This abstract method is created to accommodate stubs related to IReportToolExtractor
This will be removed or redefined when broader Interface for Extractor is defined
 */
public abstract class PowerBIExtractorAbstract implements IReportToolExtractor {

    @Override
    public ReportTool getReportToolName() {
        return ReportTool.PowerBICloudExtractorNew;
    }

    @Override
    public Object getSourceConnectionParams(int extractorID) {
        return null;
    }
    @Override
    public Object getSourceHeaderData(Object connection, int extractorID) {
        return null;
    }
    @Override
    public Object getSourceDetailData(Object connection) {
        return null;
    }
    @Override
    public Object getSourceReportPermissionData(Object connection, int extractorID) {
        return null;
    }
    @Override
    public Object getToolSpecificData(Object connection) {
        return null;
    }
    @Override
    public void stop() {
    }
}
