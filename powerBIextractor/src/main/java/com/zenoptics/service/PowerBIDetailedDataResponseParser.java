package com.zenoptics.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.zenoptics.pojo.powerBI.PowerBIDashboard;
import com.zenoptics.pojo.powerBI.PowerBIDetailedData;
import org.hibernate.Session;
import org.jdom.input.SAXBuilder;
import org.jdom.Document;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
/*
 PowerBIDetailedDataResponseParser handles the complete parsing logic of PBIX file to fetch the data.
 The logic is only slightly rewritten and for most part reused from the current extractor.
 When PowerBI enhances the Scanner API and provides the detailed data or field level details, this logic can be scrapped.
*/
@Component
public class PowerBIDetailedDataResponseParser {

    private Map<String, Set<PowerBIDetailedData>> reportDetailedDataMapping = new HashMap<>();

    public Map<String, Set<PowerBIDetailedData>> getReportDetailedDataMapping() {
        return reportDetailedDataMapping;
    }

    public Map<String, Set<PowerBIDetailedData>> parsePBIXFile(
            InputStream inp,
            JsonObject reportObj,
            int sourceSystemId,
            Map<String, List<String>> workspaceDatasetDataSourceMapping,
            Map<String, JsonElement> dataSourceInstanceDetailsMapping,
            Map<String, Map<String, Set<JsonElement>>> dashboardReportsMapping,
            Map<String, PowerBIDashboard> dashboardsMap,
            Logger log)
            throws IOException {

        Map<String, HashSet<String>> cubeNamesMap = new HashMap<String, HashSet<String>>();
        if (inp != null) {
            ZipInputStream zipIn = new ZipInputStream(inp);
            String layoutFileContent = null;
            Document dataMashupContent = null;
            ZipEntry entry = zipIn.getNextEntry();
            // iterates over entries in the zip file
            while (entry != null) {
                String filePath = entry.getName();
                if (!entry.isDirectory()) {
                    log.debug("File entry is " + entry.getName());
                    String local = "Report/Layout";
                    if (filePath.equals(local)) {
                        log.info(
                                "Parsing file = "
                                        + entry.toString()
                                        + " : Report = "
                                        + reportObj.get("name").getAsString());
                        byte[] output = extractFile(zipIn, log);
                        layoutFileContent = convert(output);
                        log.debug("File content is " + layoutFileContent);
                    } else if (filePath.equals("DataMashup")) {
                        byte[] output = extractFile(zipIn, log);
                        dataMashupContent =
                                getXMLDocument(stripSpecialChar(getSection1m(new String(output))), log);
                        // getTableNames(dataMashupContent);
                    } else if (filePath.equals("Connections")) {
                        log.info(
                                "Parsing Connections file = "
                                        + entry.toString()
                                        + " : Cube Report = "
                                        + reportObj.get("name").getAsString());
                        byte[] output = extractFile(zipIn, log);
                        HashSet<String> cubeNames =
                                getCubeNames(reportObj.get("id").getAsString(), new String(output), cubeNamesMap);
                        cubeNamesMap.put(reportObj.get("id").getAsString(), cubeNames);
                    }
                } else {
                    log.info("Directory entry is " + entry.getName());
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
            zipIn.close();
            return readJsonFile(
                    layoutFileContent,
                    dataMashupContent,
                    reportObj,
                    sourceSystemId,
                    workspaceDatasetDataSourceMapping,
                    dataSourceInstanceDetailsMapping,
                    dashboardReportsMapping,
                    dashboardsMap,
                    log);
        }
        return null;
    }

    private byte[] extractFile(ZipInputStream zipIn, Logger log) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        StringBuilder s = new StringBuilder();
        byte[] output = null;
        byte[] bytesIn = new byte[1024];
        byte read = 0;
        byte discard = 0;
        try {
            while ((read = (byte) zipIn.read(bytesIn, (byte) 0, 1)) != -1) {
                bos.write(bytesIn, (byte) 0, read);
            }
            output = bos.toByteArray();

        } catch (IOException e) {
            log.error("Exception occured: ", e);
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("Exception occured: ", e);
            }
        }
        return output;
    }

    private String convert(byte[] output) {
        StringBuilder js = new StringBuilder();

        for (int i = 0; i < output.length; i++) {
            byte s = output[i];
            if (s != '\0') {
                js.append((char) s);
            }
        }
        return js.toString();
    }

    public Map<String, Set<PowerBIDetailedData>> readJsonFile(
            String layoutFilecontent,
            Document dataMashupContent,
            JsonObject reportHeaderObj,
            int sourceSystemId,
            Map<String, List<String>> workspaceDatasetDataSourceMapping,
            Map<String, JsonElement> dataSourceInstanceDetailsMapping,
            Map<String, Map<String, Set<JsonElement>>> dashboardReportsMapping,
            Map<String, PowerBIDashboard> dashboardsMap,
            Logger log) {


        String layoutFilecontent1 = stripSpecialChar(layoutFilecontent);

        // Here we are replacing " with \\". We should not touch" within existing \".
        String layoutFilecontent2 =
                layoutFilecontent1
                        .replace("{\"", "{?><")
                        .replace("\"}", "?><}")
                        .replace("[\"", "[?><")
                        .replace("\"]", "?><]")
                        .replace(":\"", ":?><")
                        .replace("\":", "?><:")
                        .replace(",\"", ",?><")
                        .replace("\",", "?><,")
                        .replaceAll("([^\\\\])\"", "$1\\\\\\\\\\\\\"")
                        .replace("?><", "\"");

        try {

            String reportName = reportHeaderObj.get("name").getAsString();

            JsonParser parser = new JsonParser();
            JsonObject response = (JsonObject) parser.parse(layoutFilecontent2);
            // log.info("Before getting sections from pbix");
            JsonArray sections = response.get("sections").getAsJsonArray();

            for (JsonElement section : sections) {
                JsonObject secObj = section.getAsJsonObject();
                // We need pagename to insert dashboard metadata as it is the subtitle for a tile of
                // dashboard.
                String pagename = secObj.get("displayName").getAsString();
                JsonArray visualContainers = null;
                if (secObj.has("visualContainers")) {
                    visualContainers = secObj.get("visualContainers").getAsJsonArray();
                } else {
                    continue;
                }
                for (JsonElement container : visualContainers) {
                    Set<String> columnSet = new HashSet<String>();
                    String configStr = null;
                    String dataTransformsStr = null;
                    JsonObject dataTransforms = null;
                    try {
                        if (container.getAsJsonObject().has("dataTransforms")) {
                            dataTransformsStr = container.getAsJsonObject().get("dataTransforms").getAsString();
                        }

                        if (dataTransformsStr != null && !dataTransformsStr.isEmpty()) {
                            // JsonParser configParser = new JsonParser();
                            dataTransforms = (JsonObject) parser.parse(dataTransformsStr);

                            log.debug("Getting metadata from dataTransforms");
                            JsonArray selects = dataTransforms.get("selects").getAsJsonArray();

                            for (JsonElement select : selects) {
                                // Set<Entry<String, JsonElement>> set = column.getAsJsonObject().entrySet();
                                String fieldname = select.getAsJsonObject().get("displayName").getAsString();
                                String datasetColId = null;
                                if (!select.getAsJsonObject().has("expr")) {
                                    dataTransforms = null;
                                    continue;
                                }
                                JsonObject column = select.getAsJsonObject().get("expr").getAsJsonObject();
                                JsonObject propertyObj = getPropertyObj(column, "Property");
                                if (propertyObj == null) {
                                    if (column.has("HierarchyLevel")) {
                                        propertyObj = getPropertyObj(column, "Level");
                                        datasetColId = propertyObj.get("Level").getAsString();
                                    } else {
                                        continue;
                                    }
                                } else {
                                    datasetColId = propertyObj.get("Property").getAsString();
                                }
                                JsonObject sourceObj = getPropertyObj(propertyObj, "Entity");
                                String sourcename = sourceObj.get("Entity").getAsString();

                                columnSet.add(fieldname);

                                buildDetailedData(
                                        sourcename,
                                        fieldname,
                                        datasetColId,
                                        reportHeaderObj.getAsJsonObject().get("id").getAsString(),
                                        reportHeaderObj,
                                        sourceSystemId,
                                        workspaceDatasetDataSourceMapping,
                                        dataSourceInstanceDetailsMapping);
                            }
                            JsonArray filtersArray = null;
                            if (container.getAsJsonObject().has("filters")) {
                                String vizFiltersStr = container.getAsJsonObject().get("filters").getAsString();
                                if (!vizFiltersStr.isEmpty()) {
                                    filtersArray = (JsonArray) parser.parse(vizFiltersStr);
                                }
                            }

                            // Get the list of tiles of all dashboards using this report
                            final JsonArray filtersArrayTemp = filtersArray;
                            final JsonObject dataTransformsTemp = dataTransforms;
                            dashboardReportsMapping.entrySet().stream()
                                    .forEach(dashboardReportsMapEntry ->
                                    {
                                        dashboardReportsMapEntry.getValue().entrySet().stream()
                                                .forEach(reportsMapEntry ->
                                                {
                                                    if (reportsMapEntry.getKey().contains(reportHeaderObj.get("id").getAsString())) {
                                                        String dahsboardId = dashboardsMap.get(reportsMapEntry.getKey()).getId();
                                                        String dashboardName = dashboardsMap.get(reportsMapEntry.getKey()).getDisplayName();
                                                        Set<JsonElement> tilesArray = reportsMapEntry.getValue();
                                                    if (tilesArray != null) {
                                                        for (JsonElement tile : tilesArray) {
                                                            // If the present visual or page matches with the dashboard tile, insert the metadata of this visual for dashboard metadata
                                                            if (allColumnsPresent(tile.getAsJsonObject().get("title").getAsString()
                                                                    , tile.getAsJsonObject().get("subTitle").getAsString(), columnSet)
                                                                    || (tile.getAsJsonObject().get("title").getAsString().equals(reportName)
                                                                    && tile.getAsJsonObject().get("subTitle").getAsString().equals(pagename))) {
                                                                insertDashboardDetailData(dahsboardId, dashboardName, reportHeaderObj
                                                                        , null, dataTransformsTemp, null
                                                                        , dataMashupContent, sourceSystemId, workspaceDatasetDataSourceMapping, dataSourceInstanceDetailsMapping);
                                                                //break;
                                                                insertDetailDataForVizFilters(filtersArrayTemp, dahsboardId, dashboardName, reportHeaderObj
                                                                        , sourceSystemId, workspaceDatasetDataSourceMapping, dataSourceInstanceDetailsMapping, log);
                                                            }
                                                        }
                                                    }
                                                }
                                                });
                                            }
                                    );


                            insertDetailDataForVizFilters(
                                    filtersArray,
                                    reportHeaderObj.get("id").getAsString(),
                                    reportName,
                                    reportHeaderObj,
                                    sourceSystemId,
                                    workspaceDatasetDataSourceMapping,
                                    dataSourceInstanceDetailsMapping,
                                    log);
                        }

                        if (dataTransforms == null) {
                            configStr = container.getAsJsonObject().get("config").getAsString();
                            if (configStr != null && !configStr.isEmpty()) {
                                JsonParser configParser = new JsonParser();
                                JsonObject config = (JsonObject) configParser.parse(configStr);

                                // log.info("Before getting protoTypeQuery");
                                // JsonObject config = container.getAsJsonObject().get("config").getAsJsonObject();
                                if (config.has("singleVisual")) {
                                    JsonObject singleVisual = config.get("singleVisual").getAsJsonObject();
                                    JsonObject columnProperties = null;
                                    if (singleVisual.get("columnProperties") != null) {
                                        columnProperties = singleVisual.get("columnProperties").getAsJsonObject();
                                    }
                                    if (singleVisual.get("prototypeQuery") != null) {
                                        JsonObject protoTypeQuery =
                                                singleVisual.get("prototypeQuery").getAsJsonObject();

                                        if (protoTypeQuery.get("Select") != null) {
                                            // Could 'Select' be just an Single column object rather than array??
                                            JsonArray select = protoTypeQuery.get("Select").getAsJsonArray();
                                            // Set<Entry<String, JsonElement>> set1 =
                                            // protoTypeQuery.get("Select").getAsJsonArray().getAsJsonObject().entrySet();

                                            for (JsonElement column : select) {
                                                // Set<Entry<String, JsonElement>> set =
                                                // column.getAsJsonObject().entrySet();
                                                String columnName = column.getAsJsonObject().get("Name").getAsString();

                                                String fieldname = null;
                                                String datasetColId = null;

                                                JsonObject propertyObj =
                                                        getPropertyObj(column.getAsJsonObject(), "Property");
                                                if (propertyObj == null) {
                                                    if (column.getAsJsonObject().has("HierarchyLevel")) {
                                                        propertyObj = getPropertyObj(column.getAsJsonObject(), "Level");
                                                        datasetColId = propertyObj.get("Level").getAsString();
                                                    } else {
                                                        continue;
                                                    }
                                                } else {
                                                    datasetColId = propertyObj.get("Property").getAsString();
                                                }
                                                JsonObject sourceObj = getPropertyObj(propertyObj, "Source");

                                                if (columnProperties != null && columnProperties.has(columnName)) {
                                                    fieldname =
                                                            columnProperties
                                                                    .get(columnName)
                                                                    .getAsJsonObject()
                                                                    .get("displayName")
                                                                    .getAsString();
                                                } else {
                                                    fieldname = datasetColId;
                                                }

                                                String source = sourceObj.get("Source").getAsString();

                                                String sourcename = getSourceName(source, protoTypeQuery);

                                                columnSet.add(fieldname);

                                                buildDetailedData(
                                                        sourcename,
                                                        fieldname,
                                                        datasetColId,
                                                        reportHeaderObj.getAsJsonObject().get("id").getAsString(),
                                                        reportHeaderObj,
                                                        sourceSystemId,
                                                        workspaceDatasetDataSourceMapping,
                                                        dataSourceInstanceDetailsMapping);
                                            }
                                            JsonArray filtersArray = null;
                                            if (container.getAsJsonObject().has("filters")) {
                                                String vizFiltersStr =
                                                        container.getAsJsonObject().get("filters").getAsString();
                                                if (!vizFiltersStr.isEmpty()) {
                                                    filtersArray = (JsonArray) parser.parse(vizFiltersStr);
                                                }
                                            }

                                            // Get the set of tiles of all dashboards using this report
                                            final JsonObject columnPropertiesTemp = columnProperties;
                                            final JsonArray filtersArrayTemp = filtersArray;
                                            final JsonObject dataTransformsTemp = dataTransforms;
                                            dashboardReportsMapping.entrySet().stream()
                                                    .forEach(dashboardReportsMapEntry ->
                                                            {
                                                                dashboardReportsMapEntry.getValue().entrySet().stream()
                                                                        .forEach(reportsMapEntry ->
                                                                        {
                                                                            if (reportsMapEntry.getKey().contains(reportHeaderObj.get("id").getAsString())) {
                                                                                String dahsboardId = dashboardsMap.get(reportsMapEntry.getKey()).getId();
                                                                                String dashboardName = dashboardsMap.get(reportsMapEntry.getKey()).getDisplayName();
                                                                                Set<JsonElement> tilesArray = reportsMapEntry.getValue();
                                                                                if (tilesArray != null) {
                                                                                    for (JsonElement tile : tilesArray) {
                                                                                        // If the present visual or page matches with the dashboard tile, insert the metadata of this visual for dashboard metadata
                                                                                        if (allColumnsPresent(tile.getAsJsonObject().get("title").getAsString()
                                                                                                , tile.getAsJsonObject().get("subTitle").getAsString(), columnSet)
                                                                                                || (tile.getAsJsonObject().get("title").getAsString().equals(reportName)
                                                                                                && tile.getAsJsonObject().get("subTitle").getAsString().equals(pagename))) {
                                                                                            insertDashboardDetailData(dahsboardId, dashboardName, reportHeaderObj
                                                                                                    , protoTypeQuery, dataTransformsTemp, columnPropertiesTemp
                                                                                                    , dataMashupContent, sourceSystemId, workspaceDatasetDataSourceMapping, dataSourceInstanceDetailsMapping);
                                                                                            //break;
                                                                                            insertDetailDataForVizFilters(filtersArrayTemp, dahsboardId, dashboardName, reportHeaderObj
                                                                                                    , sourceSystemId, workspaceDatasetDataSourceMapping, dataSourceInstanceDetailsMapping, log);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        });
                                                            }
                                                    );


                                            insertDetailDataForVizFilters(
                                                    filtersArray,
                                                    reportHeaderObj.get("id").getAsString(),
                                                    reportName,
                                                    reportHeaderObj,
                                                    sourceSystemId,
                                                    workspaceDatasetDataSourceMapping,
                                                    dataSourceInstanceDetailsMapping,
                                                    log);
                                        }
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        log.error("Exception while accessing config from visualContainer: ", e);
                        log.info(
                                "======================================= CONFIG ===================================================================");
                        log.info(configStr);
                        log.info(
                                "======================================LAYOUT FILE CONTENT ===================================================================");
                        log.info(layoutFilecontent);
                        log.info("===================================================================");
                    }
                }
                String pageFiltersStr = null;

                JsonArray pageFilters = null;
                if (secObj.has("filters")) {
                    pageFiltersStr = secObj.get("filters").getAsString();
                    if (!pageFiltersStr.isEmpty()) {
                        pageFilters = (JsonArray) parser.parse(pageFiltersStr);
                    }
                }
                if (pageFilters != null) {
                    // Page Level Filters
                    for (JsonElement pageFilter : pageFilters) {
                        String fieldId = null;
                        JsonObject expression =
                                pageFilter.getAsJsonObject().get("expression").getAsJsonObject();
                        JsonObject propertyObj = getPropertyObj(expression.getAsJsonObject(), "Property");
                        if (propertyObj == null) {
                            if (expression.getAsJsonObject().has("HierarchyLevel")) {
                                propertyObj = getPropertyObj(expression.getAsJsonObject(), "Level");
                                fieldId = propertyObj.get("Level").getAsString();
                            } else {
                                continue;
                            }
                        } else {
                            fieldId = propertyObj.get("Property").getAsString();
                        }
                        String fieldname = null;
                        if (pageFilter.getAsJsonObject().has("displayName")) {
                            fieldname = pageFilter.getAsJsonObject().get("displayName").getAsString();
                        } else {
                            fieldname = fieldId;
                        }
                        JsonObject sourceObj = getPropertyObj(propertyObj, "Entity");
                        String datasetname = sourceObj.get("Entity").getAsString();
                        log.debug(
                                "Inserting page level filter "
                                        + fieldname
                                        + " ("
                                        + fieldId
                                        + ") :  for page: "
                                        + pagename
                                        + ": report : "
                                        + reportName
                                        + " : "
                                        + reportHeaderObj.get("id").getAsString());
                        buildDetailedData(
                                datasetname,
                                fieldname,
                                fieldId,
                                reportHeaderObj.getAsJsonObject().get("id").getAsString(),
                                reportHeaderObj,
                                sourceSystemId,
                                workspaceDatasetDataSourceMapping,
                                dataSourceInstanceDetailsMapping);
                    }
                }
            }

            // Report level Filters
            JsonArray reportFilters = null;

            if (response.getAsJsonObject().has("filters")) {
                String repFilters = response.getAsJsonObject().get("filters").getAsString();
                if (!repFilters.isEmpty()) {
                    reportFilters = (JsonArray) parser.parse(repFilters);
                }
            }
            if (reportFilters != null) {
                for (JsonElement reportFilter : reportFilters) {
                    String fieldId = null;
                    JsonObject expression =
                            reportFilter.getAsJsonObject().get("expression").getAsJsonObject();
                    JsonObject propertyObj = getPropertyObj(expression.getAsJsonObject(), "Property");
                    if (propertyObj == null) {
                        if (expression.getAsJsonObject().has("HierarchyLevel")) {
                            propertyObj = getPropertyObj(expression.getAsJsonObject(), "Level");
                            fieldId = propertyObj.get("Level").getAsString();
                        } else {
                            continue;
                        }
                    } else {
                        fieldId = propertyObj.get("Property").getAsString();
                    }
                    String fieldname = null;
                    if (reportFilter.getAsJsonObject().has("displayName")) {
                        fieldname = reportFilter.getAsJsonObject().get("displayName").getAsString();
                    } else {
                        fieldname = fieldId;
                    }
                    JsonObject sourceObj = getPropertyObj(propertyObj, "Entity");
                    String datasetname = sourceObj.get("Entity").getAsString();
                    log.debug(
                            "Inserting report level filter "
                                    + fieldname
                                    + " ("
                                    + fieldId
                                    + ") :  for report :"
                                    + reportName
                                    + " : "
                                    + reportHeaderObj.get("id").getAsString());
                    buildDetailedData(
                            datasetname,
                            fieldname,
                            fieldId,
                            reportHeaderObj.getAsJsonObject().get("id").getAsString(),
                            reportHeaderObj,
                            sourceSystemId,
                            workspaceDatasetDataSourceMapping,
                            dataSourceInstanceDetailsMapping);
                }
            }
        } catch (Exception e) {
            log.error("Exception occured while parsing layoutFileContent:  ", e);
            log.warn("Before replacing special characters : \n" + layoutFilecontent);
            log.warn("After replacing special characters : \n" + layoutFilecontent1);
            // log.info(layoutFilecontent);
        }
        return reportDetailedDataMapping;
    }

    private void insertDashboardDetailData(String dashboardId, String dashboardName, JsonObject reportHeaderObj, JsonObject protoTypeQuery
            , JsonObject dataTransforms, JsonObject columnProperties, Document dataMashupContent, int sourceSystemId
            , Map<String, List<String>> workspaceDatasetDataSourceMapping,
                                           Map<String, JsonElement> dataSourceInstanceDetailsMapping) {
        if (dataTransforms != null) {
            JsonArray selects = dataTransforms.get("selects").getAsJsonArray();
            for (JsonElement select : selects) {
                String fieldname = select.getAsJsonObject().get("displayName").getAsString();
                String datasetColId = null;

                if (!select.getAsJsonObject().has("expr")) {
                    dataTransforms = null;
                    continue;
                }
                JsonObject column = select.getAsJsonObject().get("expr").getAsJsonObject();

                JsonObject propertyObj = getPropertyObj(column, "Property");

                if (propertyObj == null) {
                    if (column.has("HierarchyLevel")) {
                        propertyObj = getPropertyObj(column, "Level");
                        datasetColId = propertyObj.get("Level").getAsString();
                    } else {
                        continue;
                    }
                } else {
                    datasetColId = propertyObj.get("Property").getAsString();
                }
                JsonObject sourceObj = getPropertyObj(propertyObj, "Entity");
                String sourcename = sourceObj.get("Entity").getAsString();

                buildDetailedData(sourcename, fieldname, datasetColId, dashboardId, reportHeaderObj
                        , sourceSystemId, workspaceDatasetDataSourceMapping, dataSourceInstanceDetailsMapping);
            }
        } else if (protoTypeQuery.get("Select") != null) {
            JsonArray select = protoTypeQuery.get("Select").getAsJsonArray();
            for (JsonElement column : select) {
                //Set<Entry<String, JsonElement>> set = column.getAsJsonObject().entrySet();
                String columnName = column.getAsJsonObject().get("Name").getAsString();

                String fieldname = null;
                String datasetColId = null;

                JsonObject propertyObj = getPropertyObj(column.getAsJsonObject(), "Property");
                if (propertyObj == null) {
                    if (column.getAsJsonObject().has("HierarchyLevel")) {
                        propertyObj = getPropertyObj(column.getAsJsonObject(), "Level");
                        datasetColId = propertyObj.get("Level").getAsString();
                    } else {
                        continue;
                    }
                } else {
                    datasetColId = propertyObj.get("Property").getAsString();
                }
                JsonObject sourceObj = getPropertyObj(propertyObj, "Source");
                if (columnProperties != null && columnProperties.has(columnName)) {
                    fieldname = columnProperties.get(columnName).getAsJsonObject().get("displayName").getAsString();
                } else {
                    fieldname = datasetColId;
                }

                String source = sourceObj.get("Source").getAsString();
                String sourcename = getSourceName(source, protoTypeQuery);
                buildDetailedData(sourcename, fieldname,
                        datasetColId, dashboardId, reportHeaderObj, sourceSystemId, workspaceDatasetDataSourceMapping, dataSourceInstanceDetailsMapping);
            }
        }
    }

    private void buildDetailedData(
            String sourcename,
            String fieldname,
            String datasetColId,
            String headerId,
            JsonObject reportObj,
            int sourceSystemId,
            Map<String, List<String>> workspaceDatasetDataSourceMapping,
            Map<String, JsonElement> dataSourceInstanceDetailsMapping) {

        String reportHeaderId = headerId;
        JsonObject field_details = new JsonObject();
        field_details.addProperty("field_id", datasetColId);
        field_details.addProperty("field_name", fieldname);
        field_details.addProperty("dataset_name", sourcename);
        field_details.addProperty("dataset_type", constructDataSetTypeName(workspaceDatasetDataSourceMapping, reportObj, dataSourceInstanceDetailsMapping));
    /*if (datasourceNames != null) {
      field_details.addProperty("datasource_name", String.join(",",datasourceNames));
    }*/
        if (reportObj != null) {
            field_details.addProperty("reportName", reportObj.get("name").getAsString());
            field_details.addProperty("reportId", reportObj.get("id").getAsString());
        }

        PowerBIDetailedData powerBIDetailedData =
                PowerBIDetailedData
                        .builder()
                        .headerId(reportHeaderId)
                        .datasetColumnId(datasetColId)
                        .fieldName(fieldname)
                        .sourceSystemId(sourceSystemId)
                        .fieldDetails(field_details.toString())
                        .build();
        Set<PowerBIDetailedData> powerBIDetailedDataList = reportDetailedDataMapping.get(reportHeaderId);
        if (powerBIDetailedDataList != null) {
            powerBIDetailedDataList.add(powerBIDetailedData);
        } else {
            Set<PowerBIDetailedData> detailedDataList = new HashSet<>();
            detailedDataList.add(powerBIDetailedData);
            reportDetailedDataMapping.put(reportHeaderId, detailedDataList);
        }
    }

    private String constructDataSetTypeName(Map<String, List<String>> workspaceDatasetDataSourceMapping
            , JsonObject reportObj, Map<String, JsonElement> dataSourceInstanceDetailsMapping) {
        List<String> datasourceInstanceIDList =
                workspaceDatasetDataSourceMapping.get(reportObj.getAsJsonObject().get("datasetId").getAsString());
        StringBuilder stringBuilder = new StringBuilder();
        if (!Optional.ofNullable(datasourceInstanceIDList).isPresent()) {
            return null;
        }
        datasourceInstanceIDList.stream()
                .forEach(
                        datasourceInstanceId -> {
                            String dataSourceType =
                                    dataSourceInstanceDetailsMapping
                                            .get(datasourceInstanceId)
                                            .getAsJsonObject()
                                            .get("datasourceType")
                                            .getAsString();
                            stringBuilder.append("\"" + dataSourceType + "\",");
                        });

        stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
        return stringBuilder.toString();
    }

    private String stripSpecialChar(String text) {
        // strips off all non-ASCII characters
        text = text.replaceAll("[^\\x00-\\x7F]", "");

        // removes non-printable characters from Unicode
        // text = text.replaceAll("\\p{C}.", "");

        // erases all the ASCII control characters
        text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

        return text.trim();
    }

    private void insertDetailDataForVizFilters(
            JsonArray filtersArray,
            String reportId,
            String reportName,
            JsonObject reportHeaderObj,
            int sourceSystemId,
            Map<String, List<String>> workspaceDatasetDataSourceMapping,
            Map<String, JsonElement> dataSourceInstanceDetailsMapping,
            Logger log) {
        if (filtersArray == null) {
            return;
        }
        for (JsonElement vizFilter : filtersArray) {
            String fieldId = null;
            if (!vizFilter.getAsJsonObject().has("expression")) {
                continue;
            }
            JsonObject expression = vizFilter.getAsJsonObject().get("expression").getAsJsonObject();
            JsonObject propertyObj = getPropertyObj(expression.getAsJsonObject(), "Property");
            if (propertyObj == null) {
                if (expression.getAsJsonObject().has("HierarchyLevel")) {
                    propertyObj = getPropertyObj(expression.getAsJsonObject(), "Level");
                    fieldId = propertyObj.get("Level").getAsString();
                } else {
                    continue;
                }
            } else {
                fieldId = propertyObj.get("Property").getAsString();
            }
            String fieldname = null;
            if (vizFilter.getAsJsonObject().has("displayName")) {
                fieldname = vizFilter.getAsJsonObject().get("displayName").getAsString();
            } else {
                fieldname = fieldId;
            }
            JsonObject sourceObj = getPropertyObj(propertyObj, "Entity");
            String datasetname = sourceObj.get("Entity").getAsString();
            log.debug("Inserting Visual Filters data for : " + reportName + " : " + reportId);
            buildDetailedData(
                    datasetname,
                    fieldname,
                    fieldId,
                    reportHeaderObj.getAsJsonObject().get("id").getAsString(),
                    reportHeaderObj,
                    sourceSystemId,
                    workspaceDatasetDataSourceMapping,
                    dataSourceInstanceDetailsMapping);
        }
    }

    private static HashSet<String> getCubeNames(
            String reportHeaderId, String connectionsFile, Map<String, HashSet<String>> cubeNamesMap) {
        HashSet<String> tableNames = new HashSet<String>();
        JsonParser parser = new JsonParser();
        JsonObject rootObj = (JsonObject) parser.parse(connectionsFile);
        if (rootObj.get("Connections") == null) {
            return tableNames;
        }
        JsonArray connections = rootObj.get("Connections").getAsJsonArray();
        for (JsonElement connection : connections) {
            JsonObject secObj = connection.getAsJsonObject();
            if (secObj.has("ConnectionString")) {
                String connectionString = secObj.get("ConnectionString").getAsString();
                String[] str = connectionString.split(";");
                for (String s : str) {
                    if (s.startsWith("Cube=")) {
                        tableNames.add(s.split("=")[1] + " (Cube)");
                        break;
                    }
                }
            }
        }
        cubeNamesMap.put(reportHeaderId, tableNames);
        return tableNames;
    }

    private JsonObject getPropertyObj(JsonObject value, String propertyName) {
        if (value.has(propertyName)) {
            return value;
        } else {
            JsonObject columnObj =
                    null; // = value.get("Expression").getAsJsonObject().get("Column").getAsJsonObject();
            Set<Map.Entry<String, JsonElement>> set = value.entrySet();
            for (Map.Entry<String, JsonElement> entry : set) {
                String key = entry.getKey();
                if (entry.getValue().isJsonPrimitive()) {
                    continue;
                } else {
                    columnObj = getPropertyObj(entry.getValue().getAsJsonObject(), propertyName);
                    if (columnObj != null) {
                        break;
                    }
                }
            }
            return columnObj;
        }
    }

    private boolean allColumnsPresent(String title, String subTitle, Set<String> columnSet) {

        if (subTitle.startsWith("by ")) subTitle = subTitle.substring(3);

        List<String> columnList = new ArrayList<>(Arrays.asList(title.split(",")));
        List<String> list2 = Arrays.asList(subTitle.split(","));
        columnList.addAll(list2);

        for (String column : columnList) {
            if (!columnSet.contains(column.trim())) {
                return false;
            }
        }
        return true;
    }

    private String getSourceName(String sourceCode, JsonObject query) {
        String sourceName = null;
        if (query.get("From") != null) {
            JsonArray from = query.get("From").getAsJsonArray();
            for (JsonElement source : from) {
                if (source.getAsJsonObject().get("Name").getAsString().equals(sourceCode)) {
                    sourceName = source.getAsJsonObject().get("Entity").getAsString();
                    break;
                } else {
                    continue;
                }
            }
        }
        return sourceName;
    }

    private Document getXMLDocument(String section1m, Logger log) {
        Document dom = null;
        try {
            SAXBuilder builder = new SAXBuilder();
            dom = builder.build(new ByteArrayInputStream(section1m.getBytes()));
        } catch (Exception e) {
            log.error("Exception occurred", e);
        }
        return dom;
    }

    private String getSection1m(String dataMashup) {
        return "<LocalPackageMetadataFile\n"
                + StringUtils.substringBetween(
                dataMashup, "<LocalPackageMetadataFile", "LocalPackageMetadataFile>")
                + "LocalPackageMetadataFile>";
    }
}
