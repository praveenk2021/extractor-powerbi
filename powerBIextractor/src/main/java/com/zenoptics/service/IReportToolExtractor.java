package com.zenoptics.service;

import com.zenoptics.util.ReportTool;

import java.util.Properties;

public interface IReportToolExtractor{
    public ReportTool getReportToolName();
    public Object getSourceConnectionParams(int extractorID);
    public Object getSourceConnection(Properties props, int extractorID);
    public Object getSourceHeaderData(Object connection,int extractorID);
    public Object getSourceDetailData(Object connection);
    public Object getSourceReportPermissionData(Object connection,int extractorID);
    public Object getToolSpecificData(Object connection);
    public void begin(int SourceSystem, boolean runmode);
    public void stop();
    public enum BatchFrequency {
        TRIGGER, MINUTE, HOUR, MORNING, AFTERNOON, EVENINIG, MIDNIGHT, ONDEMAND
    };
}
