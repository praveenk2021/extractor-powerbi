package com.zenoptics.service;

import com.google.gson.JsonObject;
import com.zenoptics.authservices.AuthenticationModes;
import com.zenoptics.dao.PowerBIDaoProcessor;
import com.zenoptics.dao.entity.LReportTypes;
import com.zenoptics.exceptionhandler.HttpClientException;
import com.zenoptics.pojo.powerBI.*;
import com.zenoptics.restclient.RestClient;
import com.zenoptics.util.ExtractorsUtil;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/*
PowerBI Service class performs following operations,
1) Calls PowerBI Authentication service to fetch tokens based on connection params(GrantTypes).
2) Calls the RestClient to fetch responses for getWorkSpaces,getDashboards,GerReports APIs.
3) Calls the RestClient to fetch responses for Scanner API.
4) Calls parser class to parse the responses of above steps and load into DB.
5) Calls populate Bi Dictionary functionality.
  */
@Component
public class PowerBIExtractorService extends PowerBIExtractorAbstract {

  @Autowired private AuthenticationModes powerBIAuthentication;

  @Autowired private RestClient powerBIRestClient;

  @Autowired private PowerBIResponseParser powerBIResponseParser;

  @Autowired private PowerBIDaoProcessor powerBIDaoProcessor;

  @Autowired private ExtractorsUtil extractorsUtil;


  private String access_token;
  private FileAppender appender;
  private String scanId;
  private String status = "Not Succeeded";
  private String EXTRACTOR_NAME = "PowerBICloudExtract";
  private static final String ID = "id";
  private static final String STATUS = "status";
  private Map<String, PowerBIDashboard> dashboardsMap = new HashMap<>();
  private Map<String, PowerBIReport> reportsMap = new HashMap<>();
  private Map<String, PowerBIApps> appsMap = new HashMap<>();
  private Map<String, PowerBIWorkspace> workspacesMap = new HashMap<>();
  private Map<String, Integer> reportTypeMapping = new HashMap<>();
  private Map<String, String> connectorInfoMasterDetailMap=new HashMap<>();
  private int totalUpdates = 0;

  private int powerBIWorkspaceCategory;
  private int powerBIAppCategory;
  final int TOP = 500;
  int jobID;
  final long timestamp = System.currentTimeMillis();
  Logger log = Logger.getLogger(getClass().getName() + timestamp);

  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

  public PowerBIExtractorService() {
    PatternLayout layout = new PatternLayout ("%-5p | %d{yyyy-MM-dd HH:mm:ss} | %C (%F:%L) - %m%n");
    try {
      appender = new FileAppender(layout, "logs/PowerBICloudExtract" + timestamp + ".log");
      appender.activateOptions();
      appender.setName(getClass().getName() + timestamp);
      log.addAppender(appender);
    } catch (Exception e) {
      log.error("Exception occurred: ", e);
    }
  }

  @Override
  public Object getSourceConnection(Properties props, int extractorID) {
    connectorInfoMasterDetailMap =
            powerBIAuthentication.getSourceConnectionParams(extractorID);
    return powerBIAuthentication.getSourceConnectionToken(connectorInfoMasterDetailMap, extractorID);
  }

  @Override
  public void begin(int sourceSystem, boolean runmode) {
    try {

      log.info(dateFormat.format(new java.util.Date()) + " ----Fetch token based on Grant Type");
    String token =
            (String)getSourceConnection(null, sourceSystem);
     jobID = extractorsUtil.createExtractJOB(timestamp,sourceSystem,EXTRACTOR_NAME, runmode);
      log.info(dateFormat.format(new java.util.Date()) + " ----Current JobID: "+jobID);
      log.info(dateFormat.format(new java.util.Date()) + " ----Fetching the existing category or Creating it");
      fetchOrCreateCategory();
      beginDataExtraction(connectorInfoMasterDetailMap, sourceSystem, token);
      populateBIDictonary(connectorInfoMasterDetailMap, sourceSystem);
      publishAfterExtraction(connectorInfoMasterDetailMap,sourceSystem);
      performSolarIndex();
      extractorsUtil.updateJOBStatus(jobID, powerBIDaoProcessor.getTotalUpdates());
      log.info(dateFormat.format(new Date()) + " ----Run Completed");
      log.removeAppender(appender);
      appender.close();
    } catch (Exception e) {
      log.error("Extractor failed :", e);
      //for exception scenario we need to update the job status as stopped.
      extractorsUtil.updateJOBStatus(jobID, powerBIDaoProcessor.getTotalUpdates());
    } catch (Throwable e) {
      log.error("Extractor failed :", e);
      extractorsUtil.updateJOBStatus(jobID, powerBIDaoProcessor.getTotalUpdates());
    }
  }

  /*
  Fetch the existing Category value or create new category and populate powerBIWorkspaceCategory,powerBIAppCategory
  Both powerBIWorkspaceCategory,powerBIAppCategory will act as parent category
  */
  private void fetchOrCreateCategory() throws Exception {

    String processingDataType = null;
    log.info("Start creating categories");
    log.info(dateFormat.format(new java.util.Date()) + " ----Fetching CategoryId for PowerBI");
    int powerBiRootCategoryId = extractorsUtil.fetchCategoryId("PowerBI", 1);
    if (powerBiRootCategoryId == 0) {
      log.info(dateFormat.format(new java.util.Date()) + " ----No Current category found for PowerBI so creating it");
      powerBiRootCategoryId = extractorsUtil.createAndSaveANewCategory("PowerBI", 1);
    }
    log.info(dateFormat.format(new java.util.Date()) + " ----Fetching CategoryId for APPs");
    powerBIAppCategory = extractorsUtil.fetchCategoryId("APPs", powerBiRootCategoryId);
    if (powerBIAppCategory == 0) {
      log.info(dateFormat.format(new java.util.Date()) + " ----No Current category found for APPs so creating it");
      powerBIAppCategory = extractorsUtil.createAndSaveANewCategory("APPs", powerBiRootCategoryId);
    }
    log.info(dateFormat.format(new java.util.Date()) + " ----Fetching CategoryId for Workspaces");
    powerBIWorkspaceCategory = extractorsUtil.fetchCategoryId("Workspaces", powerBiRootCategoryId);
    if (powerBIWorkspaceCategory == 0) {
      log.info(dateFormat.format(new java.util.Date()) + " ----No Current category found for Workspaces so creating it");
      powerBIWorkspaceCategory =
          extractorsUtil.createAndSaveANewCategory("Workspaces", powerBiRootCategoryId);
    }
  }

  /*
  This method initiates Data Extraction process, which calls PowerBI admin APIs for Workspaces,Dashboards,Reports,Apps and parse the response.
  TODO:Implement retry logic if Scan Status is not 'Succeeded'
  */
  private void beginDataExtraction(
      Map<String, String> connectorInfoMasterDetailMap, int sourceSystemId, String token)
      throws InterruptedException, IOException, HttpClientException, Throwable {
    // Load List of Workspaces,Dashboards,Reports,Apps in the organization
    log.info(dateFormat.format(new java.util.Date()) + " ----Loading PreScanner Data for Workspaces,Dashbaord,Reports,Apps");
    loadPreScannerData(connectorInfoMasterDetailMap, sourceSystemId, token);

    // To Keep the Report Type IDs handy for further operations.
    log.info(dateFormat.format(new java.util.Date()) + " ----Loading ReportTypes from DB");
    loadReportTypesIntoMemory();

    /*
    It initiates the Scanning of Workspaces(we need to call 3 APIs in sequence to complete the scanning process)
    1) Admin - WorkspaceInfo PostWorkspaceInfo
    2) Admin - WorkspaceInfo GetScanStatus.(need to call this API until the scan status is Succeeded.
    3) Admin - WorkspaceInfo GetScanResult
     */
    log.info(dateFormat.format(new java.util.Date()) + " ----Scheduling the scan for all the workspaces");
    JsonObject scheduleScanResult =
        scheduleScanForWorkspaces(
            connectorInfoMasterDetailMap, token, sourceSystemId, workspacesMap);
    scanId = scheduleScanResult.getAsJsonObject().get(ID).getAsString();

    if (repeatUntilScanIsSuccess(connectorInfoMasterDetailMap, token, sourceSystemId, scanId)) {
      log.info(dateFormat.format(new java.util.Date()) + " ----Reading response from Scanner API");
      JsonObject scanResult = getScanResult(connectorInfoMasterDetailMap, token, sourceSystemId);
      powerBIResponseParser.parseScanResultAndLoadDataIntoDB(
          connectorInfoMasterDetailMap,
          scanResult,
          workspacesMap,
          dashboardsMap,
          reportsMap,
          appsMap,
          reportTypeMapping,
          powerBIWorkspaceCategory,
          powerBIAppCategory,
          sourceSystemId,
          token);
    }
  }

  private void loadPreScannerData(
      Map<String, String> connectorInfoMasterDetailMap, int sourceSystemId, String token)
      throws Throwable {
    CompletableFuture<JsonObject> listOfWorkspacesInTheOrganization =
        getListOfWorkspacesInTheOrganization(connectorInfoMasterDetailMap, token, sourceSystemId);
    powerBIResponseParser.parseWorkspacesAndLoadIntoMemory(
        listOfWorkspacesInTheOrganization, workspacesMap);
    CompletableFuture<JsonObject> listOfDashboardsInTheOrganization =
        getListOfDashboardsInTheOrganization(connectorInfoMasterDetailMap, token, sourceSystemId);
    powerBIResponseParser.parseDashboardsAndLoadIntoMemory(
        listOfDashboardsInTheOrganization, dashboardsMap);
    CompletableFuture<JsonObject> listOfReportsInTheOrganization =
        getListOfReportsInTheOrganization(connectorInfoMasterDetailMap, token, sourceSystemId);
    powerBIResponseParser.parseReportsAndLoadIntoMemory(listOfReportsInTheOrganization, reportsMap);
    CompletableFuture<JsonObject> listOfAppsInTheOrganization =
        getListOfAppsInTheOrganization(connectorInfoMasterDetailMap, token, sourceSystemId);
    powerBIResponseParser.parseAppsAndLoadIntoMemory(listOfAppsInTheOrganization, appsMap);
  }

  private boolean repeatUntilScanIsSuccess(
      Map<String, String> connectorInfoMasterDetailMap,
      String token,
      int sourceSystemId,
      String scanId)
      throws InterruptedException, IOException, HttpClientException {
    log.info(dateFormat.format(new java.util.Date()) + " ----Waiting until the scan Succeeded");
    if (!status.equals("Succeeded")) {
      JsonObject scanStatus =
          getScanStatus(connectorInfoMasterDetailMap, token, sourceSystemId, scanId);
      status = scanStatus.getAsJsonObject().get(STATUS).getAsString();
      repeatUntilScanIsSuccess(connectorInfoMasterDetailMap, token, sourceSystemId, scanId);
    }
    return true;
  }

  private void loadReportTypesIntoMemory() {
    List<LReportTypes> reportTypeList =
        Arrays.asList(
            powerBIDaoProcessor.getReportTypeList("PowerBI Service Report"),
            powerBIDaoProcessor.getReportTypeList("PowerBI Service Dashboard"),
            powerBIDaoProcessor.getReportTypeList("PowerBI Service Apps"));
    for (LReportTypes reportType : reportTypeList) {
      reportTypeMapping.put(reportType.getDisplayValue(), reportType.getReportTypeId());
    }
  }

  /*
  Fetch List of Workspaces in the organization through Admin API
  https://docs.microsoft.com/en-us/rest/api/power-bi/admin/groups-get-groups-as-admin
   */
  @Async
  private CompletableFuture<JsonObject> getListOfWorkspacesInTheOrganization(
      Map<String, String> connectorInfoMasterDetailMap, String token, int sourceSystem)
      throws InterruptedException, IOException, HttpClientException {
    String reportsUri = "https://api.powerbi.com/v1.0/myorg/admin/groups?$top=5000";

    return CompletableFuture.completedFuture(
        powerBIRestClient.getCallRestClientAPI(
            connectorInfoMasterDetailMap, reportsUri, token, sourceSystem));
  }

  /*
  Fetch List of Dashboards in the organization through Admin API
  https://docs.microsoft.com/en-us/rest/api/power-bi/admin/dashboards-get-dashboards-as-admin
   */
  @Async
  private CompletableFuture<JsonObject> getListOfDashboardsInTheOrganization(
      Map<String, String> connectorInfoMasterDetailMap, String token, int sourceSystem)
      throws InterruptedException, IOException, HttpClientException {

    String reportsUri = "https://api.powerbi.com/v1.0/myorg/admin/dashboards";

    return CompletableFuture.completedFuture(
        powerBIRestClient.getCallRestClientAPI(
            connectorInfoMasterDetailMap, reportsUri, token, sourceSystem));
  }

  /*
  Fetch List of Reports in the organization through Admin API
  https://docs.microsoft.com/en-us/rest/api/power-bi/admin/reports-get-reports-as-admin
   */
  @Async
  private CompletableFuture<JsonObject> getListOfReportsInTheOrganization(
      Map<String, String> connectorInfoMasterDetailMap, String token, int sourceSystem)
      throws InterruptedException, IOException, HttpClientException {

    String reportsUri = "https://api.powerbi.com/v1.0/myorg/admin/reports";

    return CompletableFuture.completedFuture(
        powerBIRestClient.getCallRestClientAPI(
            connectorInfoMasterDetailMap, reportsUri, token, sourceSystem));
  }

  /*
  Fetch List of Apps in the organization through Admin API
  https://docs.microsoft.com/en-us/rest/api/power-bi/admin/apps-get-apps-as-admin#code-try-0
   */
  @Async
  private CompletableFuture<JsonObject> getListOfAppsInTheOrganization(
      Map<String, String> connectorInfoMasterDetailMap, String token, int sourceSystem)
      throws InterruptedException, IOException, HttpClientException {

    String reportsUri = "https://api.powerbi.com/v1.0/myorg/admin/apps?%24top=5000";

    return CompletableFuture.completedFuture(
        powerBIRestClient.getCallRestClientAPI(
            connectorInfoMasterDetailMap, reportsUri, token, sourceSystem));
  }

  /*
  Schedule scan for all the workspaces
  https://docs.microsoft.com/en-us/rest/api/power-bi/admin/workspace-info-post-workspace-info
   */
  private JsonObject scheduleScanForWorkspaces(
      Map<String, String> connectorInfoMasterDetailMap,
      String token,
      int sourceSystem,
      Map<String, PowerBIWorkspace> workspacesMap)
      throws InterruptedException, IOException, HttpClientException {
    String reportsUri =
        "https://api.powerbi.com/v1.0/myorg/admin/workspaces/getInfo?lineage=True&"
            + "datasourceDetails=True&datasetSchema=True&datasetExpressions=True&getArtifactUsers=True";

    return powerBIRestClient.postCallRestClientAPI(
        connectorInfoMasterDetailMap, reportsUri, token, sourceSystem, workspacesMap.keySet());
  }

  /*
  Retrieve Status of workspace scan.
  https://docs.microsoft.com/en-us/rest/api/power-bi/admin/workspace-info-get-scan-status
   */
  private JsonObject getScanStatus(
      Map<String, String> connectorInfoMasterDetailMap,
      String token,
      int sourceSystem,
      String scanId)
      throws InterruptedException, IOException, HttpClientException {
    String reportsUri = "https://api.powerbi.com/v1.0/myorg/admin/workspaces/scanStatus/" + scanId;

    return powerBIRestClient.getCallRestClientAPI(
        connectorInfoMasterDetailMap, reportsUri, token, sourceSystem);
  }

  /*
   Fetch the Workspace scan results.
   https://docs.microsoft.com/en-us/rest/api/power-bi/admin/workspace-info-get-scan-result
  */
  private JsonObject getScanResult(
      Map<String, String> connectorInfoMasterDetailMap, String token, int sourceSystem)
      throws InterruptedException, IOException, HttpClientException {
    String reportsUri = "https://api.powerbi.com/v1.0/myorg/admin/workspaces/scanResult/" + scanId;

    return powerBIRestClient.getCallRestClientAPI(
        connectorInfoMasterDetailMap, reportsUri, token, sourceSystem);
  }

  private void populateBIDictonary(
      Map<String, String> connectorInfoMasterDetailMap, int sourceSystem) {
    // populate.bi.dictionary
    if (connectorInfoMasterDetailMap.get("populate.bi.dictionary") != null
        && (connectorInfoMasterDetailMap.get("populate.bi.dictionary").equalsIgnoreCase("yes")
            || connectorInfoMasterDetailMap
                .get("populate.bi.dictionary")
                .equalsIgnoreCase("true"))) {
      log.info(dateFormat.format(new java.util.Date()) + " ---- Start populating BI Dictionary");
      extractorsUtil.populateDataDictionary(sourceSystem);
      log.info(dateFormat.format(new java.util.Date()) + " ---- End populating BI Dictionary");
    } else {
      log.info(
          dateFormat.format(new java.util.Date())
              + " ---- BI Dictionary is disabled for this extractor. Going ahead without populating BI Dictionary.");
    }
  }

  private void publishAfterExtraction(Map<String, String> connectorInfoMasterDetailMap,int sourceSystemId) {
    // publish after extraction
    String publishAfterExtraction = connectorInfoMasterDetailMap.get("publishafterextraction");
    if (publishAfterExtraction == null
        || publishAfterExtraction.equals("")
        || publishAfterExtraction.equalsIgnoreCase("yes")
        || !publishAfterExtraction.equalsIgnoreCase("no")) {
      log.info(dateFormat.format(new java.util.Date()) + " ---- Publishing Category Tree JSON");
      extractorsUtil.generateJsonStructureFromCategoryTree();
      log.info(
          dateFormat.format(new java.util.Date()) + " ---- Finished Publishing Category Tree JSON");
    } else {
      log.info(
          dateFormat.format(new java.util.Date())
              + " ---- Publishing is disabled for this extractor. Going ahead without publishing the category tree JSON");
    }
  }

  private void performSolarIndex() {
    log.info(dateFormat.format(new Date()) + " ----Start solr indexing");
    extractorsUtil.startSolrIndex();
    log.info(dateFormat.format(new Date()) + " ----Finished solr indexing");
    log.info(dateFormat.format(new Date()) + " ----Updating Job Status");
  }
}
