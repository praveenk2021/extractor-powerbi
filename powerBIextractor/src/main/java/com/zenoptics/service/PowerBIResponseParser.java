package com.zenoptics.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.zenoptics.dao.PowerBIDaoProcessor;
import com.zenoptics.dao.entity.LUserInfo;
import com.zenoptics.exceptionhandler.HttpClientException;
import com.zenoptics.pojo.powerBI.*;
import com.zenoptics.restclient.RestClient;
import com.zenoptics.util.ExtractorsUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/*
PowerBI Parser class performs following operations,
1) Parse the response received from getWorkSpaces,getDashboards,GerReports and load into memory.
2) Parse the response received from Scanner API and prepare it for loading into DB.
3) From Scanner API prepare HeaderLevel metadata,Detailed Level metadata and User permissions.
4) Pass all the above data to PowerBIDAOProcessor for inserting into database.
 */
@Component
public class PowerBIResponseParser {

  private static final String VALUE = "value";
  private static final String TYPE = "type";
  private static final String REPORT_TYPE = "reportType";
  private static final String REPORT_NAME = "reportName";
  private static final String WORKSPACES = "workspaces";
  private static final String REPORTS = "reports";
  private static final String REPORT_ID = "reportId";
  private static final String CREATEDBY = "createdBy";
  private static final String CREATED_DATETIME = "createdDateTime";
  private static final String MODIFIED_DATETIME = "modifiedDateTime";
  private static final String DASHBOARDS = "dashboards";
  private static final String TILES = "tiles";
  private static final String DATASETS = "datasets";
  private static final String DATASOURCE_USAGES = "datasourceUsages";
  private static final String DATASOURCE_INSTANCE_ID = "datasourceInstanceId";
  private static final String DATA_SOURCE_INSTANCES = "datasourceInstances";
  private static final String DATA_SOURCE_ID = "datasourceId";
  private static final String DATA_SOURCE_TYPE = "datasourceType";
  private static final String CONNECTION_DETAILS = "connectionDetails";
  private static final String USERS = "users";
  private static final String PRINCIPAL_TYPE = "principalType";
  private static final String IDENTIFIER = "identifier";
  private static final String EMAIL_ADDRESS = "emailAddress";
  private static final String WEB_URL = "webUrl";
  private static final String EMBED_URL = "embedUrl";
  private static final String IS_OWNED_BY_ME = "isOwnedByMe";
  private static final String DATASET_ID = "datasetId";
  private static final String APP_ID = "appId";
  private static final String ID = "id";
  private static final String NAME = "name";
  private static final String DISPLAY_NAME = "displayName";
  private static final String DESCRIPTION = "description";
  private static final String LAST_UPDATE = "lastUpdate";
  private static final String PUBLISHED_BY = "publishedBy";
  private static final String IS_READ_ONLY = "isReadOnly";

  final long timestamp = System.currentTimeMillis();
  Logger log = Logger.getLogger(getClass().getName() + timestamp);
  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

  private Map<String, List<JsonElement>> workspaceReportsMapping = new HashMap<>();
  private Map<String, List<JsonElement>> workspaceDashboardsMapping = new HashMap<>();
  private Map<String, List<String>> workspaceDatasetDataSourceMapping = new HashMap<>();
  private Map<String, List<JsonElement>> workspaceUsersMapping = new HashMap<>();
  private Map<String, List<JsonElement>> reportUsersMapping = new HashMap<>();
  private Map<String, List<JsonElement>> dashboardUsersMapping = new HashMap<>();
  private Map<String, Set<String>> finalUserList = new HashMap<>();
  private Map<String, JsonElement> dataSourceInstanceDetailsMapping = new HashMap<>();
  private Map<String, PowerBIHeaderData> powerBIHeaderDataMap = new HashMap<>();
  private Map<String, Map<String, Set<JsonElement>>> dashboardReportsMapping = new HashMap<>();
  private Map<String, Set<JsonElement>> reportTilesMapping = new HashMap<>();
  private Map<String, Set<String>> appReportsMapping = new HashMap<>();
  private Map<String, String> reportDataSourceMapping = new HashMap<>();
  Map<String, Set<PowerBIDetailedData>> detailedDataMapping = new HashMap<>();

  private String workSpaceID;
  private String workSpaceName;

  @Autowired private PowerBIDaoProcessor powerBIDaoProcessor;

  @Autowired private PowerBIDetailedDataResponseParser powerBIDetailedDataResponseParser;

  @Autowired public RestClient powerBIRestClient;

  @Autowired private ExtractorsUtil extractorsUtil;

  /*
  parse the Workspace details and load into memory.
   */
  public void parseWorkspacesAndLoadIntoMemory(
      CompletableFuture<JsonObject> response, Map<String, PowerBIWorkspace> workspacesMap)
      throws Throwable {
    JsonArray workspacesArray = response.get().get(VALUE).getAsJsonArray();
    for (JsonElement workspace : workspacesArray) {
      if (workspace.getAsJsonObject().get(TYPE).getAsString().equals("Workspace")) {
        workspacesMap.put(
            workspace.getAsJsonObject().get(ID).getAsString(), buildPowerBIWorkspace(workspace));
      }
    }
  }

  /*
  parse the Dashboard details and load into memory.
   */
  public void parseDashboardsAndLoadIntoMemory(
      CompletableFuture<JsonObject> response, Map<String, PowerBIDashboard> dashboardsMap)
      throws Throwable {
    JsonArray dashboardList = response.get().get(VALUE).getAsJsonArray();
    for (JsonElement dashboard : dashboardList) {
      dashboardsMap.put(
          dashboard.getAsJsonObject().get(ID).getAsString(), buildPowerBIDashboard(dashboard));
    }
  }

  /*
   parse the Report details and load into memory.
  */
  public void parseReportsAndLoadIntoMemory(
      CompletableFuture<JsonObject> response, Map<String, PowerBIReport> reportsMap)
      throws Throwable {
    JsonArray reportList = response.get().get(VALUE).getAsJsonArray();
    for (JsonElement report : reportList) {
      reportsMap.put(report.getAsJsonObject().get(ID).getAsString(), buildPowerBIReport(report));
    }
  }

  /*
  parse the Apps details and load into memory.
   */
  public void parseAppsAndLoadIntoMemory(
      CompletableFuture<JsonObject> response, Map<String, PowerBIApps> appsMap) throws Throwable {
    JsonArray appsList = response.get().get(VALUE).getAsJsonArray();
    for (JsonElement app : appsList) {
      appsMap.put(app.getAsJsonObject().get(ID).getAsString(), buildPowerBIApps(app));
    }
  }

  /*
   parse all scan details and load into memory and further load into DB.
  */
  public void parseScanResultAndLoadDataIntoDB(
      Map<String, String> connectorInfoMasterDetailMap,
      JsonObject scanResult,
      Map<String, PowerBIWorkspace> workspacesMap,
      Map<String, PowerBIDashboard> dashboardsMap,
      Map<String, PowerBIReport> reportsMap,
      Map<String, PowerBIApps> appsMap,
      Map<String, Integer> reportTypeMapping,
      int powerBIWorkspaceCategoryID,
      int powerBIAppCategoryID,
      int sourceSystemId,
      String token) throws InterruptedException {

    log.info(
        dateFormat.format(new java.util.Date()) + " ----Parsing and loading scan date into memory");
    loadScanResultIntoMemory(scanResult);
    executeTasksWithMultiThreading(connectorInfoMasterDetailMap,workspacesMap,dashboardsMap,reportsMap,appsMap
                                  ,reportTypeMapping,powerBIWorkspaceCategoryID,powerBIAppCategoryID,sourceSystemId,token);
    extractHeaderDataForApps(appsMap, reportTypeMapping, powerBIAppCategoryID);
    extractDetailedDataForApps();
    prepareUserListForApps();
    powerBIDaoProcessor.loadHeaderDataIntoDB(powerBIHeaderDataMap, sourceSystemId);
    powerBIDaoProcessor.loadDetailedDataIntoDB(detailedDataMapping, sourceSystemId);
    powerBIDaoProcessor.loadUsersPermissionDataIntoDB(finalUserList, sourceSystemId);
  }

  private void executeTasksWithMultiThreading(
      Map<String, String> connectorInfoMasterDetailMap,
      Map<String, PowerBIWorkspace> workspacesMap,
      Map<String, PowerBIDashboard> dashboardsMap,
      Map<String, PowerBIReport> reportsMap,
      Map<String, PowerBIApps> appsMap,
      Map<String, Integer> reportTypeMapping,
      int powerBIWorkspaceCategoryID,
      int powerBIAppCategoryID,
      int sourceSystemId,
      String token) throws InterruptedException {
    ExecutorService executorService = Executors.newCachedThreadPool();
    Callable<String> callableTask1 =
        () -> {
            extractHeaderDataForReports(
                reportsMap,
                reportTypeMapping,
                appReportsMapping,
                workspacesMap,
                powerBIWorkspaceCategoryID);
            return "Header Data for Reports extraction";
        };

    Callable<String> callableTask2 =
        () -> {
            extractHeaderDataForDashboards(
                dashboardsMap,
                reportsMap,
                reportTypeMapping,
                workspacesMap,
                powerBIWorkspaceCategoryID);
          return "Header Data for Dashboards extraction";
        };
    Callable<String> callableTask3 =
        () -> {
            extractDetailedData(connectorInfoMasterDetailMap, dashboardsMap, sourceSystemId, token);
            return "Detailed Data extraction";
        };
    Callable<String> callableTask4 =
        () -> {
            extractUserPermissions(connectorInfoMasterDetailMap, sourceSystemId);
          return "User Permissions extraction";
        };
    List<Callable<String>> callableTasks = Arrays.asList(callableTask1,callableTask2,callableTask3,callableTask4);
    executorService.invokeAll(callableTasks);
    executorService.shutdown();
  }

  /*
  Parse the Scanning API response and load the results into
  1.WorkspacesReportsMapping -> Workspcaes and list of Reports inside it.
  2.WorkspacesDashboardsMapping -> Workspcaes and list of Dashboards inside it.
  3.WorkspacesDatasetsMapping -> Workspcaes and list of Datasets inside it.
  4.WorkspacesUsersMapping -> WorkExspcaes and list of Users inside it.
  5.DataSourceInstanceDetailsMapping -> Mapping btw datasourceId and datasourceInstance.
  */
  private void loadScanResultIntoMemory(JsonObject scanResult) {
    log.info(
        dateFormat.format(new java.util.Date())
            + " ----Parsing and loading Workspaces data into memory");
    JsonArray workspacesArray = scanResult.get(WORKSPACES).getAsJsonArray();
    for (JsonElement workspaces : workspacesArray) {
      workSpaceID = workspaces.getAsJsonObject().get(ID).getAsString();
      workSpaceName = workspaces.getAsJsonObject().get(NAME).getAsString();

      log.info(
          dateFormat.format(new java.util.Date())
              + " ----Parsing reports data and creating workspaceReportsMapping,reportUsersMapping");
      // Parse Reports form Scan results and their respective userList.
      JsonArray reportsArray = workspaces.getAsJsonObject().get(REPORTS).getAsJsonArray();
      List<JsonElement> reportsList = new ArrayList<JsonElement>();
      if (reportsArray.size() != 0) {
        for (JsonElement report : reportsArray) {
          String reportId = report.getAsJsonObject().get(ID).getAsString();
          reportsList.add(report);
          String reportName = report.getAsJsonObject().get(NAME).getAsString();
          if (!reportName.substring(1, Math.min(reportName.length(), 4)).equals("App")) {
            JsonArray reportUsers =
                Optional.ofNullable(report.getAsJsonObject().get(USERS)).isPresent()
                    ? report.getAsJsonObject().get(USERS).getAsJsonArray()
                    : new JsonArray();
            List<JsonElement> reportUsersList = new ArrayList<JsonElement>();
            for (JsonElement userList : reportUsers) {
              if (!userList.isJsonNull()) {
                reportUsersList.add(userList);
              }
            }
            if (reportUsersList.size() != 0) {
              reportUsersMapping.put(reportId, reportUsersList);
            }
          }
        }
        workspaceReportsMapping.put(workSpaceID, reportsList);
      }

      // Parse Dashboards from Scan results and their respective userList.
      log.info(
          dateFormat.format(new java.util.Date())
              + " ----Parsing dashboards data and creating workspaceDashboardsMapping,dashboardUsersMapping");
      JsonArray dashboardsArray = workspaces.getAsJsonObject().get(DASHBOARDS).getAsJsonArray();
      List<JsonElement> dashboardsList = new ArrayList<JsonElement>();
      if (dashboardsArray.size() != 0) {
        for (JsonElement dashboard : dashboardsArray) {
          String dashboardId = dashboard.getAsJsonObject().get(ID).getAsString();
          dashboardsList.add(dashboard);
          String dashboardName = dashboard.getAsJsonObject().get(DISPLAY_NAME).getAsString();
          if (!dashboardName.substring(1, Math.min(dashboardName.length(), 4)).equals("App")) {
            JsonArray dashboardUsers =
                Optional.ofNullable(dashboard.getAsJsonObject().get(USERS)).isPresent()
                    ? dashboard.getAsJsonObject().get(USERS).getAsJsonArray()
                    : new JsonArray();
            List<JsonElement> dashboardUsersList = new ArrayList<JsonElement>();
            for (JsonElement userList : dashboardUsers) {
              if (!userList.isJsonNull()) {
                dashboardUsersList.add(userList);
              }
            }
            if (dashboardUsersList.size() != 0) {
              dashboardUsersMapping.put(dashboardId, dashboardUsersList);
            }
          }
          performDashboardReportsMapping(dashboard);
        }
        workspaceDashboardsMapping.put(workSpaceID, dashboardsList);
      }

      // Parse Datasets form Scan results and map it with workspaces.
      log.info(
          dateFormat.format(new java.util.Date())
              + " ----Parsing Datasets data and creating workspaceDatasetDataSourceMapping");
      JsonArray datasetsArray = workspaces.getAsJsonObject().get(DATASETS).getAsJsonArray();
      if (datasetsArray.size() != 0) {
        for (JsonElement dataset : datasetsArray) {
          List<String> dataSourceIdList = new ArrayList<>();
          JsonArray datasourcearray =
              Optional.ofNullable(dataset.getAsJsonObject().get(DATASOURCE_USAGES)).isPresent()
                  ? dataset.getAsJsonObject().get(DATASOURCE_USAGES).getAsJsonArray()
                  : new JsonArray();
          if (datasourcearray.size() != 0) {
            for (JsonElement datasourceid : datasourcearray) {
              dataSourceIdList.add(
                  datasourceid.getAsJsonObject().get(DATASOURCE_INSTANCE_ID).getAsString());
            }
            workspaceDatasetDataSourceMapping.put(
                dataset.getAsJsonObject().get(ID).getAsString(), dataSourceIdList);
          }
        }
      }

      // Map Workspaces with respective User List.
      log.info(
          dateFormat.format(new java.util.Date())
              + " ----Parsing USERS data and creating workspaceUsersMapping");
      JsonArray workspaceUsers =
          Optional.ofNullable(workspaces.getAsJsonObject().get(USERS)).isPresent()
              ? workspaces.getAsJsonObject().get(USERS).getAsJsonArray()
              : new JsonArray();
      List<JsonElement> workspaceUsersList = new ArrayList<JsonElement>();
      if (workspaceUsers.size() != 0) {
        for (JsonElement userList : workspaceUsers) {
          workspaceUsersList.add(userList);
        }
        workspaceUsersMapping.put(workSpaceID, workspaceUsersList);
      }
    }
    // Parse DataSources form Scan results and map it with dataSources.
    log.info(
        dateFormat.format(new java.util.Date())
            + " ----Parsing Data source instances data and creating dataSourceInstanceDetailsMapping");
    JsonArray data_source_instances =
        Optional.ofNullable(scanResult.get(DATA_SOURCE_INSTANCES)).isPresent()
            ? scanResult.get(DATA_SOURCE_INSTANCES).getAsJsonArray()
            : new JsonArray();
    if (data_source_instances.size() != 0) {
      for (JsonElement datasource : data_source_instances) {
        dataSourceInstanceDetailsMapping.put(
            datasource.getAsJsonObject().get(DATA_SOURCE_ID).getAsString(), datasource);
      }
    }
  }

  /*
  To parse Reports and extract Header data and put into powerBIHeaderDataMap.
  */
  private void extractHeaderDataForReports(
      Map<String, PowerBIReport> reportsMap,
      Map<String, Integer> reportTypeMapping,
      Map<String, Set<String>> appReportsMapping,
      Map<String, PowerBIWorkspace> workspacesMap,
      int parentCategoryId) {

    // fetch reportTypeId for PowerBI reports.
    log.info(dateFormat.format(new java.util.Date()) + " ----Preparing Header data for Reports");
    int reportTypeId = reportTypeMapping.get("PowerBI Service Report");
    workspaceReportsMapping.entrySet().stream()
        .forEach(
            entrySet -> {
              String workspaceId = entrySet.getKey();
              String workspaceName = workspacesMap.get(workspaceId).getWorkspaceName();
              // using the parent categoryId for Workspaces fetch the child categoryId for specific
              // workspace.
              int categoryIdForWorkspace =
                  extractorsUtil.fetchCategoryId(workspaceName, parentCategoryId);
              if (categoryIdForWorkspace == 0) {
                categoryIdForWorkspace =
                    extractorsUtil.createAndSaveANewCategory(workspaceName, parentCategoryId);
              }
              List<JsonElement> reportList = entrySet.getValue();
              for (JsonElement report : reportList) {
                String reportName = report.getAsJsonObject().get(NAME).getAsString();

                // Report name with App prefix needs to be ignored for data extraction, which will
                // be parsed in Apps section with specific logic.
                if (!reportName.substring(1, Math.min(reportName.length(), 4)).equals("App")) {
                  List<String> dataSourceList = new ArrayList<>();
                  String reportHeaderID = report.getAsJsonObject().get(ID).getAsString();
                  PowerBIReport powerBIReport = reportsMap.get(reportHeaderID);
                  List<String> datasourceInstanceIDList =
                      workspaceDatasetDataSourceMapping.get(powerBIReport.getDatasetId());
                  if (datasourceInstanceIDList != null) {
                    datasourceInstanceIDList.stream()
                        .forEach(
                            datasourceInstanceId -> {
                              String dataSourceType =
                                  dataSourceInstanceDetailsMapping
                                      .get(datasourceInstanceId)
                                      .getAsJsonObject()
                                      .get(DATA_SOURCE_TYPE)
                                      .getAsString();
                              String dataSourceName =
                                  dataSourceInstanceDetailsMapping
                                      .get(datasourceInstanceId)
                                      .getAsJsonObject()
                                      .get(CONNECTION_DETAILS)
                                      .toString();
                              dataSourceList.add(
                                  "{\"datasourceType\":"
                                      + "\""
                                      + dataSourceType
                                      + "\","
                                      + "\"datasourceName\":"
                                      + dataSourceName
                                      + "}");
                            });
                  }
                  // String appending is done inorder to maintain certain format for the data
                  // source.
                  String data_source =
                      "{\"embedURL\":"
                          + "\""
                          + powerBIReport.getEmbedUrl()
                          + "\""
                          + ",\"groupId\":"
                          + "\""
                          + entrySet.getKey()
                          + "\""
                          + ",\"datasourceList\":"
                          + dataSourceList
                          + "}";
                  PowerBIHeaderData powerBIHeaderData =
                      buildReportHeaderData(
                          reportHeaderID,
                          reportTypeId,
                          categoryIdForWorkspace,
                          data_source,
                          report);
                  powerBIHeaderDataMap.put(reportHeaderID, powerBIHeaderData);
                  reportDataSourceMapping.put(reportHeaderID, data_source);
                }
                /*
                Mapping btw Apps and set of reports inside it will be populated here.
                1) For App we can't get the respective reportId directly from the scanner API.
                2) We need to look up the reportsMap using the ID associated with the App.
                3) In the respective report, in the WebUrl filed the actual reportID associated with this report can be found.
                 */
                else {
                  log.info(
                      dateFormat.format(new java.util.Date())
                          + " ----Preparing App Reports Mapping");
                  Set<String> reportSet =
                      appReportsMapping.get(report.getAsJsonObject().get(APP_ID).getAsString());
                  PowerBIReport powerBIReport =
                      reportsMap.get(report.getAsJsonObject().get(ID).getAsString());
                  String reportId =
                      StringUtils.substringAfter(powerBIReport.getWebUrl(), "reports/");
                  if (reportSet != null) {
                    reportSet.add(reportId);
                    appReportsMapping.put(
                        report.getAsJsonObject().get(APP_ID).getAsString(), reportSet);
                  } else {
                    Set<String> newReportsSet = new HashSet<>();
                    newReportsSet.add(reportId);
                    appReportsMapping.put(
                        report.getAsJsonObject().get(APP_ID).getAsString(), newReportsSet);
                  }
                }
              }
            });
  }

  /*
  To parse Dashboards and extract Header data and put into powerBIHeaderDataMap.
  */
  private void extractHeaderDataForDashboards(
      Map<String, PowerBIDashboard> dashboardsMap,
      Map<String, PowerBIReport> reportsMap,
      Map<String, Integer> reportTypeMapping,
      Map<String, PowerBIWorkspace> workspacesMap,
      int parentCategoryId) {
    log.info(dateFormat.format(new java.util.Date()) + " ----Preparing Header data for Dasboards");
    int reportTypeID = reportTypeMapping.get("PowerBI Service Dashboard");
    workspaceDashboardsMapping.entrySet().stream()
        .forEach(
            entrySet -> {
              String workspaceId = entrySet.getKey();
              String workspaceName = workspacesMap.get(workspaceId).getWorkspaceName();
              // using the parent categoryId for Workspaces fetch the child categoryId for specific
              // workspace.
              int categoryIdForWorkspace =
                  extractorsUtil.fetchCategoryId(workspaceName, parentCategoryId);
              if (categoryIdForWorkspace == 0) {
                categoryIdForWorkspace =
                    extractorsUtil.createAndSaveANewCategory(workspaceName, parentCategoryId);
              }
              List<JsonElement> dashboardList = entrySet.getValue();
              for (JsonElement dashboard : dashboardList) {
                String dashboardName = dashboard.getAsJsonObject().get(DISPLAY_NAME).getAsString();
                // Dashboard name with App prefix needs to be ignored for data extraction.
                if (!dashboardName
                    .substring(1, Math.min(dashboardName.length(), 4))
                    .equals("App")) {
                  String dashboardHeaderID = dashboard.getAsJsonObject().get(ID).getAsString();
                  PowerBIDashboard powerBIDashboard = dashboardsMap.get(dashboardHeaderID);
                  String displayValue = powerBIDashboard.getDisplayName();
                  Set<String> reportIDList =
                      dashboardReportsMapping.get(dashboardHeaderID).keySet();
                  List<String> dataSourceList = new ArrayList<>();
                  if (null != reportIDList) {
                    int finalCategoryIdForWorkspace = categoryIdForWorkspace;
                    reportIDList.stream()
                        .forEach(
                            reportID -> {
                              String datasetID = reportsMap.get(reportID).getDatasetId();
                              List<String> datasourceInstanceIDList =
                                  workspaceDatasetDataSourceMapping.get(datasetID);
                              if (datasourceInstanceIDList != null) {
                                datasourceInstanceIDList.stream()
                                    .forEach(
                                        datasourceInstanceId -> {
                                          String dataSourceType =
                                              dataSourceInstanceDetailsMapping
                                                  .get(datasourceInstanceId)
                                                  .getAsJsonObject()
                                                  .get(DATA_SOURCE_TYPE)
                                                  .getAsString();
                                          String dataSourceName =
                                              dataSourceInstanceDetailsMapping
                                                  .get(datasourceInstanceId)
                                                  .getAsJsonObject()
                                                  .get(CONNECTION_DETAILS)
                                                  .toString();
                                          dataSourceList.add(
                                              "datasourceType :"
                                                  + dataSourceType
                                                  + "\"datasourceName\":"
                                                  + dataSourceName);
                                        });
                              }
                              String data_source =
                                  "{\"embedURL\":"
                                      + powerBIDashboard.getEmbedUrl()
                                      + ",\"groupId\":"
                                      + entrySet.getKey()
                                      + ",\"datasourceList\":"
                                      + dataSourceList;
                              PowerBIHeaderData powerBIHeaderData =
                                  buildReportHeaderData(
                                      dashboardHeaderID,
                                      reportTypeID,
                                      finalCategoryIdForWorkspace,
                                      data_source,
                                      dashboard);
                              powerBIHeaderDataMap.put(dashboardHeaderID, powerBIHeaderData);
                            });
                  }
                }
              }
            });
  }

  /*
  Detailed data or field level data for Report will be extracted by Exporting the PBIX file and parsing through it.
  PowerBIDetailedDataResponseParser handles the complete parsing logic of PBIX file to fetch the data.
   */
  private void extractDetailedData(
      Map<String, String> connectorInfoMasterDetailMap,
      Map<String, PowerBIDashboard> dashboardsMap,
      int sourceSystemId,
      String token) {

    log.info(dateFormat.format(new java.util.Date()) + " ----Preparing Detailed data");
    workspaceReportsMapping.entrySet().stream()
        .forEach(
            entrySet -> {
              String workspaceId = entrySet.getKey();
              List<JsonElement> reportList = entrySet.getValue();
              for (JsonElement report : reportList) {
                JsonObject reportObj = (JsonObject) report;
                String reportId = reportObj.get("id").getAsString();
                String reportName = reportObj.get("name").getAsString();
                  if (!reportName.substring(1, Math.min(reportName.length(), 4)).equals("App")) {

                    InputStream inputStream = null;
                    try {
                      inputStream =
                          callExportPBIXFileAPI(
                              connectorInfoMasterDetailMap,
                              token,
                              sourceSystemId,
                              workspaceId,
                              reportId);
                      if (Optional.ofNullable(inputStream).isPresent()) {
                        Map<String, Set<PowerBIDetailedData>> localDetailedDataMapping =
                            powerBIDetailedDataResponseParser.parsePBIXFile(
                                inputStream,
                                reportObj,
                                sourceSystemId,
                                workspaceDatasetDataSourceMapping,
                                dataSourceInstanceDetailsMapping,
                                dashboardReportsMapping,
                                dashboardsMap,
                                log);
                        detailedDataMapping.putAll(localDetailedDataMapping);
                      }
                    } catch (InterruptedException e) {
                      log.info("Exception occured during extracting DetailedData : "+ e.getMessage());
                    } catch (IOException e) {
                      log.info("Exception occured during extracting DetailedData : "+ e.getMessage());
                    }
                  }
              }
            });
  }

  /*
    To extract the Header Level data for Apps.
    Single App can be associated with multiple reports, so only one entry for the App will be present in Header table.
    But the data source filed should have the DataSource list for all the reports concatenated.
    Final Header level data for Apps will be populated to the existing PowerBIHeaderDataMap.
  */
  private void extractHeaderDataForApps(
      Map<String, PowerBIApps> appsMap,
      Map<String, Integer> reportTypeMapping,
      int powerBIAppCategoryID) {
    int reportTypeId = reportTypeMapping.get("PowerBI Service Apps");
    appReportsMapping.entrySet().stream()
        .forEach(
            entrySet -> {
              String appHeaderID = entrySet.getKey();
              StringBuilder dataSource =
                  new StringBuilder(
                      "{\"embedURL\":\"\",\"groupId\":\"\",\"webUrl\":\"\",\"datasourceList\":[");
              PowerBIApps powerBIApps = appsMap.get(appHeaderID);
              entrySet.getValue().stream()
                  .forEach(
                      report -> {
                        if (Optional.ofNullable(powerBIHeaderDataMap.get(report)).isPresent()) {
                          JsonObject jsonObj =
                              new JsonParser()
                                  .parse(powerBIHeaderDataMap.get(report).getDataSource())
                                  .getAsJsonObject();
                          String source =
                              jsonObj.getAsJsonObject().get("datasourceList").toString();
                          if (!source.substring(1).equals("]")) {
                            StringBuilder stringBuilder = new StringBuilder(source);
                            stringBuilder.deleteCharAt(0);
                            stringBuilder.deleteCharAt(source.length() - 2);
                            dataSource.append(stringBuilder + ",");
                          }
                        }
                      });
              dataSource.deleteCharAt(dataSource.lastIndexOf(","));
              dataSource.append("]}");
              powerBIHeaderDataMap.put(
                  appHeaderID,
                  PowerBIHeaderData.builder()
                      .headerId(appHeaderID)
                      .displayValue(powerBIApps.getName())
                      .description(powerBIApps.getDescription())
                      .createdBy(powerBIApps.getPublishedBy())
                      .dataSource(dataSource.toString())
                      .reportTypeId(reportTypeId)
                      .catagoryId(powerBIAppCategoryID)
                      .createdDateTime(powerBIApps.getLastUpdate())
                      .modifiedDateTime(powerBIApps.getLastUpdate())
                      .build());
            });
  }

  /*
  To extract the Header Level data for Apps.
  Use the DetailedDataMapping which has the detailed data for reports populated in previous step.
  Extract the detailed data for the respective reports associated with the Apps.
  Create new PowerBIDetailedDataSet by just changing the HeaderID(using appHeaderID) and reusing all other fields from existing detailed data object for report.
   */
  private void extractDetailedDataForApps() {
    appReportsMapping.entrySet().stream()
        .forEach(
            entrySet -> {
              String appHeaderID = entrySet.getKey();
              Set<PowerBIDetailedData> powerBIDetailedDataSet = new HashSet<>();
              entrySet.getValue().stream()
                  .forEach(
                      report -> {
                        if (Optional.ofNullable(detailedDataMapping.get(report)).isPresent()) {
                          detailedDataMapping.get(report).stream()
                              .forEach(
                                  powerBIDetailedData -> {
                                    powerBIDetailedData.setHeaderId(appHeaderID);
                                    powerBIDetailedDataSet.add(powerBIDetailedData);
                                  });
                        }
                      });
              detailedDataMapping.put(appHeaderID, powerBIDetailedDataSet);
            });
  }

  private InputStream callExportPBIXFileAPI(
      Map<String, String> connectorInfoMasterDetailMap,
      String token,
      int sourceSystem,
      String workspaceId,
      String reportId)
      throws InterruptedException, IOException, HttpClientException {
    String reportsUri =
        "https://api.powerbi.com/v1.0/myorg/groups/"
            + workspaceId
            + "/reports/"
            + reportId
            + "/Export";
    ;

    return powerBIRestClient.getCallForStreamRestClientAPI(
        connectorInfoMasterDetailMap, reportsUri, token, sourceSystem);
  }

  /*
  To extract use permissions associated with Reports and Dashboards.
  */
  private void extractUserPermissions(
      Map<String, String> connectorInfoMasterDetailMap, int sourceSystemId) {
    prepareUserList(connectorInfoMasterDetailMap, reportUsersMapping);
    prepareUserList(connectorInfoMasterDetailMap, dashboardUsersMapping);
  }

  private void prepareUserList(
      Map<String, String> connectorInfoMasterDetailMap,
      Map<String, List<JsonElement>> userMappingRaw) {
    String clientId = connectorInfoMasterDetailMap.get("client_id");
    String clientSecret = connectorInfoMasterDetailMap.get("client_secret");
    String tenantId = connectorInfoMasterDetailMap.get("tenant_id");

    String access_token = ExtractorsUtil.getAccessTokenForAzureAD(tenantId, clientId, clientSecret);

    userMappingRaw.entrySet().stream()
        .forEach(
            asset -> {
              String id = asset.getKey();
              List<JsonElement> assetUserList = asset.getValue();
              Set<String> userSet = new HashSet<>();
              for (JsonElement userDetail : assetUserList) {
                if (userDetail
                    .getAsJsonObject()
                    .get(PRINCIPAL_TYPE)
                    .getAsString()
                    .equals("Group")) {
                  Optional<List<String>> userListFromADOptional =
                      Optional.ofNullable(
                          ExtractorsUtil.getAllUsersInAzureADGroupByID(
                              userDetail.getAsJsonObject().get(IDENTIFIER).getAsString(),
                              tenantId,
                              clientId,
                              clientSecret,
                              access_token));
                  final Serializable serializable =
                      userListFromADOptional.isPresent()
                          ? userSet.addAll(userListFromADOptional.get())
                          : "";
                } else if (userDetail
                    .getAsJsonObject()
                    .get(PRINCIPAL_TYPE)
                    .getAsString()
                    .equals("User")) {
                  final Serializable serializable =
                      Optional.ofNullable(userDetail.getAsJsonObject().get(EMAIL_ADDRESS))
                              .isPresent()
                          ? userSet.add(
                              userDetail.getAsJsonObject().get(EMAIL_ADDRESS).getAsString())
                          : "";
                } else if (userDetail.getAsJsonObject().has(EMAIL_ADDRESS)) {
                  userSet.add(userDetail.getAsJsonObject().get(EMAIL_ADDRESS).getAsString());
                } else if (userDetail.getAsJsonObject().has(IDENTIFIER)
                    && !userDetail
                        .getAsJsonObject()
                        .get(PRINCIPAL_TYPE)
                        .getAsString()
                        .equals("App")) {
                  Optional<List<String>> userListOptional =
                      Optional.ofNullable(
                          ExtractorsUtil.getAllUsersInAzureADGroupByID(
                              userDetail.getAsJsonObject().get(IDENTIFIER).getAsString(),
                              tenantId,
                              clientId,
                              clientSecret,
                              access_token));
                  final Serializable serializable =
                      userListOptional.isPresent() ? userSet.addAll(userListOptional.get()) : "";
                }
              }
              Set<String> userSet1 =
                  userSet.stream()
                      .map(
                          userId -> {
                            LUserInfo lUserInfo =
                                extractorsUtil.getUserInfoFromEmailAddress(userId);
                            if (lUserInfo != null) {
                              userId = lUserInfo.getUserId();
                            }
                            return userId;
                          })
                      .collect(Collectors.toSet());
              finalUserList.put(id, userSet1);
            });
  }

  private void prepareUserListForApps() {
    appReportsMapping.entrySet().stream()
        .forEach(
            entrySet -> {
              String appHeaderID = entrySet.getKey();
              Set<String> userSet = new HashSet<>();
              entrySet.getValue().stream()
                  .forEach(
                      report -> {
                        if (Optional.ofNullable(finalUserList.get(report)).isPresent()) {
                          finalUserList.get(report).stream()
                              .forEach(
                                  setOfUsers -> {
                                    userSet.add(setOfUsers);
                                  });
                        }
                      });
              finalUserList.put(appHeaderID, userSet);
            });
  }

  /*
  Build Header Object - PowerBIHeaderData
  */
  private PowerBIHeaderData buildReportHeaderData(
      String reportHeaderID,
      int reportTypeId,
      int categoryId,
      String data_source,
      JsonElement report) {
    return PowerBIHeaderData.builder()
        .headerId(reportHeaderID)
        .displayValue(
            Optional.ofNullable(report.getAsJsonObject().get(NAME)).isPresent()
                ? report.getAsJsonObject().get(NAME).getAsString()
                : report.getAsJsonObject().get(DISPLAY_NAME).getAsString())
        .description(
            Optional.ofNullable(report.getAsJsonObject().get(NAME)).isPresent()
                ? report.getAsJsonObject().get(NAME).getAsString()
                : report.getAsJsonObject().get(DISPLAY_NAME).getAsString())
        .createdBy(
            Optional.ofNullable(report.getAsJsonObject().get(CREATEDBY)).isPresent()
                ? report.getAsJsonObject().get(CREATEDBY).getAsString()
                : "")
        .dataSource(data_source)
        .reportTypeId(reportTypeId)
        .createdDateTime(
            Optional.ofNullable(report.getAsJsonObject().get(CREATED_DATETIME)).isPresent()
                ? report.getAsJsonObject().get(CREATED_DATETIME).getAsString()
                : "")
        .modifiedDateTime(
            Optional.ofNullable(report.getAsJsonObject().get(MODIFIED_DATETIME)).isPresent()
                ? report.getAsJsonObject().get(MODIFIED_DATETIME).getAsString()
                : "")
        .catagoryId(categoryId)
        .build();
  }

  /*
  This method performs,
  1) mapping Dashboard with set of Reports.
  2) mapping Report with set of Tiles.
   */
  private void performDashboardReportsMapping(JsonElement dashboard) {
    Set<String> reportIDList = new HashSet<String>();

    JsonArray tilesArray = dashboard.getAsJsonObject().get(TILES).getAsJsonArray();
    for (JsonElement tiles : tilesArray) {
      String reportID =
          Optional.ofNullable(tiles.getAsJsonObject().get(REPORT_ID)).isPresent()
              ? tiles.getAsJsonObject().get(REPORT_ID).getAsString()
              : "";
      if (reportID != "") {
        reportIDList.add(reportID);
        reportTilesMapping
            .computeIfAbsent(reportID, k -> new HashSet<>())
            .add(tiles.getAsJsonObject());
      }
    }
    dashboardReportsMapping.put(
        dashboard.getAsJsonObject().get(ID).getAsString(), reportTilesMapping);
  }

  private PowerBIWorkspace buildPowerBIWorkspace(JsonElement workspace) {
    return PowerBIWorkspace.builder()
        .id(workspace.getAsJsonObject().get(ID).getAsString())
        .workspaceName(workspace.getAsJsonObject().get(NAME).getAsString())
        .build();
  }

  private PowerBIDashboard buildPowerBIDashboard(JsonElement dashboard) {
    return PowerBIDashboard.builder()
        .id(dashboard.getAsJsonObject().get(ID).getAsString())
        .displayName(dashboard.getAsJsonObject().get(DISPLAY_NAME).getAsString())
        .isReadOnly(dashboard.getAsJsonObject().get(IS_READ_ONLY).getAsBoolean())
        .build();
  }

  private PowerBIReport buildPowerBIReport(JsonElement report) {
    return PowerBIReport.builder()
        .id(report.getAsJsonObject().get(ID).getAsString())
        .reportType(report.getAsJsonObject().get(REPORT_TYPE).getAsString())
        .reportName(report.getAsJsonObject().get(NAME).getAsString())
        .webUrl(report.getAsJsonObject().get(WEB_URL).getAsString())
        .embedUrl(report.getAsJsonObject().get(EMBED_URL).getAsString())
        .datasetId(report.getAsJsonObject().get(DATASET_ID).getAsString())
        .appId(
            (Optional.ofNullable(report.getAsJsonObject().get(APP_ID)).isPresent())
                ? report.getAsJsonObject().get(APP_ID).getAsString()
                : "")
        .build();
  }

  private PowerBIApps buildPowerBIApps(JsonElement app) {
    return PowerBIApps.builder()
        .id(app.getAsJsonObject().get(ID).getAsString())
        .name(app.getAsJsonObject().get(NAME).getAsString())
        .description(app.getAsJsonObject().get(DESCRIPTION).getAsString())
        .lastUpdate(app.getAsJsonObject().get(LAST_UPDATE).getAsString())
        .publishedBy(app.getAsJsonObject().get(PUBLISHED_BY).getAsString())
        .build();
  }
}
