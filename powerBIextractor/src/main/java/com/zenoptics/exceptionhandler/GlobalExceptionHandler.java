package com.zenoptics.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<String> handleException(final Exception exception) {
        return new ResponseEntity<String>("",HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = HttpClientException.class)
    public ResponseEntity<String> handleIOException(final HttpClientException httpClientException) {
        return new ResponseEntity<String>(httpClientException.getStatusText(),httpClientException.getStatusCode());
    }

}
