package com.zenoptics.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

public class HttpClientException extends HttpClientErrorException {

    public HttpClientException(HttpStatus statusCode) {
        super(statusCode);
    }

    public HttpClientException(HttpStatus statusCode, String statusText) {
        super(statusCode, statusText);
    }

}
